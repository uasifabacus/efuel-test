import UIKit
import SwiftDate

class VC_register: BaseViewController {

    @IBOutlet weak var lbl_PDA: Label!
    @IBOutlet weak var lbl_register: Label!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.setDateNTime()
        
        if self.viewIfLoaded?.window != nil {
            
            self.isPrinterConected(truckId: RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists) { (isSucceed, deviceType) in
                if isSucceed {
                                if deviceType == .simulator {
                                    self.setDateNTime()
                                }
                                else {
                //                    If LCR then get register date and time from LCR SDK method.
                                }
                            }
            }
        }
    }
    
    
    func setDateNTime() {
        
        if let syncRegisterTime = Singleton.shared.syncRegisterTime {
            
            let date = DateInRegion(syncRegisterTime, region: Region.local)
            self.lbl_PDA.text = "\(date?.toFormat(k_MMDDYY) ?? "") \(date?.toFormat(k_HHMMA) ?? "")"
            self.lbl_register.text = self.lbl_PDA.text
        }
        else {
            let date = DateInRegion(Singleton.shared.loginTime, region: Region.local)
            self.lbl_PDA.text = "\(date?.toFormat(k_MMDDYY) ?? "") \(date?.toFormat(k_HHMMA) ?? "")"
            self.lbl_register.text = k_LCRRegisterTime
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    @IBAction func action_sync(_ sender: Any) {
        Singleton.shared.syncRegisterTime = Helper.mmDDYYWithCurrentTime
        self.setDateNTime()
    }
}
