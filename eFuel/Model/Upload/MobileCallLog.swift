import UIKit
import Realm
import RealmSwift

class MobileCallLog: RealmSwift.Object, Codable {

    @objc dynamic var mobile_call_log_id: Int = 0
    var efuel_mobile_call_log_id = RealmOptional<Int>()
    var SQL_UID = RealmOptional<Int>()
    var user_account_id = RealmOptional<Int>()
    @objc dynamic var call_date: String?
    var login_user_id = RealmOptional<Int>()
    var call_status = RealmOptional<Int>()
    @objc dynamic var call_notes: String?
    @objc dynamic var call_action: String?
    @objc dynamic var call_type: String?
    @objc dynamic var reason: String?
    var who_next = RealmOptional<Int>()
    var assigned_to = RealmOptional<Int>()
    @objc dynamic var is_this_open: String?
    var installation_street_address_id = RealmOptional<Int>()
    var tank_id = RealmOptional<Int>()
    @objc dynamic var opened_by: String?
    @objc dynamic var closed_by: String?
    @objc dynamic var login_user_name: String?
    @objc dynamic var followup_date: String?
    @objc dynamic var Mobile_insert_update_create_timestamp: String?
    @objc dynamic var last_insert_update_time_stamp: String?
    
    
    
    
    enum CodingKeys: String, CodingKey {
        
        case mobile_call_log_id
        case efuel_mobile_call_log_id
        case SQL_UID
        case user_account_id
        case call_date
        case login_user_id
        case call_status
        case call_notes
        case call_action
        case call_type
        case reason
        case who_next
        case assigned_to
        case is_this_open
        case installation_street_address_id
        case tank_id
        case opened_by
        case closed_by
        case login_user_name
        case followup_date
        case Mobile_insert_update_create_timestamp
        case last_insert_update_time_stamp
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "mobile_call_log_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    //Incrementa ID
    var IncrementaID : Int {
        let realm = try! Realm()
        if let retNext = realm.objects(MobileCallLog.self).sorted(byKeyPath: "mobile_call_log_id").last?.mobile_call_log_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
