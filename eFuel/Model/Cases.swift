import Foundation
import UIKit

enum DownloadLevel {
    case appNewlyInstalled
    case downloadAfterUpload
    case isShiftStarting
    case downloadAfterUploadBG
}


enum SiftStatus : String {
    case login
    case truck
    case new_shift_id
}



enum ShiftExistingFlag : Int {
    case login = 1
    case truck = 2
    case newId = 3
    case last_manual_sale_number = 4
}



enum ShiftStatusFlag : Int {
    case err = 0
    case completeShift = 1
    case openShiftNewUser = 2
    case openShiftOldUser = 3
    case noShift = 4
}


enum tfwCrashType : Int {
    case AppSTOP = 0
    case eFuel_mobile_config_missing = 1          //Problem with eFuel_mobile_config.xml missing
    case eFuel_mobile_config_bad_deviceType = 2           //Problem with eFuel_mobile_config.xml has bad value on deviceType
    case eFuel_mobile_config_bad_XML_format = 3           //Problem with xml format
    case eFuel_mobile_config_bad_RDA_Data_Source = 4          //Problem with eFuel_mobile_config.xml has bad value on data source
    case eFuel_mobile_config_bad_debugType = 5           //Problem with eFuel_mobile_config.xml has bad value on data source
    case eFuel_mobile_config_bad_SqlCeAgentUrl = 6           //Problem with eFuel_mobile_config.xml has bad value on data source
    case eFuel_mobile_config_bad_printerType = 7         //Problem with eFuel_mobile_config.xml has bad value on printerType
    case eFuel_mobile_trucks_bad_deviceType = 8           //Problem with trucks has bad value on deviceType
    case eFuel_mobile_trucks_bad_printerType = 9          //Problem with trucks has bad value on printerType
    case eFuel_mobile_not_specified_deviceType = 10          //Problem with eFuel_mobile_config.xml and trucks has no deviceType
    case eFuel_mobile_not_specified_printerType = 11         //Problem with eFuel_mobile_config.xml and trucks has no deviceType
    case eFuel_mobile_config_bad_debugTrack = 12
}



enum erType : Int {
    case er_Manual = 0
    case er_simulator = 1
    case er_LCR = 2
}



enum isDeviceTypeValid {
    case wrongSpecifiedDevice
    case notSpecifiedDevice
    case validDevice
}


enum DeviceType: String {
    case simulator
    case LCR
    case Manual
}


enum EventLogCases : String {
    case startShift = "Start Shift"
    case stopShift = "Stop Shift"
    case loadTruck = "Load truck"
    case unloadTruck = "Unload truck"
    case pump = "Pump"
}




enum LoadingStatus {
    case prepare_load
    case loading
    case log_load_data
}


enum RecordType : Int {
    case newDelivery = 1          //just new created record.
    
    case newDelivery_done_after_sync = 2         //created from PDA and synced
    case newDelivery_done_before_sync = 3            //created from PDA and not synced yet
    case Delivery_done = 4            //existing deliveries
    
    case newDelivery_skip_after_sync = 5         //created from PDA & skip and synced
    case newDelivery_skip_before_sync = 6            //created from PDA & skip and not synced yet
    case Delivery_skip = 7            //existing deliveries was skip
    
    case LoadTruck = 8
    case unLoadTruck = 9
    
    case newDeliveryNT = 11          //created from PDA with adding new customer
    case newDeliveryNT_done = 14         //created from PDA with adding new customer and done
    case newDeliveryNT_skip = 17         //created from PDA with adding new customer and skip
}



enum SetImage : String {
    case green
    case red
    case yellow
    case na = "n/a"
    
    
    func getImage() -> UIImage {
        switch self {
        case .green:
            return UIImage(named: "green-dot")!
        case .red:
            return UIImage(named: "orange-dot")!
        case .yellow:
            return UIImage(named: "yellow-dot")!
        default:
            return UIImage(named: "gray-dot-ico")!
        }
    }
}



enum DeliveryStatus : Int {
    case open = 6
    case completed = 8
    case skipped = 3
}
