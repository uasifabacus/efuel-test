import UIKit
import SwifterSwift
import SwiftEntryKit
import MBProgressHUD
import SwiftDate




enum ContactsFilter {
    case none
    case mail
    case message
}



extension CGRect {
    var minEdge: CGFloat {
        return min(width, height)
    }
}

extension UIScreen {
    var minEdge: CGFloat {
        return UIScreen.main.bounds.minEdge
    }
}


class Helper: NSObject {
    
    
    
    
    private static let toast : EKAttributes = {
        var attributes: EKAttributes
        attributes = .topNote
        attributes.popBehavior = .animated(animation: .translation)
        attributes.entryBackground = EKAttributes.BackgroundStyle.color(color: EKColor.init(UIColor.purple))
//        attributes.entryBackground = .color(color: .purple)
//        attributes.shadow = .active(with: .init(color: .red, opacity: 0.5, radius: 2))
        
//        attributes.shadow = .active(with: .init(color: .red, opacity: 0.5, radius: 2, offset: .zero))
        
        attributes.statusBar = .light
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .easeOut)
        
        return attributes
    }()
    
    
    
    
    
    
    
    
    class func navBarColor(navBar: UINavigationBar) {
        navBar.setColors(background: .eFuelTheme, text: .white)
    }
    
    class func TF_underLine(textField: UITextField) -> () {
        /*
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0,
                              y: textField.frame.size.height - width,
                              width: textField.frame.size.width,
                              height: textField.frame.size.height)
        
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        
        textField.borderStyle = .none
*/
        
        
        let height = CGFloat(1.0)
        
        if (textField.layer.sublayers?.contains{ $0.name == "underLine"} ?? false) == false {
            
            let layer_ = CALayer()
            layer_.borderColor = UIColor.darkGray.cgColor
            layer_.frame = CGRect(x: 0,
                                  y: textField.frame.size.height - height,
                                  width: textField.frame.size.width,
                                  height: textField.frame.size.height)
            
            layer_.borderWidth = height
            textField.layer.addSublayer(layer_)
            textField.layer.masksToBounds = true
            layer_.name = "underLine"
            
            textField.borderStyle = .none
        }
        else {
            
            if let array_ = textField.layer.sublayers?.filter({$0.name == "underLine"}), array_.count > 0 {
                
                let layer_ = array_[0]
                layer_.frame = CGRect(x: 0,
                                      y: textField.frame.size.height - height,
                                      width: textField.frame.size.width,
                                      height: height)
            }
        }
    }
    
    
    class func TF_downArrow(textField: UITextField) -> () {
        let imageView = UIImageView(image: UIImage(named: "downArrow"))
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        textField.rightView = imageView
        textField.rightView?.frame = CGRect(x: 0, y: 0, width: 10 , height:10)
        textField.rightViewMode = .always
        
        textField.borderStyle = .none
    }
    
    
    
    
    
    
    /*
     func phoneNumberWithContryCode() -> [String] {
     
     let contacts = PhoneContacts.getContacts() // here calling the getContacts methods
     var arrPhoneNumbers = [String]()
     for contact in contacts {
     for ContctNumVar: CNLabeledValue in contact.phoneNumbers {
     if let fulMobNumVar  = ContctNumVar.value as? CNPhoneNumber {
     //let countryCode = fulMobNumVar.value(forKey: "countryCode") get country code
     if let MccNamVar = fulMobNumVar.value(forKey: "digits") as? String {
     arrPhoneNumbers.append(MccNamVar)
     }
     }
     }
     }
     return arrPhoneNumbers // here array has all contact numbers.
     }
     */
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    /*
    class func concatinateImageURL(imageURL: String?, imgView: UIImageView) {
        
        guard var str = imageURL else { return }
        
        var url : URL?
        if (str.contains("http")  == true) {
            str = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            url = URL(string: str)
        }
        else {
            url = URL(string: "\(k_imageBaseUrl)\(str)")
        }

        
        
//        url = URL(string: "https://images.pexels.com/photos/277253/pexels-photo-277253.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
//        let processor = RoundCornerImageProcessor(cornerRadius: 20)
        imgView.kf.indicatorType = .activity
//        imgView.backgroundColor = UIColor.lightGray
        
        imgView.kf.setImage(with: url,
                            placeholder: k_placeHolder,
                            options: [.forceRefresh, .transition(.fade(1)), .cacheOriginalImage])
    }
    */
    
    
    
    
    
    class func priceFormate(str : String) -> (String) {
        
        return "\(str) USD"
    }
    
    
    
    
    
    
    class func TF_setLeftPaddingWithImage(image: UIImage, textField: UITextField) {
        
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: textField.height))
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.tag = 1
        imageView.frame = CGRect(x: 5, y: 5, width: 20, height: textField.height - 10)
        view.addSubview(imageView)
        
        
        textField.leftViewMode = .always
        textField.leftView = view
    }
    
    
    
    class func TF_setRightPaddingWithImage(image: UIImage, textField: UITextField) {
        
        
        let view = UIView(frame: CGRect(x: textField.width - 25 , y: 0, width: 15, height: textField.height))
        
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: -5, y: 5, width: 10, height: textField.height - 10)
        view.addSubview(imageView)
        
        textField.rightViewMode = .always
        textField.rightView = view
    }
    
    
//************************************************************************************************** Cancel same previous request
    class func cancelSamePrevRequest(currentURL: String) {
        
        AlamofireManager.shared.session.getTasksWithCompletionHandler {(dataTasks, _, _) in
            
            dataTasks.forEach
                {
                    if let previousRequest = $0.originalRequest?.url?.absoluteString {
                        
                        if (previousRequest.contains(currentURL.components(separatedBy: "/")[0], caseSensitive: false)) {
                            print("previousRequest going to cancel: \(previousRequest)")
                            $0.cancel()
                        }
                    }
            }
        }
    }
//************************************************************************************************** Cancel same previous request
    
    
    
    static func validateAndTrimText(TF: UITextField, message: String? = nil) -> String? {
        
        return validateAndTrimText(str: TF.text, message: message)
        
//        guard (TF.text?.isEmpty == false) else {
//
//            if let msg = message {
//                Helper.showBanner(text: msg, isSuccess: false)
//            }
//
//            return nil
//        }
//
//        guard let comment = TF.text, comment.isWhitespace == false else {
//
//            if let msg = message {
//                Helper.showBanner(text: msg, isSuccess: false)
//            }
//
//            return nil
//        }
//
//
//        return comment.trimmed
    }
    
    
    static func validateAndTrimText(str: String?, message: String? = nil) -> String? {
        
        guard (str?.isEmpty == false) else {
            
            if let msg = message {
                Helper.showBanner(text: msg, isSuccess: false)
            }
            
            return nil
        }
        
        guard let comment = str, comment.isWhitespace == false else {
            
            if let msg = message {
                Helper.showBanner(text: msg, isSuccess: false)
            }
            
            return nil
        }
        
        
        return comment.trimmed
    }
    
    
    /*
    static func isValidateAmount (amountTF: UITextField, message: String) -> Bool {
        
        guard (amountTF.text?.isValidAmount() == true) else {
            Helper.showBanner(text: message, isSuccess: false)
            return false
        }
        
        return true
    }
    */
    
    
    
    class func showBanner(text: String, isSuccess:Bool) -> () {
        
//        let style = EKProperty.LabelStyle(font: .systemFont(ofSize: 14, weight: .heavy), color: UIColor.white, alignment: .center)
        let style = EKProperty.LabelStyle(font: UIFont(name: "Helvetica", size: 36.0)!, color: EKColor.init(.white), alignment: .center, displayMode: EKAttributes.DisplayMode.dark, numberOfLines: 3)
        let labelContent = EKProperty.LabelContent(text: text, style: style)
        let contentView = EKNoteMessageView(with: labelContent)
        
        var attributes = Helper.toast
        
        if isSuccess{
            
            attributes.hapticFeedbackType = .success
            attributes.entryBackground = EKAttributes.BackgroundStyle.color(color: .init(.green))
//            attributes.entryBackground = .color(color: .purple)
        }
        else{
            
            attributes.hapticFeedbackType = .error
//            attributes.entryBackground = .color(color: .red)
            attributes.entryBackground = EKAttributes.BackgroundStyle.color(color: .init(.red))
        }
        
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    
    
    
//
//    static func isValidateContact (contactTF: UITextField, message: String) -> Bool {
//
//        guard (contactTF.text?.isValidContact == true) else {
//            Helper.showBanner(text: message, isSuccess: false)
//            return false
//        }
//
//        return true
//    }
    
    
    
//    class func secondsForRadiantWF(str: String) -> Int? {
//        
//        let str = str.trimmingCharacters(in: .whitespacesAndNewlines)
//        
//        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        
//        let date = dateFormatter.date(from: str) // "2017-01-09T11:00:00.000Z"
//        
//        guard let hours = date?.hour, let minutes = date?.minute, let seconds = date?.second else { return nil }
//        
//        return ((hours * 3600) + (minutes * 60) + seconds)
//    }
    
    
    
//    class func dateTimeForRadiantWF(str: String) -> String {
//
//        let str = str.trimmingCharacters(in: .whitespacesAndNewlines)
//        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//        let date = dateFormatter.date(from: str) // "2017-01-09T11:00:00.000Z"
//
//        return "\(date?.monthName(ofStyle: .threeLetters) ?? "") \((date?.year)!) \(date?.timeString() ?? "")"
//    }
    
    
    private static let dateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSS"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter
    }()
    
    static var mmDDYYWithCurrentTime : String {
        return dateFormatter.string(from: Date())
    }
    
    static func toDate(string: String) -> Date {

        return dateFormatter.date(from: string)!
    }
    
    static func monthYearDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "MMMM dd, yyyy"
//        formatter.dateFormat = "dd, MMMM, yyyy"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.string(from: date)
    }
    
    
    static func hhmmsssss(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "HH:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.string(from: date)
    }
    
    
    
    
    
    class func hourMinutes(str: String) -> Int? {
        
        var str = str.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let arrray = str.components(separatedBy: ":")
        str = "\(arrray[0]):\(arrray[1])"
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "HH:mm"
        
        let date = dateFormatter.date(from: str) // "2017-01-09T11:00:00.000Z"
        
        guard let hours = date?.hour, let minutes = date?.minute, let seconds = date?.second else { return nil }
        
        return ((hours * 3600) + (minutes * 60) + seconds)
    }
    
    
    
    static func dateFormateChange(dateText: String) -> String {
        
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "MM dd,yyyy"

        
        // convert your string to date
        let yourDate = formatter.date(from: dateText)
        
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "MMM-dd-yyyy"
        
        // again convert your date to string
        return formatter.string(from: yourDate!)
    }
    
    
    
    
    
    static func amountInWords(value: String) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .spellOut
        
        guard let doubleValue = value.double() else { return "-" }
        
        guard let amountInWords = numberFormatter.string(from: NSNumber(floatLiteral: doubleValue)) else { return "-" }
        
        return amountInWords
    }
    
    
    static func showHud(str: String? = nil) {
        
        
        if let window = UIApplication.shared.keyWindow {
            
            if let previoudHud = MBProgressHUD(for: window) {
                setHudText(str: str, hud: previoudHud)
                return
            }
            
            let loadingNotification = MBProgressHUD.showAdded(to: window, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            
            setHudText(str: str, hud: loadingNotification)
        }
    }
    
    private static func setHudText(str: String?, hud: MBProgressHUD) {
        
        guard let str = str else {
            hud.label.text = "Loading..."
            return
        }
        
        
        hud.label.text = str
    }
    
    
    
    
    static func hideHud() {
        
        if let window = UIApplication.shared.keyWindow { MBProgressHUD.hide(for: window, animated: true) }
    }
    
    
    static func checkCharacterLength(str: String) -> String {
        
        var str: String = str
        
        if str.count > 33 {
            
            
            var isCompleted = false
            var lastIndex = 33
            
            while (isCompleted == false) {
                
                let index = str.index(str.startIndex, offsetBy: 33)
                print(str.prefix(upTo: index))
                print(str.suffix(from: index))
                
                if str.count < 33 {
                    isCompleted = true
                }
            }
        }
        
        return str
    }
}



extension StringProtocol where Self: RangeReplaceableCollection {
    mutating func insert(separator: Self, every n: Int) {
        for index in indices.reversed() where index != startIndex &&
            distance(from: startIndex, to: index) % n == 0 {
            insert(contentsOf: separator, at: index)
        }
    }

    func inserting(separator: Self, every n: Int) -> Self {
        var string = self
        string.insert(separator: separator, every: n)
        return string
    }
}
