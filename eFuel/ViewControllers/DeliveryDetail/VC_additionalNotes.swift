import UIKit

class VC_additionalNotes: BaseViewController {

    private static let k_date = "date"
    private static let k_note = "note"
    
    
    @IBOutlet weak var TV_note: UITextView!
    @IBOutlet weak var tblView: UITableView!
    
    
    
    private static var array_notes = [[VC_additionalNotes.k_date:"08/22/2019 4:10:00 PM", VC_additionalNotes.k_note: "asdfasd fas df asdf ads"],
                                      [VC_additionalNotes.k_date:"", VC_additionalNotes.k_note: "asdfasd"],
                                      [VC_additionalNotes.k_date:"08/22/2019 4:10:00 PM", VC_additionalNotes.k_note: "fas df asdf ads"],
                                      [VC_additionalNotes.k_date:"08/22/2019 4:10:00 PM", VC_additionalNotes.k_note: "ads123 123 123 asdfasd fas df asdf"]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Additional Notes"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? VC_additionalNotesCreate {
            vc.callbackNewNote = { newNote in
                print(newNote)
                
                
                let dateformatter = DateFormatter()
                dateformatter.dateFormat = "MM/dd/yyyy h:mm:ss a"
                let now = dateformatter.string(from: Date())
                
                VC_additionalNotes.array_notes.append([VC_additionalNotes.k_date:now,
                                                       VC_additionalNotes.k_note: newNote])
                
                let selected = self.tblView.indexPathForSelectedRow
                self.tblView.reloadData()
                self.tblView.selectRow(at: selected, animated: false, scrollPosition: .none)
            }
        }
    }
}




//MARK:- UITableViewDataSource
extension VC_additionalNotes : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VC_additionalNotes.array_notes.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_newCaseReason") as! Cell_newCaseReason
        cell.lbl_title.text = VC_additionalNotes.array_notes[indexPath.row][VC_additionalNotes.k_date]
        return cell
    }
}



//MARK:- UITableViewDelegate
extension VC_additionalNotes : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.TV_note.text = VC_additionalNotes.array_notes[indexPath.row][VC_additionalNotes.k_note]
    }
}

