import UIKit

class VC_service: BaseViewController {

    private static let k_opened = "opened"
    private static let k_reason = "reason"
    
    private static let array_services = [[k_opened:"08/22/2019 2:59PM", k_reason:"No Reason"],
                                         [k_opened:"08/22/2019 2:59PM", k_reason:"No Reason"],
                                         [k_opened:"08/22/2019 2:59PM", k_reason:"No Reason"]]
    
    
    
    
    @IBOutlet weak var tabLeView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



//MARK:- UITableViewDataSource
extension VC_service : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VC_service.array_services.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_searchCustomer") as! Cell_searchCustomer
        
        cell.lbl_acctNumb.text = VC_service.array_services[indexPath.row][VC_service.k_opened]
        cell.lbl_customerName.text = VC_service.array_services[indexPath.row][VC_service.k_reason]
        
        return cell
    }
}



//MARK:- UITableViewDelegate
extension VC_service : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.deSelect(table: tableView)
    }
}

