import Foundation
import Alamofire
import Realm
import RealmSwift

// MARK: - TruckInspectionReportItemsRoot
class TruckInspectionReportItemsRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [TruckInspectionReportItems]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [TruckInspectionReportItems]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - Datum
class TruckInspectionReportItems: RealmSwift.Object, Codable {
    @objc dynamic var truckInspectionReportItemID: Int = 0
    @objc dynamic var truckInspectionReportItemCategoryID: Int = 0
    @objc dynamic var item: String?
    @objc dynamic var isChecked = false
    
    enum CodingKeys: String, CodingKey {
        case truckInspectionReportItemID = "truck_inspection_report_item_id"
        case truckInspectionReportItemCategoryID = "truck_inspection_report_item_category_id"
        case item
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "truckInspectionReportItemID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
