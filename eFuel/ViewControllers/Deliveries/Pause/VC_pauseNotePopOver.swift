import UIKit

class VC_pauseNotePopOver: UIViewController {

    @IBOutlet weak var tv_notes: TextView!
    
    weak var delegate_ : PassDataPopoverToParent?
    
    
    //MARK:- Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
//MARK:- Actions
    @IBAction func action_saveNote(_ sender: Any) {
        self.dismiss(animated: true) {[unowned self] in
            if let text = Helper.validateAndTrimText(str: self.tv_notes.text) {
                self.delegate_?.passData(obj: text, vc: self)
            }
        }
    }
}
