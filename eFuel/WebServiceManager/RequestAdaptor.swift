import UIKit
import Alamofire

class RequestInterceptor: RequestAdapter {

    func adapt(_ urlRequest: URLRequest) throws -> URLRequest
    {
        /*
        let reachable = NetworkReachabilityManager()?.isReachable ?? false
        if !reachable{
            throw NSError.NoInternet
        }
        */
        
        
        
        var urlRequest_ = urlRequest
        
        urlRequest_.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest_.setValue("application/json", forHTTPHeaderField: "Accept")
        
        
//
//        let hardCodedToken = #"""
//bearer ddg2SbGX9oSjXee2_z2tyu9LVVa-MegG1RYBGbB_z-TQcSsrozJqH68Y6Zwt3JmaNt7o-JzYxjWVIG3mR85V0VXOyplUQ-Q3nfgDf5Oi2sDhJoBRvubMMpmSOdmJgRKVnlwuOsEt22DA4TYayJQQXHNqdXdrl0eyZndjR3JIQAdcb1qmHfHooBx33h2cErODmCeh5vFHX281B-cFmMKCi1Ll7x4XXLCwL4nX90wdQQE
//"""#
        
        
        /* ahad machine
         bearer ddg2SbGX9oSjXee2_z2tyu9LVVa-MegG1RYBGbB_z-TQcSsrozJqH68Y6Zwt3JmaNt7o-JzYxjWVIG3mR85V0VXOyplUQ-Q3nfgDf5Oi2sDhJoBRvubMMpmSOdmJgRKVnlwuOsEt22DA4TYayJQQXHNqdXdrl0eyZndjR3JIQAdcb1qmHfHooBx33h2cErODmCeh5vFHX281B-cFmMKCi1Ll7x4XXLCwL4nX90wdQQE
         */
        
        
        
        urlRequest_.setValue(Singleton.shared.concatinatedToken, forHTTPHeaderField: "Authorization")
        
        /*
        if let authoCode = UserDetail.shared().authorizationCode {
            urlRequest_.setValue(authoCode, forHTTPHeaderField: "authorizationCode")
        }
        
        
        if let apiK = UserDetail.shared().apiKey {
            urlRequest_.setValue(apiK, forHTTPHeaderField: "apiKey")
        }
        else {
            if let reqToken = UserDetail.shared().requestToken {
                urlRequest_.setValue(reqToken, forHTTPHeaderField: "requestToken")
            }
        }
        */
        
        
        print(#"Header: \#(urlRequest_.allHTTPHeaderFields ?? ["header": "Header is nil"])"#)
        
        return urlRequest_
    }
}
