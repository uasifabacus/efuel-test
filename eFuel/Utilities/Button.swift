import UIKit
import DeviceKit

class Button: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
        print("Device.current: \(Device.current)")
        
        if Device.current == .simulator(.iPadAir) ||
            Device.current == .iPadAir {
            self.titleLabel?.font = UIFont(name: self.titleLabel!.font.fontName, size: 24)
        }
    }
}
