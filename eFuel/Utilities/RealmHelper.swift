import Foundation
import RealmSwift
import Realm


class RealmHelper: NSObject {
    
    static let shared = RealmHelper()
    
    let realm = try! Realm()
    
    var deliveryShiftStatus: DeliveryShiftStatus?
//    var startDataIncompleteFlag = false
    
    var isGPSEnabled : Bool {
        let realm = try! Realm()
        guard let mobileDeliveryShift = realm.objects(MobileDeliveryShift.self).first else { return false }
        guard mobileDeliveryShift.time_in == nil else { return false }
        
        guard let truck = realm.objects(Trucks.self).filter({$0.truckID == mobileDeliveryShift.truck_id.value}).first else { return false }
        
        return (truck.gpsType.value == 1 && truck.gpsCOMPort.value != -1 && truck.gpsCollectionInterval.value ?? 0 > 0)
    }
    
    
    var truckSizeAndNumber : Trucks? {
        let realm = try! Realm()
        guard let mobileDeliveryShift = realm.objects(MobileDeliveryShift.self).first else { return nil }
        guard mobileDeliveryShift.time_in == nil else { return nil }
        
        guard let truck = realm.objects(Trucks.self).filter({$0.truckID == mobileDeliveryShift.truck_id.value}).first else { return nil }
        return truck
    }
    
    
    var selectedTruckDetails : Trucks? {
        let realm = try! Realm()
        guard let mobileDeliveryShift = realm.objects(MobileDeliveryShift.self).first else { return nil }
        guard let truck = realm.objects(Trucks.self).filter({$0.truckID == mobileDeliveryShift.truck_id.value}).first else { return nil }
        return truck
    }
    
    
    
    //MARK:- from Delivery Shift Status
    var efuelMobDelvryShiftIdFromDeliveryShiftStatus : Int? {
        let realm = try! Realm()
        return realm.objects(DeliveryShiftStatus.self).first?.efuel_mobile_delivery_shift_id
    }
    
    var isDisableEfuelMobile: Bool {
        let realm = try! Realm()
        guard let user = realm.objects(LoginUser.self).first else { return false }
        if user.disableEfuelMobile! == "N" {
            return true
        }
        else {
            Helper.showBanner(text: "You are not Authorized to use this App", isSuccess: false)
            return false
        }
    }
    
    
    var mobileDeliveryShiftTruckIdIfExists: Int? {
        let realm = try! Realm()
        return realm.objects(MobileDeliveryShift.self).first?.truck_id.value
    }
    
    
    
    
    
    var mobileDeliveryShiftData: MobileDeliveryShift? {
        let realm = try! Realm()
        return realm.objects(MobileDeliveryShift.self).first
    }
    
    
    
    var isFullShiftSyncEnabled: Bool {
        
        let realm = try! Realm()
        return ((realm.objects(LoginUser.self).first?.enableFullShiftSync?.uppercased())! == "Y")
    }
    
    var opLocationId : Int? {
        let realm = try! Realm()
        return realm.objects(LoginUser.self).first?.defaultOperatingLocationID
    }
    
    var userId : Int? {
        let realm = try! Realm()
        return realm.objects(LoginUser.self).first?.loginUserID
    }
    
    var userDesc : String? {
        let realm = try! Realm()
        return realm.objects(LoginUser.self).first?.loginUserDescr
    }
    
    var mobileDeliveryShiftId : Int? {
        let realm = try! Realm()
        return realm.objects(MobileDeliveryShift.self).first?.mobile_delivery_shift_id
    }
    
    var efuelMobileDeliveryShiftId : Int? {
        let realm = try! Realm()
        return realm.objects(MobileDeliveryShift.self).first?.efuel_mobile_delivery_shift_id.value
    }
    
    
    
    var isDownloadByPassAllowed: Bool {
        let realm = try! Realm()
        guard let user = realm.objects(LoginUser.self).first else { return false }
        return user.allowDownloadBypass?.uppercased() == "Y"
    }
    
    
    var isShiftInProgress: ShiftStatusFlag {
        
        let realm = try! Realm()
        guard let mobileDeliveryShift = realm.objects(MobileDeliveryShift.self).first else { return .noShift }
        let user = realm.objects(LoginUser.self).first!
        
        guard mobileDeliveryShift.time_in == nil  else { return .completeShift }
        
        guard user.loginUserID == mobileDeliveryShift.login_user_id.value else { return .openShiftNewUser }
        return .openShiftOldUser
    }
    
    
    var createNewShift: Bool {

        switch RealmHelper.shared.isShiftInProgress {
        case .err, .noShift, .completeShift, .openShiftNewUser:
            return true
            
        case .openShiftOldUser:
            return false
        }
    }
    
    
    
    
    
    var differenOPLogin: Bool {
        
        let realm = try! Realm()
        guard let defaultOperatingLocationID = realm.objects(LoginUser.self).first?.defaultOperatingLocationID else { return true }
//        guard let existingOPID = realm.objects(Trucks.self).filter("operatingLocationID.value.@max").first?.operatingLocationID else { return true}
        
        let trucks = realm.objects(Trucks.self)
        guard trucks.count > 0 else { return true }
        
        
        let id = trucks.max(ofProperty: "operatingLocationID") as Int?
        let entity = id != nil ? trucks.filter("operatingLocationID == %@", id!).first : nil


        return (entity?.operatingLocationID.value != defaultOperatingLocationID)
//        return (existingOPID.value != defaultOperatingLocationID)
    }
    
    
    var inProgressFlag: Bool {
        
        let realm = try! Realm()
        return (realm.objects(MobileDeliveryShift.self).count > 0)
    }
    
    var startDataIncompleteFlag: Bool {
        
        let realm = try! Realm()
        guard let obj = realm.objects(MobileDeliveryShift.self).first else { return false}
        
        guard obj.time_in == nil else { return false }
        
        return (obj.time_out == nil ||
            obj.totalizer_out.value == nil ||
            obj.odometer_out.value == nil ||
            obj.inventory_out.value == nil)
    }
    
    
    var isDownloadedDataEmpty: Bool {
        
        let realm = try! Realm()
        if realm.objects(CallDetail.self).count > 0 {
            return false
        }
        if realm.objects(CallLog.self).count > 0 {
            return false
        }
        if realm.objects(DeliveryInfo.self).count > 0 {
            return false
        }
        if realm.objects(DeliverySkipCategories.self).count > 0 {
            return false
        }
        if realm.objects(DeliverySkipReasons.self).count > 0 {
            return false
        }
        if realm.objects(InstalltionStreetAddresses.self).count > 0 {
            return false
        }
        if realm.objects(LookUp.self).count > 0 {
            return false
        }
        if realm.objects(TanksPricingCreditMonitorStatus.self).count > 0 {
            return false
        }
        if realm.objects(TruckInspectionReportItems.self).count > 0 {
            return false
        }
        if realm.objects(Trucks.self).count > 0 {
            return false
        }
        if realm.objects(UserAccounts.self).count > 0 {
            return false
        }
        
        
        return true
    }
    
    
    static var retrieveTotalizerVal : Double {
        
        let realm = try! Realm()
        if let mobileDeliveryShift = realm.objects(MobileDeliveryShift.self).first {
            
            guard let totOut = mobileDeliveryShift.totalizer_out.value else { return -1 }
            
            let predicate = NSPredicate(format: "record_type >= 2 and record_type <=4")
            
            let maxOfRecordCreateTime = realm.objects(MobileDeliveryQueue.self).filter(predicate).sorted(byKeyPath: "record_create_time").last
            
            let end_system_net_totalizer = realm.objects(MobileDeliveryQueue.self).filter({$0.record_create_time == maxOfRecordCreateTime?.record_create_time}).first
            
            if let end_system_net_totalizer = end_system_net_totalizer?.end_system_net_totalizer.value {
                return end_system_net_totalizer
            }
            else {
                return totOut
            }
        }
    
    /*
     Step # 01 – query : select totalizer_out from mobile_delivery_shifts
     FYI: if totalizer_out in the table “mobile_delivery_shifts” is NULL then set totalizer.endValue = -1 and return else depend on second step.
     
     Step # 02- query1: SELECT max(record_create_time) FROM mobile_delivery_queue where record_type >= 2 and record_type <=4
     Query2: SELECT end_system_net_totalizer FROM mobile_delivery_queue WHERE record_create_time = {pass date of step#2 query1 here}
     FYI: if end_system_net_totalizer is null then set totalizer.endValue = totalizer_out of step#01 query else set end_system_net_totalizer to totalizer.endValue.
     
     On the shift summary you can see Totalizer value 616 which is coming from the above mentioned query.
     */
    
    return -1
    }
    
    
    
    
//    static var montStatus : [TanksPricingCreditMonitorStatus] {
//        let realm = try! Realm()
//        let userAccounts = realm.objects(UserAccounts.self)
//
//        userAccounts.forEach { (userAccount) in
//            let realm.objects(InstalltionStreetAddresses.self).filter({$0.userAccountID.value == userAccount.userAccountID})
//        }
//    }
    
    
    
    static func truckNumber(truckId: Int? = nil) -> String? {
        
        let realm = try! Realm()
        if let truckId = truckId {
            let obj = realm.objects(Trucks.self).filter({$0.truckID == truckId}).first
            return obj?.truckNumber
        }
        
        let truckId = realm.objects(MobileDeliveryShift.self).first!.truck_id.value
        let obj = realm.objects(Trucks.self).filter({$0.truckID == truckId}).first
        return obj?.truckNumber
    }
    
    
    
    
    static func deleteFromDB<T: Object>(type: T.Type) {
        try! RealmHelper.shared.realm.write {
            RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(type.self))
        }
    }
    
    
    static func uncheckTruckInspectReportItems() {
        
        let realm = try! Realm()
        let array = realm.objects(TruckInspectionReportItems.self)
        for obj in array {
            try! realm.write {
                obj.isChecked = false
            }
        }
    }
}


// MARK:- static functions
extension RealmHelper {
    
    static func storeListToDB(array: [Any], completion: @escaping (_ succeed: Bool) -> Void) {
        
        
        if let array = array as? [CallLog] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(CallLog.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [CallDetail] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(CallDetail.self))
                }
                
                
                array.forEach { (callDetail) in
                    let obj = CallDetail(callDetailID: callDetail.callDetailID,
                                         calllogSQLUid: callDetail.calllogSQLUid,
                                         callNotes: callDetail.callNotes,
                                         loginUserName: callDetail.loginUserName,
                                         callCompletionDate: callDetail.callCompletionDate,
                                         lastInsertUpdateTimeStamp: callDetail.lastInsertUpdateTimeStamp,
                                         callDate: callDetail.callDateFromJSON)
                    
                    try! RealmHelper.shared.realm.write {
                        RealmHelper.shared.realm.add(obj)
                    }
                }
                
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [LookUp] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(LookUp.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [Trucks] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(Trucks.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [UserAccounts] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(UserAccounts.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [InstalltionStreetAddresses] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(InstalltionStreetAddresses.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [TanksPricingCreditMonitorStatus] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(TanksPricingCreditMonitorStatus.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [DeliveryInfo] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliveryInfo.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [DeliverySkipCategories] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    
                    //                    RealmHelper.shared.realm.objects(DeliverySkipCategories.self).forEach({ (obj) in
                    //                        RealmHelper.shared.realm.delete(obj.reasons)
                    //                    })
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliverySkipReasons.self))
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliverySkipCategories.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [DeliverySkipReasons] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliverySkipReasons.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
        else if let array = array as? [TruckInspectionReportItems] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(TruckInspectionReportItems.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
            
        else if let array = array as? [DeliveryQueue] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliveryQueue.self))
                }
                
                
                array.forEach { (deliveryQueue) in
                    try! RealmHelper.shared.realm.write {
                        RealmHelper.shared.realm.add(deliveryQueue, update: Realm.UpdatePolicy.all)
                    }
                }
                
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
            
            
        else if let array = array as? [MobileDeliveryTicket] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(MobileDeliveryTicket.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array, update: Realm.UpdatePolicy.all)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
            
        else if let array = array as? [MobileDeliverySurchargeLabels] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(MobileDeliverySurchargeLabels.self))
                }
                
                
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(array)
                }
                
                completion(true)
                
            } catch {
                print("failed to convert data")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
            
            
        else if let array = array as? [DeliveryShiftStatus] {
            
            do {
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliveryShiftStatus.self))
                }
                
                
                let obj = array[0]
//                obj.efuel_mobile_delivery_shift_id = obj.IncrementaID()
                
                try RealmHelper.shared.realm.write {
                    RealmHelper.shared.realm.add(obj)
                }
                
                completion(true)
                
            } catch {
                print("error in db: \(error.localizedDescription)")
                //                Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                completion(false)
            }
        }
            
        else { completion(false) }
    }
    
    
    
    
    
    
    
    static func removePreviousDownloadedData() {
        
        do {
            try RealmHelper.shared.realm.write {
                
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(CallDetail.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(CallLog.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliveryInfo.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliveryQueue.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliverySkipReasons.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(DeliverySkipCategories.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(InstalltionStreetAddresses.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(LookUp.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(TanksPricingCreditMonitorStatus.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(TruckInspectionReportItems.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(Trucks.self))
                RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(UserAccounts.self))
            }
        } catch {
            print(error.localizedDescription)
            Helper.showBanner(text: error.localizedDescription, isSuccess: false)
        }
    }
    
    
    
    
    
    
    static func isUserAtLocalDB(userName: String, password: String) -> Bool {
        
        return RealmHelper.shared.realm.objects(LoginUser.self).filter({$0.loginUserName == userName && $0.loginUserPassword == password}).count > 0
    }
    
    
    
    
//    static func findShiftStatusInfo(last_manual_sale_number: inout Int? = nil, generateOne: Bool) -> ShiftExistingFlag? {
    static func findShiftStatusInfo(last_manual_sale_number: inout Int, generateOne: Bool) -> ShiftExistingFlag? {
        
        guard let obj = RealmHelper.shared.realm.objects(DeliveryShiftStatus.self).first else { return nil }
        
        try! RealmHelper.shared.realm.write {
            obj.status.trim()
        }
        
        
        let enum_ = SiftStatus(rawValue: obj.status)
        switch enum_ {
        case .login?:
            return .login
            
        case .truck?:
            return .truck
            
        case .new_shift_id?:
            
            if last_manual_sale_number == 0 {
                
                var iLast_manual_sale_number = obj.last_manual_sale_number
                
                if generateOne {
                    
                    iLast_manual_sale_number += 1
                    
                    if iLast_manual_sale_number > 999999 { iLast_manual_sale_number = 1 }
                    
                    try! RealmHelper.shared.realm.write {
                        obj.last_manual_sale_number = iLast_manual_sale_number
                    }
                }
                
                last_manual_sale_number = iLast_manual_sale_number
                
                return .last_manual_sale_number
            }
            else {
                
                RealmHelper.shared.deliveryShiftStatus = obj
                return .newId
            }
            
            
        default:
            Helper.showBanner(text: "Unable to detect Shift Status", isSuccess: false)
            return nil
        }
    }
    
    
    
    static func writeLogToDB (isSucceed:String, reason: String) {
        
        print("writeLogToDB")
        let realm = try! Realm()
        let obj = MobileSyncLog()
        obj.sync_log_id = obj.IncrementaID()
        obj.truck_id.value = RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists
        obj.operating_location_id.value = RealmHelper.shared.opLocationId
        obj.login_user_id.value = realm.objects(LoginUser.self).first?.loginUserID
        obj.sync_type = "Standard"
        obj.sync_status = isSucceed
        obj.sync_time = Helper.mmDDYYWithCurrentTime
        obj.sync_failure_reason = reason
        
        
        do {
            try realm.write {
                realm.add(obj)
            }
        } catch {
            print("error\(error)")
        }
    }
    
    
    
    static func GPSDataLog(lat: Double, long: Double) {
        print("GPSDataLog")
        let obj = MobileGPSData()
        obj.truck_id.value = RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists
        obj.operating_location_id.value = RealmHelper.shared.opLocationId
        obj.login_user_id.value = RealmHelper.shared.userId
        obj.gps_data_id = obj.IncrementaID
        obj.gps_data_time_stamp = Helper.mmDDYYWithCurrentTime
        obj.geocode_lat.value = lat
        obj.geocode_long.value = long
        obj.direction = nil
        obj.no_gps_data_timestamp = nil
        
        let realm = try! Realm()
        do {
            try realm.write {
                realm.add(obj)
            }
        } catch {
            print("error\(error)")
        }
    }
    
    
    static func createMobileEventLog(eventName: EventLogCases,
                               startTime: String? = nil,
                               endTime: String? = nil,
                               note: String? = nil,
                               lat: Double? = nil,
                               long: Double? = nil,
                               gallons: Double? = nil,
                               cost: Double? = nil,
                               queueId: Int? = nil,
                               efuelMobileDeliveryShiftId: Int? = nil) {
        
        //event name, gallons, cost, event notes, 
        
        print("mobileEventLog")
        
        let obj = MobileEventLog()
        obj.mobile_event_log_id = obj.IncrementaID()
        obj.event_name = eventName.rawValue
        obj.event_start_time = startTime
        obj.event_end_time = endTime
        obj.event_notes = note
        obj.gallons.value = gallons
        obj.cost.value = cost
        obj.geocode_Lat.value = lat
        obj.geocode_long.value = long
        obj.efuel_mobile_delivery_queue_id.value = queueId
        obj.efuel_mobile_sw_version.value = k_swVersion
        obj.efuel_mobile_delivery_shift_id.value = efuelMobileDeliveryShiftId //ye DeliveryShiftStatus.efuel_mobile_delivery_shift_id hogi
        obj.operating_location_id.value = RealmHelper.shared.opLocationId
        obj.truck_id.value = RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists
        obj.login_user_id.value = RealmHelper.shared.userId
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(obj)
            }
        } catch {
            print("error\(error)")
        }
    }
    
    
    @discardableResult
    static func createOrUpdateMobileDeliveryShifts (timeOut: String? = nil,
                                            timeIn: String? = nil,
                                            odometer_out: Double? = nil,
                                            odometer_in: Double? = nil,
                                            totalizer_out: Double? = nil,
                                            totalizer_in: Double? = nil,
                                            inventory_out: Double? = nil,
                                            inventory_in: Double? = nil,
                                            truckId: Int) -> Bool {
        
        let realm = try! Realm()
        let efuel_mobile_delivery_shift_id = realm.objects(DeliveryShiftStatus.self).first?.efuel_mobile_delivery_shift_id
        
        
        var obj: MobileDeliveryShift!

        if let objExists = realm.objects(MobileDeliveryShift.self).first {
            obj = objExists
        }
        else {
            obj = MobileDeliveryShift()
            obj.mobile_delivery_shift_id = obj.IncrementaID
            obj.efuel_mobile_delivery_shift_id.value = efuel_mobile_delivery_shift_id
            obj.truck_id.value = truckId
            obj.login_user_id.value = RealmHelper.shared.userId
            obj.login_user_descr = RealmHelper.shared.userDesc
        }
        
        
        
        
        
        
        
        do {
            try realm.write {
                
                if let timeOut = timeOut {
                    obj.time_out = timeOut
                }
                
                if let timeIn = timeIn {
                    obj.time_in = timeIn
                }
                
                if let odoMeterOut = odometer_out {
                    obj.odometer_out.value = odoMeterOut
                }
                
                if let odoMeterIn = odometer_in {
                    obj.odometer_in.value = odoMeterIn
                }
                
                if let totoOut = totalizer_out {
                    obj.totalizer_out.value = totoOut
                }
                
                if let totIn = totalizer_in {
                    obj.totalizer_in.value = totIn
                }
                
                if let inventoryOut = inventory_out {
                    obj.inventory_out.value = inventoryOut
                }
                
                if let inventoryIn = inventory_in {
                    obj.inventory_in.value = inventoryIn
                }
                
                
                realm.add(obj, update: .modified)
            }
            
            return true
        }
        
        catch {
            print("error\(error)")
            
            let predicate = NSPredicate(format: "efuel_mobile_delivery_shift_id = %@ AND truck_id = %@", efuel_mobile_delivery_shift_id!, truckId)
            let objsToDel = realm.objects(MobileDeliveryShift.self).filter(predicate)
            realm.delete(objsToDel)
            
            return false
        }
    }
    
    
    
    static func createLoadUnloadTruck(isLoadTruck: Bool,
                                      RW_after: Double,
                                      RW_before: Double,
                                      DW_before: Double,
                                      DW_after: Double,
                                      startTime: String,
                                      wareHouseCode: String,
                                      odoMeterVal: Double,
                                      totalizerEnd: Double) {
        
        let realm = try! Realm()
        
        let obj = MobileDeliveryQueue()
        obj.mobile_delivery_queue_id = obj.IncrementaID
        obj.truck_id.value = RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists
        obj.login_user_id.value = RealmHelper.shared.userId
        obj.created_by = "EfuelMobile"
        obj.efuel_mobile_delivery_shift_id.value = RealmHelper.shared.mobileDeliveryShiftId
        obj.efuel_mobile_delivery_queue_id.value = obj.efuelMobileDeliveryQueueId
        
        if isLoadTruck {

            obj.record_type.value = RecordType.LoadTruck.rawValue
            obj.units_delivered.value = (RealmHelper.shared.truckSizeAndNumber?.capacityGallons.value)! * (RW_after - RW_before) / 100
        }
        else {
            
            obj.record_type.value = RecordType.unLoadTruck.rawValue
            obj.units_delivered.value = (RealmHelper.shared.truckSizeAndNumber?.capacityGallons.value)! * (DW_before - DW_after) / 100
        }
        obj.start_time = startTime
        obj.end_time = Helper.mmDDYYWithCurrentTime
        obj.record_create_time = obj.end_time
        obj.warehouse_code = wareHouseCode
        obj.percent_receiving_warehouse_start.value = RW_before
        obj.percent_receiving_warehouse_end.value = RW_after
        obj.percent_delivering_warehouse_start.value = DW_before
        obj.percent_delivering_warehouse_end.value = DW_after
        obj.current_odometer.value = odoMeterVal
        obj.start_system_net_totalizer.value = totalizerEnd
        
        do {
            try realm.write {
                realm.add(obj)
            }
        } catch  {
            print("createLoadUnloadTruck error : \(error.localizedDescription)")
        }
    }
    
    
    static func createOrUpdateMobileAppStatus(last_inventory_startValue: Double? = nil, last_inventory_startQty: Double? = nil) {
        
        var obj: MobileAppStatus!
        let realm = try! Realm()
        
        if let objExists = realm.objects(MobileAppStatus.self).first {
            obj = objExists
        }
        else {
            obj = MobileAppStatus()
            obj.mobile_app_status_id = obj.IncrementaID
        }
        
        
        
        
        do {
            try realm.write {
                
                if let lastInventoryStartValue = last_inventory_startValue {
                    obj.last_inventory_startValue.value = lastInventoryStartValue
                }
                
                if let lastInventoryStartQty = last_inventory_startQty {
                    obj.last_inventory_startQty.value = lastInventoryStartQty
                }
                
                
                realm.add(obj, update: .modified)
            }
        }

        catch {
            print("createOrUpdateMobileAppStatus db error\(error.localizedDescription)")
        }
    }
    
    
    
    static func fetchUsersByTankId(remaining_Skip_Completed : inout [Remaining_Skip_Completed])  {
        
        let realm = try! Realm()
        
        for (index, r_S_C) in remaining_Skip_Completed.enumerated() {
            
            if let tanksPricingCreditMonitorStatus = realm.objects(TanksPricingCreditMonitorStatus.self).filter("tankID = %@", r_S_C.tank_id!).filter({$0.owners.count > 0}).first {
                
                extractTankPricing(obj: tanksPricingCreditMonitorStatus, mainArrayObj: &remaining_Skip_Completed[index])
                
                if let installtionStreetAddress = tanksPricingCreditMonitorStatus.owners.first {
                    
                    extractStreetAddress(obj: installtionStreetAddress, mainArrayObj: &remaining_Skip_Completed[index])
                    
                    if let userAccount = installtionStreetAddress.owners.first {

                        extractUserAccount(obj: userAccount, mainArrayObj: &remaining_Skip_Completed[index])
                    }
                }
            }
        }
    }
    
    
    
    static private func extractTankPricing(obj: TanksPricingCreditMonitorStatus, mainArrayObj: inout Remaining_Skip_Completed) {
        mainArrayObj.tank_num = obj.tankNum.value
        mainArrayObj.serialNumOnTank = obj.serialNumOnTank
        mainArrayObj.stopNumber = obj.stopNumber.value
        mainArrayObj.tankSizeGals = obj.tankSizeGals.value
        mainArrayObj.willCallFlag = obj.willCallFlag
        mainArrayObj.sales_tax = obj.salesTax.value
        mainArrayObj.minDelDollars = obj.minDelDollars.value
        mainArrayObj.surcharge_1 = obj.surcharge1.value
        mainArrayObj.surcharge_2 = obj.surcharge2.value
        mainArrayObj.surcharge_3 = obj.surcharge3.value
        mainArrayObj.regulatory_fee = obj.regulatoryFee.value
        mainArrayObj.price_per_unit = obj.pricePerUnit.value
        mainArrayObj.credit_status_code = obj.creditStatusCode.value
        mainArrayObj.credit_status_description = obj.creditStatusDescription
        mainArrayObj.terms_code = obj.termsCode.value
        mainArrayObj.terms_descriptions = obj.termsDescriptions
//        mainArrayObj.desc = obj.unitsOfMeasureDescr
//        mainArrayObj.unit_of_measure_id = obj.unitOfMeasureID.value

        mainArrayObj.product_type = obj.productType.value

//        mainArrayObj.terms_credit_limit = obj.termsCreditLimit.value
//        mainArrayObj.ageing_1 = obj.ageing1.value
//        mainArrayObj.ageing_2 = obj.ageing2.value
//        mainArrayObj.ageing_3 = obj.ageing3.value
//        mainArrayObj.ageing_4 = obj.ageing4.value
//        mainArrayObj.ageing_current = obj.ageingCurrent.value
//        mainArrayObj.balance = obj.balance.value
//        mainArrayObj.balance_date = obj.balanceDate
//        mainArrayObj.last_payment_amt = obj.lastPaymentAmt.value
//        mainArrayObj.last_payment_date = obj.lastPaymentDate
//        mainArrayObj.comm_link_alert = obj.commLinkAlert
//        mainArrayObj.inside_temp_alert = obj.insideTempAlert
        mainArrayObj.low_level_alert = obj.lowLevelAlert
        mainArrayObj.unusual_usage_alert = obj.unusualUsageAlert
        mainArrayObj.sensor_status = obj.sensorStatus
        mainArrayObj.fill_trigger_last_24_hours = obj.fillTriggerLast24_Hours
        mainArrayObj.float_level = obj.floatLevel.value
        mainArrayObj.GM_last_callin_local_time_at_GM_location = obj.gmLastCallinLocalTimeAtGMLocation
        mainArrayObj.reading_time = obj.readingTime
        mainArrayObj.GWM_installation_id = obj.gwmInstallationID.value
        mainArrayObj.DM_installation_id = obj.dmInstallationID.value
        mainArrayObj.last_delivery_time = obj.lastDeliveryTime
        mainArrayObj.units_delivered = obj.unitsDelivered.value
    }
    
    
    static private func extractStreetAddress(obj: InstalltionStreetAddresses, mainArrayObj: inout Remaining_Skip_Completed) {
        
        mainArrayObj.installation_street_address_line_1 = obj.installationStreetAddressLine1
        mainArrayObj.installation_city = obj.installationCity
        mainArrayObj.installation_state = obj.installationState
        mainArrayObj.installation_zip = obj.installationZip
        mainArrayObj.installation_street_address_id = obj.installationStreetAddressID
        mainArrayObj.installation_GeoCodeLat = obj.installationGeoCodeLat.value
        mainArrayObj.installation_GeoCodeLong = obj.installationGeoCodeLong.value
    }
    
    
    static private func extractUserAccount(obj: UserAccounts, mainArrayObj: inout Remaining_Skip_Completed) {
        
        mainArrayObj.user_account_name = obj.userAccountName
        mainArrayObj.account_num = obj.accountNum.value
//        mainArrayObj.user_account_id = obj.userAccountID
//        mainArrayObj.main_phone = obj.mainPhone
        mainArrayObj.main_street_address_1 = obj.mainStreetAddress1
        mainArrayObj.main_city = obj.mainCity
        mainArrayObj.main_state = obj.mainState
        mainArrayObj.mainPostalCode = obj.mainPostalCode
    }
}

/*
 t.tank_num
 t.tank_id
 t.serial_num_on_tank
 t.stop_number
 t.tank_size_gals
 t.will_call_flag
 t.sales_tax
 t.min_del_dollars
 t.surcharge_1
 t.surcharge_2
 t.surcharge_3
 t.regulatory_fee
 t.price_per_unit
 t.credit_status_code
 t.credit_status_description
 t.terms_code
 t.terms_descriptions
 t.units_of_measure_descr
 t.unit_of_measure_id
 t.product_type
 t.terms_credit_limit
 t.ageing_1
 t.ageing_2
 t.ageing_3
 t.ageing_4
 t.ageing_current
 t.balance
 t.balance_date
 t.last_payment_amt
 t.last_payment_date
 t.comm_link_alert
 t.inside_temp_alert
 t.low_level_alert
 t.unusual_usage_alert
 t.sensor_status
 t.fill_trigger_last_24_hours
 t.float_level
 t.GM_last_callin_local_time_at_GM_location
 t.reading_time
 t.GWM_installation_id
 t.DM_installation_id
 t.last_delivery_time
 t.units_delivered
 */
