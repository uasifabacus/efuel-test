import UIKit

class VC_menu: BaseViewController {

    @IBOutlet weak var btn_start_end_shift: UIButton!
    @IBOutlet weak var btn_loadTruck: UIButton!
    @IBOutlet weak var btn_deliveries: UIButton!
    @IBOutlet weak var btn_utilities: UIButton!
    
    
    var isShiftOpened: Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isShiftOpened == true {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_shiftSummary") as! VC_shiftSummary
            vc.isShiftOpened = self.isShiftOpened
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.enableDisableButtons()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? VC_shiftSummary {
            vc.isShiftOpened = self.isShiftOpened
        }
    }
    
    private func enableDisableButtons() {
        
        if RealmHelper.shared.isShiftInProgress == .openShiftOldUser {       // shift already in progress

            self.btn_start_end_shift.setImage(UIImage(named: "end-shift-btn"), for: .normal)
            self.btn_loadTruck.isEnabled = true
            self.btn_deliveries.isEnabled = true
            self.btn_utilities.isEnabled = true
        }
        else {
            self.btn_start_end_shift.setImage(UIImage(named: "start-shift-btn"), for: .normal)
            self.btn_loadTruck.isEnabled = false
            self.btn_deliveries.isEnabled = false
            self.btn_utilities.isEnabled = false
        }
    }
    
    
    
    //MARK:- actions
    @IBAction func action_start_end_shift(_ sender: Any) {
        
        switch RealmHelper.shared.isShiftInProgress {
        
        case .noShift:
            self.performSegue(withIdentifier: "segue_showTrucks", sender: nil)
            break
        case .openShiftOldUser:
            self.performSegue(withIdentifier: "segue_VC_shiftSummary", sender: nil)
            break
        default:
            Helper.showBanner(text: "Shift Error", isSuccess: false)
        }
    }
    
    
    @IBAction func action_logout_quit(_ sender: Any) {
        
        Singleton.stopTimmers()
        exit(0)
    }
    
    
    @IBAction func action_deliveries(_ sender: Any) {
        
        Singleton.startTimmers()
        
        self.performSegue(withIdentifier: "segue_deliveryList", sender: nil)
    }
}
