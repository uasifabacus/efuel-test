import Foundation
import Alamofire
import RealmSwift
import Realm

// MARK: - CallLog
class CallLogRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [CallLog]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [CallLog]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - CallLog
class CallLog: RealmSwift.Object, Codable {
    @objc dynamic var sqlUid: Int = 0
    var userAccountID = RealmOptional<Int>()
    var loginUserID = RealmOptional<Int>()
    var callStatus = RealmOptional<Int>()
    var whoNext = RealmOptional<Int>()
    var assignedTo = RealmOptional<Int>()
    var installDetailID = RealmOptional<Int>()
    var installationStreetAddressID = RealmOptional<Int>()
    var tankID = RealmOptional<Int>()
    var efuelMobileCallLogID = RealmOptional<Int>()
    @objc dynamic var callNotes, callAction, callType, reason, callDate, isThisOpen, openedBy, closedBy, loginUserName, followupDate, lastInsertUpdateTimeStamp: String?
    
    enum CodingKeys: String, CodingKey {
        case sqlUid = "SQL_UID"
        case userAccountID = "user_account_id"
        case callDate = "call_date"
        case loginUserID = "login_user_id"
        case callStatus = "call_status"
        case callNotes = "call_notes"
        case callAction = "call_action"
        case callType = "call_type"
        case reason
        case whoNext = "who_next"
        case assignedTo = "assigned_to"
        case isThisOpen = "is_this_open"
        case installDetailID = "install_detail_id"
        case installationStreetAddressID = "installation_street_address_id"
        case tankID = "tank_id"
        case openedBy = "opened_by"
        case closedBy = "closed_by"
        case loginUserName = "login_user_name"
        case followupDate = "followup_date"
        case lastInsertUpdateTimeStamp = "last_insert_update_time_stamp"
        case efuelMobileCallLogID = "efuel_mobile_call_log_id"
    }
    
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "sqlUid"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
