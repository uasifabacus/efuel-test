import UIKit
import RealmSwift
import MessageUI
import SwiftDate

class VC_shiftSummary: BaseViewController {
    
    
    var isShiftOpened : Bool = false
    
    var truck: Trucks!
    
    @IBOutlet weak var lbl_date_start: Label!
    @IBOutlet weak var lbl_date_end: Label!
    
    @IBOutlet weak var lbl_time_start: Label!
    @IBOutlet weak var lbl_time_end: Label!
    
    @IBOutlet weak var lbl_totalizer_start: Label!
    @IBOutlet weak var lbl_totalizer_end: Label!
    
    @IBOutlet weak var TF_inventory_start: TextField!
    @IBOutlet weak var TF_inventory_end: TextField!
    
    @IBOutlet weak var TF_odometer_start: TextField!
    @IBOutlet weak var TF_odometer_end: TextField!
    
    
    @IBOutlet weak var btn_startOrEndShift: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Shift Summary"
        self.setupUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addNotifier()
    }
    
    
    private func setupUI() {
        
        
        self.TF_odometer_start.isEnabled = false
        self.TF_odometer_end.isEnabled = false
        self.TF_inventory_start.isEnabled = false
        self.TF_inventory_end.isEnabled = false
        
        
        self.TF_odometer_start.text = ""
        self.TF_odometer_end.text = ""
        self.TF_inventory_start.text = ""
        self.TF_inventory_end.text = ""
        
        
        if isShiftOpened {
            self.navigationItem.hidesBackButton = true
        }
        
        //        if !RealmHelper.shared.inProgressFlag || RealmHelper.shared.startDataIncompleteFlag {
        
        self.lbl_date_end.text = ""
        self.lbl_time_end.text = ""
        self.lbl_date_start.text = ""
        self.lbl_time_start.text = ""
        
        if RealmHelper.shared.isShiftInProgress == .openShiftOldUser {
            
            if let obj = RealmHelper.shared.mobileDeliveryShiftData {

                if let totStart = obj.totalizer_out.value {
                    self.lbl_totalizer_start.text = totStart.string
                }
                else {
                    self.lbl_totalizer_start.text = "0"
                }

                self.TF_odometer_start.text = obj.odometer_out.value?.string
                self.TF_inventory_start.text = obj.inventory_out.value?.string
                
                let date = DateInRegion(obj.time_out!, region: Region.local)
                self.lbl_date_start.text = date?.toFormat(k_MMDDYY)
                self.lbl_time_start.text = date?.toFormat(k_HHMMA)
            }
            
            
            
            self.TF_inventory_end.isEnabled = true
            self.TF_inventory_end.becomeFirstResponder()
            self.TF_odometer_end.isEnabled = true
            self.btn_startOrEndShift.setImage(UIImage(named: "end-shift-btn"), for: .normal)
            
        }
        else {
            
            self.TF_inventory_start.isEnabled = true
            self.TF_inventory_start.becomeFirstResponder()
            self.TF_odometer_start.isEnabled = true
            self.lbl_totalizer_start.text = RealmHelper.retrieveTotalizerVal.string
            self.btn_startOrEndShift.setImage(UIImage(named: "start-shift-btn"), for: .normal)
        }
    }
    
    
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    @IBAction func action_continue(_ sender: Any) {
        
        guard let odoStartStr = Helper.validateAndTrimText(TF: self.TF_odometer_start), let odoStart = odoStartStr.double() else {
            Helper.showBanner(text: "Invalid Odometer value", isSuccess: false)
            return
        }
        
        guard let inventoryStartStr = Helper.validateAndTrimText(TF: self.TF_inventory_start),
            let inventoryStartDouble = inventoryStartStr.double()  else {
                Helper.showBanner(text: "Invalid Inventory Start %", isSuccess: false)
                return
        }
        
        guard inventoryStartDouble > 0 && inventoryStartDouble <= 80 else {
            Helper.showBanner(text: "Inventory start percentage Out of range, value should be greater than 0 and less than equal to 80 ", isSuccess: false)
            return
        }
        
        if RealmHelper.shared.isShiftInProgress == .noShift {
            
            RealmHelper.createOrUpdateMobileDeliveryShifts(timeOut: Helper.mmDDYYWithCurrentTime,
                                                           odometer_out: odoStart,
                                                           totalizer_out: self.lbl_totalizer_start!.text?.double(),
                                                           inventory_out: inventoryStartDouble,
                                                           truckId: self.truck.truckID)
            
            
            WebMethods.upload_mobile_delivery_shift(shouldRemoveMobDeliveryShiftFromLocal: false, completion: { [weak self] (isSucceed) in
                
                Helper.hideHud()
                
                guard let self_ = self, isSucceed else {
                    Helper.showBanner(text: "Error in upload Mobile Delivery Shift", isSuccess: false)
                    return
                }
                self_.goBack()
            })
        }
        else {
            
            guard let odoEndString = Helper.validateAndTrimText(TF: self.TF_odometer_end), let odoEnd = odoEndString.double() else {
                Helper.showBanner(text: "Invalid Odometer value", isSuccess: false)
                return
            }
            
            let difference = odoEnd - odoStart
            
            guard difference > 0 && difference <= 200 else {
                self.TF_odometer_start.isEnabled = true
                self.TF_inventory_end.isEnabled = false
                Helper.showBanner(text: "Total miles should be greater than 0 and less than equal to 200 ", isSuccess: false)
                return
            }
            
            guard let inventoryEndString = Helper.validateAndTrimText(TF: self.TF_inventory_end, message: "Invalid Inventory End %"),
                let inventoryEndDouble = inventoryEndString.double()  else { return }
            
            guard inventoryEndDouble > 0 && inventoryEndDouble <= 80 else {
                Helper.showBanner(text: "Inventory start percentage Out of range, value should be greater than 0 and less than equal to 80 ", isSuccess: false)
                return
            }
            
            
            RealmHelper.createOrUpdateMobileDeliveryShifts(timeIn: Helper.mmDDYYWithCurrentTime,
                                                           odometer_in: odoEnd,
                                                           totalizer_in: self.lbl_totalizer_end!.text?.double(),
                                                           inventory_in: inventoryEndDouble,
                                                           truckId: RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists!)
            
            
            
            WebMethods.update_mobile_delivery_shift { [weak self] (isSucceed) in
                
                Helper.hideHud()
                guard let self_ = self else { return }
                if isSucceed {
                    
                    /*
                     self_.isPrinterFoundForShiftSummary { (isSucceed) in
                     if isSucceed {
                     // Send to printer & after it call self_.uploadShift()
                     }
                     else {
                     self_.uploadShift()
                     }
                     }
                     */
                    
                    self_.printLoadUnloadSummary(message: "Do you want to Print an End of Shift Summary?") { (isSucceed, deviceType) -> (Void) in
                        
                        if isSucceed {
                            if deviceType == .simulator {
                                if self_.email(str: self_.printShiftSummaryReport, vc: self_) == false {
                                    self_.uploadShift()
                                }
                            }
                            else if deviceType == .LCR {
                                // LCR
                            }
                            else {      // it means, User don't want to print
                                self_.uploadShift()
                            }
                        }
                        else {
                            exit(0)
                        }
                    }
                }
            }
        }
    }
    
    
    
    private func uploadShift() {
        
        self.showAlert(title: nil,
                       message: "You must upload your End of Shift data before you can start a new shift.if you would like to upload it now, please connect to the internet, then press “YES”. Otherwise press “No” and you can upload at later time",
                       buttonTitles: ["Yes", "No"],
                       highlightedButtonIndex: 0) { (buttonTag) in
                        
                        if buttonTag == 0 {
                            
                            WebMethods.uploadData(shouldUploadMobileDelvryShift: false, isOnlyUpload: true) { [weak self] (isSucceed) in
                                Helper.hideHud()
                                guard let self_ = self, isSucceed else { return }
                                
                                Singleton.stopTimmers()
                                RealmHelper.deleteFromDB(type: MobileDeliveryShift.self)
                                self_.goBack()
                            }
                        }
                        else { self.dismiss(animated: true) }       // go to login screen
        }
    }
    
    
    
    private func goBack() {
        guard isShiftOpened == false else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
    }
}



//MARK:- VC_shiftSummary MFMailComposeViewControllerDelegate
extension VC_shiftSummary : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
            self.uploadShift()
        }
    }
}
