import Foundation
import Alamofire
import RealmSwift
import Realm

// MARK: - MobileDeliverySurchargeLabelsRoot
class MobileDeliverySurchargeLabelsRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [MobileDeliverySurchargeLabels]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [MobileDeliverySurchargeLabels]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}


// MARK: - Datum
class MobileDeliverySurchargeLabels: RealmSwift.Object, Codable {
    
    @objc dynamic var surcharge0_Label, surcharge1_Label, surcharge2_Label, surcharge3_Label: String?
    
    var surcharge0_Taxable = RealmOptional<Int>()
    var surcharge1_Taxable = RealmOptional<Int>()
    var surcharge2_Taxable = RealmOptional<Int>()
    var surcharge3_Taxable = RealmOptional<Int>()
    
    enum CodingKeys: String, CodingKey {
        case surcharge0_Label = "surcharge_0_label"
        case surcharge1_Label = "surcharge_1_label"
        case surcharge2_Label = "surcharge_2_label"
        case surcharge3_Label = "surcharge_3_label"
        case surcharge0_Taxable = "surcharge_0_taxable"
        case surcharge1_Taxable = "surcharge_1_taxable"
        case surcharge2_Taxable = "surcharge_2_taxable"
        case surcharge3_Taxable = "surcharge_3_taxable"
    }

    
    
    //MARK:- realm
//    override static func primaryKey() -> String? {
//        return "deliveryQueueId"
//    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
