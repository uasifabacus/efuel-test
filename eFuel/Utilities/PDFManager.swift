import Foundation
import PDFGenerator
import SwiftDate


class PDFManager {
    static func makePDFAndSendToPrinter(view: UIView) {
        
        let fileName = "\(String(describing: Date().dateString())).pdf"
        
        let dst = URL(fileURLWithPath: NSTemporaryDirectory().appending(fileName))
        // outputs as Data
        do {
            let data = try PDFGenerator.generated(by: [view])
            
            
            if UIPrintInteractionController.canPrint(data) {
                let printInfo = UIPrintInfo(dictionary: nil)
                printInfo.jobName = fileName
                printInfo.outputType = .general
                
                let printController = UIPrintInteractionController.shared
                printController.printInfo = printInfo
                printController.showsNumberOfCopies = false
                
                printController.printingItem = data
                
                printController.present(animated: true, completionHandler: nil)
            }
            
            
            
//            try data.write(to: dst, options: .atomic)
        } catch (let error) {
            print(error)
        }
        
        
        return
        
        
        
        
//        let dst = URL(fileURLWithPath: NSTemporaryDirectory().appending("pdf.pdf"))
        // writes to Disk directly.
        do {
            try PDFGenerator.generate([view], to: dst)
        } catch (let error) {
            print(error)
        }
    }
}
