import Foundation
import Alamofire
import Realm
import RealmSwift

// MARK: - MobileDeliveryTicketRoot
class MobileDeliveryTicketRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [MobileDeliveryTicket]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
}


class MobileDeliveryTicket: RealmSwift.Object, Codable {
    @objc dynamic var mobileDeliveryTicketID: Int = 0
    var ticketSection = RealmOptional<Int>()
    var termsCode = RealmOptional<Int>()
    @objc dynamic var textToPrint: String?
    
    enum CodingKeys: String, CodingKey {
        case mobileDeliveryTicketID = "mobile_delivery_ticket_id"
        case ticketSection = "ticket_section"
        case termsCode = "terms_code"
        case textToPrint = "text_to_print"
    }
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "mobileDeliveryTicketID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
