//
//  UIViewController+General.swift
//  eFuel
//
//  Created by naveed durrani on 02/10/2019.
//  Copyright © 2019 Naveed Durrani. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func popOverSetting(widthPercent: CGFloat, heightPercent: CGFloat) {
        
        //            vc.modalPresentationStyle = UIModalPresentationStyle.formSheet
        let percentWidth = (widthPercent * self.view.frame.size.width)/100
        let percentHeight = (heightPercent * self.view.frame.size.height)/100
        self.view.backgroundColor = .lightGray
        self.preferredContentSize = CGSize(width: percentWidth, height:percentHeight) // IMPORTANT
        self.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        //            vc.popoverPresentationController?.sourceView = self.view
        
        
        self.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
    }
}
