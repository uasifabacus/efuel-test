import UIKit

class VC_newCaseReason: BaseViewController {

    
    
    private static let array_reasons = ["No reason",
                                        "Equipment arrived broken",
                                        "Problem with data module",
                                        "ye sab Database sy aengy, aur DB me server sy aengy"]
    
    //MARK:- Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}




//MARK:- UITableViewDataSource
extension VC_newCaseReason : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VC_newCaseReason.array_reasons.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_newCaseReason", for:indexPath) as! Cell_newCaseReason
        cell.setupUI(str: VC_newCaseReason.array_reasons[indexPath.row])
        return cell
    }
}
