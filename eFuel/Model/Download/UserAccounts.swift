import Foundation
import Alamofire
import RealmSwift
import Realm

// MARK: - UserAccountsRoot
class UserAccountsRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [UserAccounts]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [UserAccounts]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}


// MARK: - UserAccounts
class UserAccounts: RealmSwift.Object, Codable {
    @objc dynamic var userAccountID: Int = 0
    var accountNum = RealmOptional<Int>()
//    @objc dynamic var customerID: Int = -1
    @objc dynamic var mainPhone, mainStreetAddress1, mainStreetAddress2, mainCity, mainState, mainPostalCode, userAccountName, customerID: String?
    var installationStreetAddresses = List<InstalltionStreetAddresses>()
    
    enum CodingKeys: String, CodingKey {
        case userAccountID = "user_account_id"
        case userAccountName = "user_account_name"
        case accountNum = "account_num"
        case mainPhone = "main_phone"
        case mainStreetAddress1 = "main_street_address_1"
        case mainStreetAddress2 = "main_street_address_2"
        case mainCity = "main_city"
        case mainState = "main_state"
        case mainPostalCode = "main_postal_code"
        case customerID = "customer_id"
    }
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "userAccountID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
