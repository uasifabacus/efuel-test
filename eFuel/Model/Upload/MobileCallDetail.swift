import UIKit
import Realm
import RealmSwift

class MobileCallDetail: RealmSwift.Object, Codable {

    @objc dynamic var mobile_call_detail_id: Int = 0
    var efuel_mobile_call_detail_id = RealmOptional<Int>()
    var login_user_id = RealmOptional<Int>()
    var efuel_mobile_call_log_id = RealmOptional<Int>()
    var SQL_UID = RealmOptional<Int>()
    @objc dynamic var call_date: String?
    @objc dynamic var call_notes: String?
    @objc dynamic var login_user_name: String?
    @objc dynamic var call_completion_date: String?
    var bilable = RealmOptional<Bool>()
    @objc dynamic var last_insert_update_time_stamp: String?
    @objc dynamic var Mobile_insert_update_create_timestamp: String?
    
    
    enum CodingKeys: String, CodingKey {
        
        case efuel_mobile_call_detail_id
        case login_user_id
        case efuel_mobile_call_log_id
        case SQL_UID
        case call_date
        case call_notes
        case login_user_name
        case call_completion_date
        case bilable
        case last_insert_update_time_stamp
        case Mobile_insert_update_create_timestamp
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "mobile_call_detail_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    //Incrementa ID
    var IncrementaID : Int {
        let realm = try! Realm()
        if let retNext = realm.objects(MobileCallDetail.self).sorted(byKeyPath: "mobile_call_detail_id").last?.mobile_call_detail_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
