import UIKit
import Realm
import RealmSwift

class MobileDeliveryNewTanks: RealmSwift.Object, Codable {

    @objc dynamic var mobile_delivery_new_tank_id: Int = 0
    var efuel_mobile_delivery_new_tank_id = RealmOptional<Int>()
    var efuel_mobile_delivery_queue_id = RealmOptional<Int>()// generated from
    var efuel_mobile_delivery_shift_id = RealmOptional<Int>()
    var mobile_delivery_shift_id = RealmOptional<Int>()
    var delivery_truck_id = RealmOptional<Int>()
    @objc dynamic var record_create_time: String?
    @objc dynamic var Delivery_datetime: String?
    var login_user_id = RealmOptional<Int>()
    var delivery_queue_id = RealmOptional<Int>()
    var delivery_open_status_code = RealmOptional<Int>()
    var delivery_skip_reason_id = RealmOptional<Int>()
    var tank_id = RealmOptional<Int>()
    var tank_num = RealmOptional<Int>()
    @objc dynamic var serial_num_on_tank: String?
    var tank_size_gals = RealmOptional<Int>()
    var price_per_unit = RealmOptional<Double>()
    var salesTax = RealmOptional<Float>()
    var regulatory_fee = RealmOptional<Float>()
    var credit_status_code = RealmOptional<Int>()
    var terms_code = RealmOptional<Int>()
    var product_type = RealmOptional<Int>()
    var unit_of_measure_id = RealmOptional<Int>()
    var min_del_dollars = RealmOptional<Double>()
    var account_num = RealmOptional<Int>()
    @objc dynamic var user_account_name: String?
    @objc dynamic var main_street_address_1: String?
    @objc dynamic var main_city: String?
    @objc dynamic var main_state: String?
    @objc dynamic var tank_notes_new: String?
    
    
    
    enum CodingKeys: String, CodingKey {
        case efuel_mobile_delivery_new_tank_id
        case efuel_mobile_delivery_queue_id
        case efuel_mobile_delivery_shift_id
        case mobile_delivery_shift_id
        case delivery_truck_id
        case record_create_time
        case Delivery_datetime
        case login_user_id
        case delivery_queue_id
        case delivery_open_status_code
        case delivery_skip_reason_id
        case tank_id
        case tank_num
        case serial_num_on_tank
        case tank_size_gals
        case price_per_unit
        case salesTax
        case regulatory_fee
        case credit_status_code
        case terms_code
        case product_type
        case unit_of_measure_id
        case min_del_dollars
        case account_num
        case user_account_name
        case main_street_address_1
        case main_city
        case main_state
        case tank_notes_new = "new_tank_notes"
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "mobile_delivery_new_tank_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    //Incrementa ID
    var IncrementaID : Int {
        let realm = try! Realm()
        if let retNext = realm.objects(MobileDeliveryNewTanks.self).sorted(byKeyPath: "mobile_delivery_new_tank_id").last?.mobile_delivery_new_tank_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
