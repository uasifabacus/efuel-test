import UIKit
import Realm
import RealmSwift

class MobileEventLog: RealmSwift.Object, Codable {

    @objc dynamic var mobile_event_log_id: Int = 0
    @objc dynamic var event_name: String?
    @objc dynamic var event_start_time: String?
    @objc dynamic var event_end_time: String?
    @objc dynamic var event_notes: String?
    var gallons = RealmOptional<Double>()
    var cost = RealmOptional<Double>()
    var operating_location_id = RealmOptional<Int>()
    var truck_id = RealmOptional<Int>()
    var login_user_id = RealmOptional<Int>()
    var geocode_Lat = RealmOptional<Double>()
    var geocode_long = RealmOptional<Double>()
    var efuel_mobile_delivery_shift_id = RealmOptional<Int>()
    var efuel_mobile_delivery_queue_id = RealmOptional<Int>()
    var efuel_mobile_sw_version = RealmOptional<Int>()
    
    
    
    enum CodingKeys: String, CodingKey {
        
        case mobile_event_log_id
        case event_name
        case event_start_time
        case event_end_time
        case event_notes
        case gallons
        case cost
        case operating_location_id
        case truck_id
        case login_user_id
        case geocode_Lat
        case geocode_long
        case efuel_mobile_delivery_shift_id
        case efuel_mobile_delivery_queue_id
        case efuel_mobile_sw_version
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "mobile_event_log_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
   
    //Incrementa ID
    func IncrementaID() -> Int{
        let realm = try! Realm()
        if let retNext = realm.objects(MobileEventLog.self).sorted(byKeyPath: "mobile_event_log_id").last?.mobile_event_log_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
