import UIKit
import MessageUI

class VC_loadUnloadTruck: BaseViewController {

    private let k_constVal = 86.0
    private var startTime = Helper.mmDDYYWithCurrentTime
    
    @IBOutlet var btns_loadUnload: [UIButton]!
    
    @IBOutlet weak var lbl_deliver_receive: UILabel!
    
    @IBOutlet weak var TF_DW_before: TextField!
    @IBOutlet weak var TF_DW_after: TextField!
    @IBOutlet weak var TF_RW_before: TextField!
    @IBOutlet weak var TF_RW_after: TextField!
    @IBOutlet weak var TF_odoMeter: TextField!
    @IBOutlet weak var TF_DW_RW_notes: TextField!
    @IBOutlet weak var TF_totalizer: TextField!
    
    
    @IBOutlet weak var btn_start_stop: UIButton!
    
    
    
    private var loadingStatus = LoadingStatus.prepare_load {
        didSet {
            self.updateUI()
        }
    }
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btns_loadUnload[0].isSelected = true
        
        self.title = "Load/Unload Truck"
        
        self.setTotalizer()
        
        self.loadingStatus = .prepare_load
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.TF_DW_before.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func setTotalizer() {
        self.TF_totalizer.text = RealmHelper.retrieveTotalizerVal.string
        
        if self.TF_totalizer.text == "-1.0" {
            self.TF_totalizer.text = "0"
        }
        
        self.TF_totalizer.isEnabled = false
    }
    
    
    
    
    //MARK:- Actions
    @IBAction func action_load_unload(_ sender: UIButton) {

        guard sender.isSelected != true else { return }
        
        for btn in btns_loadUnload {
            btn.isSelected = false
        }
        
        
        sender.isSelected = true
        if sender.tag == 1 {
            self.lbl_deliver_receive.text = "Delivering Warehouse Code"
        }
        else {
            self.lbl_deliver_receive.text = "Receiving Warehouse Code"
        }
    }
    
    
    
    
    @IBAction func action_start_stop(_ sender: Any) {

        switch self.loadingStatus {
            
        case .prepare_load:
            self.startLoading()
            break
            
        case .loading:
            self.endLoading()
            break
            
        case .log_load_data:
            break
        }
    }
    
    
    
    private func startLoading() {
        
        guard let DW_beforeStr = Helper.validateAndTrimText(TF: self.TF_DW_before),
            let DW_before = DW_beforeStr.double() else {
                Helper.showBanner(text: "Invalid Delivering Warehouse% Before Value", isSuccess: false)
                return
        }
        
        guard let RW_beforeStr = Helper.validateAndTrimText(TF: self.TF_RW_before),
            let RW_before = RW_beforeStr.double() else {
                Helper.showBanner(text: "Invalid Receiving Warehouse% Before Value", isSuccess: false)
                return
        }

        guard ((DW_before >= 0) && (DW_before <= k_constVal) && (RW_before >= 0) && (RW_before <= k_constVal)) else {
            Helper.showBanner(text: "Delivering/Receiving warehouse % before should be between 0 to \(k_constVal)", isSuccess: false)
            return
        }
        
        guard ((Helper.validateAndTrimText(TF: self.TF_odoMeter)?.double()) != nil) else {
            Helper.showBanner(text: "Invalid Odometer Value", isSuccess: false)
            return
        }
        
        guard (Helper.validateAndTrimText(TF: self.TF_DW_RW_notes, message: "Invalid Warehouse code") != nil) else {return}
        
        self.showAlert(title: nil,
                       message: "You can now begin loading, please return to complete the loading transaction",
                       buttonTitles: ["Ok"],
                       highlightedButtonIndex: nil,
                       completion: nil)
        
        self.loadingStatus = .loading
    }
    
    
    
    
    private func endLoading() {
        
        //******************************         After Values
        
        guard let DW_afterStr = Helper.validateAndTrimText(TF: self.TF_DW_after),
            let DW_after = DW_afterStr.double() else {
                Helper.showBanner(text: "Invalid Delivering Warehouse% After Value", isSuccess: false)
                return
        }
        
        guard let RW_afterStr = Helper.validateAndTrimText(TF: self.TF_RW_after),
            let RW_after = RW_afterStr.double() else {
                Helper.showBanner(text: "Invalid Receiving Warehouse% After Value", isSuccess: false)
                return
        }
        
        //******************************         After Values
        
        
        
        //******************************         Before Values
        
        guard let DW_beforeStr = Helper.validateAndTrimText(TF: self.TF_DW_before, message: "Invalid Delivering Warehouse% Before Value"),
            let DW_before = DW_beforeStr.double() else {return}
        
        guard let RW_beforeStr = Helper.validateAndTrimText(TF: self.TF_RW_before, message: "Invalid Receiving Warehouse% Before Value"),
            let RW_before = RW_beforeStr.double() else {return}
        
        guard let odoMeterStr = Helper.validateAndTrimText(TF: self.TF_odoMeter),
            let odoMeter = odoMeterStr.double() else {
                Helper.showBanner(text: "Invalid Odometer Value", isSuccess: false)
                return
        }
        
        guard let WHCodeStr = Helper.validateAndTrimText(TF: self.TF_DW_RW_notes, message: "Invalid Warehouse code") else {return}
        
        //******************************         Before Values
        
        
        
        guard (DW_after >= 0) && (DW_after <= DW_before) else {
            Helper.showBanner(text: "Delivering warehouse % after should be between 0 to \(DW_before)", isSuccess: false)
            return
        }
        
        
        guard (RW_after <= k_constVal) && (RW_after >= RW_before) else {
            Helper.showBanner(text: "Receiving warehouse % after should be between \(RW_before) to \(k_constVal)", isSuccess: false)
            return
        }
        
        
        
        RealmHelper.createLoadUnloadTruck(isLoadTruck: self.btns_loadUnload[0].isSelected,
                                          RW_after: RW_after,
                                          RW_before: RW_before,
                                          DW_before: DW_before,
                                          DW_after: DW_after,
                                          startTime: self.startTime,
                                          wareHouseCode: WHCodeStr,
                                          odoMeterVal: odoMeter,
                                          totalizerEnd: (self.TF_totalizer.text?.double())!)
        
        RealmHelper.createMobileEventLog(eventName: self.btns_loadUnload[0].isSelected ? .loadTruck : .unloadTruck,
                                   startTime: self.startTime,
                                   endTime: Helper.mmDDYYWithCurrentTime,
                                   note: "truck#\(RealmHelper.truckNumber() ?? "empty")",
            efuelMobileDeliveryShiftId: RealmHelper.shared.efuelMobileDeliveryShiftId!)


        RealmHelper.createOrUpdateMobileAppStatus(last_inventory_startValue: RW_after,
                                                  last_inventory_startQty: (RW_after * (RealmHelper.shared.truckSizeAndNumber?.capacityGallons.value)!) / 100)
        
        self.printLoadUnloadSummary(message: "Do you want to Print \(self.btns_loadUnload[0].isSelected ? "Load" : "Unload") Summary?") { (isSucceed, deviceType) -> (Void) in
            
            if isSucceed {
                if deviceType == .simulator {
                    if self.email(str: self.printLoadTruckSummary, vc: self) == false {
                        self.navigationController?.popViewController()
                    }
                }
                else if deviceType == .LCR {
                    // LCR
                }
                else {      // it means, User don't want to print
                    self.navigationController?.popViewController()
                }
            }
        }
        
//        self.loadingStatus = .prepare_load
    }
    
    
    

    
    
    private func updateUI() {
        
        switch self.loadingStatus {
            
        case .prepare_load, .log_load_data:
            self.btn_start_stop.setImage(UIImage(named: "start_btn"), for: .normal)
            self.navigationItem.hidesBackButton = false
            self.enableDisableAfter(isEnabled: false)
            self.enableDisableBefore(isEnabled: true)
            break

            
        case .loading:
            self.startTime = Helper.mmDDYYWithCurrentTime
            self.btn_start_stop.setImage(UIImage(named: "complete_btn"), for: .normal)
            self.navigationItem.hidesBackButton = true
            self.enableDisableBefore(isEnabled: false)
            self.enableDisableAfter(isEnabled: true)
            break
        }
    }
    
    
    private func enableDisableBefore(isEnabled: Bool) {
        
        self.TF_RW_before.isEnabled = isEnabled
        self.TF_DW_before.isEnabled = isEnabled
        self.TF_DW_RW_notes.isEnabled = isEnabled
        self.TF_odoMeter.isEnabled = isEnabled
    }
    
    private func enableDisableAfter(isEnabled: Bool) {
        
        self.TF_RW_after.isEnabled = isEnabled
        self.TF_DW_after.isEnabled = isEnabled
    }
    
    
    private func goToBack () {
        self.navigationController?.popViewController()
    }
}




//MARK:- VC_loadUnloadTruck MFMailComposeViewControllerDelegate
extension VC_loadUnloadTruck : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
            self.goToBack()
        }
    }
}
