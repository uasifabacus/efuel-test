import UIKit

class VC_additionalNotesCreate: BaseViewController {

    
    @IBOutlet weak var TV_newNote: UITextView!
    var callbackNewNote : ((String)->())!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Create Additional Notes"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func action_create(_ sender: Any) {
        
        self.callbackNewNote(TV_newNote.text)
        self.navigationController?.popViewController(animated: true)
    }
}
