import UIKit
import DeviceKit

class TextField: UITextField {

    override var isEnabled: Bool {
        willSet {
            if self.tag == 99 { backgroundColor = newValue ? UIColor.white : UIColor.hexStringToUIColor(hex: "E6E6FA") }
        }
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        print("Device.current: \(Device.current)")
        
        if Device.current == .simulator(.iPadAir) ||
            Device.current == .iPadAir {
            self.font = UIFont(name: self.font!.fontName, size: 24)
        }
    }
}
