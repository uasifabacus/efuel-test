import Foundation
import Alamofire
import Realm
import RealmSwift
import SwiftDate

// MARK: - TruckInspectionReportItemsRoot
class DeliveryQueueRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [DeliveryQueue]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [DeliveryQueue]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - Datum
class DeliveryQueue: RealmSwift.Object, Codable {
    
    
    @objc dynamic var deliveryQueueId: Int = 0
    var efuelMobileDeliveryQueueId = RealmOptional<Int>()
    var efuelMobileDeliveryShiftId = RealmOptional<Int>()
    var efuelMobileDeliveryTruckId = RealmOptional<Int>()
    var tankId = RealmOptional<Int>()
    var latestReportedTankLevel = RealmOptional<Int>()
    var projectedFloatLevelAsPct = RealmOptional<Int>()
    var projectedFillGals = RealmOptional<Int>()
    var deliveryTruckId = RealmOptional<Int>()
    var deliveryOpenStatusCode = RealmOptional<Int>()
    var installationStreetAddressId = RealmOptional<Int>()
    var pushpinLabel = RealmOptional<Int>()
    var truckStopNumber = RealmOptional<Int>()
    @objc dynamic var deliveryNote, tankIsMonitored, forecastDatetime: String?
    var deliveryDatetimeFromJSON : String?
    @objc dynamic var deliveryDatetime : Date? = nil
    
//    var dateTime_delivery: DateInRegion? { // read-only properties are automatically ignored
//        return deliveryDatetimeFromJSON?.toDate()
//    }
    
    
    enum CodingKeys: String, CodingKey {
        case deliveryQueueId = "delivery_queue_id"
        case efuelMobileDeliveryQueueId = "efuel_mobile_delivery_queue_id"
        case efuelMobileDeliveryShiftId = "efuel_mobile_delivery_shift_id"
        case efuelMobileDeliveryTruckId = "efuel_mobile_delivery_truck_id"
        case tankId = "tank_id"
        case latestReportedTankLevel = "latest_reported_tank_level"
        case projectedFloatLevelAsPct = "projected_float_level_as_pct"
        case projectedFillGals = "projected_fill_gals"
        case deliveryTruckId = "delivery_truck_id"
        case deliveryOpenStatusCode = "delivery_open_status_code"
        case installationStreetAddressId = "installation_street_address_id"
        case pushpinLabel = "pushpin_label"
        case truckStopNumber = "truck_stop_number"
        case deliveryNote = "delivery_note"
        case tankIsMonitored = "tank_is_monitored"
        case deliveryDatetimeFromJSON = "Delivery_datetime"
        case forecastDatetime = "forecast_datetime"
    }
    
    
    
    //MARK:- realm
    
    override static func ignoredProperties() -> [String] {
        return ["deliveryDatetimeFromJSON"]
    }
    
    
    convenience init(obj: DeliveryQueue) {
        self.init()
        self.deliveryQueueId = obj.deliveryQueueId
        self.efuelMobileDeliveryQueueId = obj.efuelMobileDeliveryQueueId
        self.efuelMobileDeliveryShiftId = obj.efuelMobileDeliveryShiftId
        self.efuelMobileDeliveryTruckId = obj.efuelMobileDeliveryTruckId
        self.tankId = obj.tankId
        self.latestReportedTankLevel = obj.latestReportedTankLevel
        self.projectedFloatLevelAsPct = obj.projectedFloatLevelAsPct
        self.projectedFillGals = obj.projectedFillGals
        self.deliveryTruckId = obj.deliveryTruckId
        self.deliveryOpenStatusCode = obj.deliveryOpenStatusCode
        self.installationStreetAddressId = obj.installationStreetAddressId
        self.pushpinLabel = obj.pushpinLabel
        self.truckStopNumber = obj.truckStopNumber
        self.deliveryNote = obj.deliveryNote
        self.tankIsMonitored = obj.tankIsMonitored
        self.forecastDatetime = obj.forecastDatetime
        self.deliveryDatetime = obj.deliveryDatetimeFromJSON?.toDate()?.date
    }
    override static func primaryKey() -> String? {
        return "deliveryQueueId"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
