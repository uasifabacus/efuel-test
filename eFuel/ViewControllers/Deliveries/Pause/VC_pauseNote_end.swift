import UIKit
import SwiftDate

class VC_pauseNote_end: BaseViewController {

    var heading : String!
    private var notes : String?
    private var timer : Timer!
    
    private var date : DateInRegion! {
        didSet {
            self.lbl_startAt.text = self.makeFormattedTime()
        }
    }
    
    private lazy var currentTime : DateInRegion = {
        return DateInRegion()
    }()
    
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_startAt: UILabel!
    @IBOutlet weak var lbl_elapsedTime: UILabel!
    
    
    
    
    
    //MARK:- Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Event"
        
        self.lbl_heading.text = self.heading
        
        self.setupUI(isFirst: true)
    }
    
    deinit {
        print("deinit called")
        self.timer.invalidate()
    }
    
    
    
    private func setupUI(isFirst: Bool = false) {
        
        guard isFirst == true else { return }
        
        self.setupTimer()
        self.date = self.currentTime
        self.lbl_elapsedTime.text = "0"
    }
    
    
    
    func setupTimer() {
        
        self.timer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { [unowned self] (timer) in
            self.date = self.date + 1.minutes
            self.lbl_elapsedTime.text = String((self.lbl_elapsedTime.text?.int)! + 1)
        }
    }
    
    
    
    func makeFormattedTime() -> String {

        return "\(self.date.toFormat(k_MMDDYY)) \(self.date.toFormat(k_HHMMA))"
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "segue_notes" {

            let vc = segue.destination as! VC_pauseNotePopOver
            vc.popOverSetting(widthPercent: k_popOverWidthPercent, heightPercent: k_popOverHeightPercent)
            vc.delegate_ = self
        }
    }
    
    
    
    //MARK:-Actions
    @IBAction func action_end(_ sender: Any) {
        
        #warning("Abdul Ahad sy pochna hai k event name kia aega yahan? aur efuelMobileDeliveryShiftId kia hogi?")
        RealmHelper.createMobileEventLog(eventName: .pump,
                                         startTime: self.currentTime.toFormat(k_standardDateFormat),
                                         endTime: DateInRegion().toFormat(k_standardDateFormat),
                                         note: self.notes,
                                         efuelMobileDeliveryShiftId: RealmHelper.shared.mobileDeliveryShiftId)

        self.navigationController?.popViewController(animated: true)
    }
}



extension VC_pauseNote_end : PassDataPopoverToParent {
    
    func passData(obj: Any?, vc: UIViewController) {
        
        if let text = obj as? String { notes = text }
    }
}
