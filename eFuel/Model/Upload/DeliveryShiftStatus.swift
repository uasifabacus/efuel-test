

import UIKit
import Realm
import RealmSwift


class DeliveryShiftStatusRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [DeliveryShiftStatus]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [DeliveryShiftStatus]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}




class DeliveryShiftStatus: RealmSwift.Object, Codable {
    
    @objc dynamic var efuel_mobile_delivery_shift_id: Int = -1
    @objc dynamic var truck_id: Int = -1
    @objc dynamic var login_user_id: Int = -1
    @objc dynamic var status: String!// make Enum login, truck, new_shift_id
    @objc dynamic var efuel_mobile_call_log_id: Int = -1
    @objc dynamic var efuel_mobile_call_detail_id: Int = -1
    @objc dynamic var last_manual_sale_number: Int = -1
    
    
    
    
    
    enum CodingKeys: String, CodingKey {
        case efuel_mobile_delivery_shift_id
        case truck_id
        case login_user_id
        case status
        case efuel_mobile_call_log_id
        case efuel_mobile_call_detail_id
        case last_manual_sale_number
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "efuel_mobile_delivery_shift_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    //Incrementa ID
    func IncrementaID() -> Int {
        
        let userDefault = UserDefaults.standard
        
        if var primaryKey = userDefault.value(forKey: "efuel_mobile_delivery_shift_id") as? Int {
            
            primaryKey += 1
            
            userDefault.setValue(primaryKey, forKey: "efuel_mobile_delivery_shift_id")
            return primaryKey
        }
        else {
            return 1
        }
        
//        UserDefaults.setValue(0, forKey: "efuel_mobile_delivery_shift_id")
        
        
//        let realm = try! Realm()
//        if let retNext = realm.objects(DeliveryShiftStatus.self).sorted(byKeyPath: "efuel_mobile_delivery_shift_id").last?.efuel_mobile_delivery_shift_id {
//            return retNext + 1
//        }else{
//            return 1
//        }
    }
}
