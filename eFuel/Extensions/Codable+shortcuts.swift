import Foundation

extension Encodable {
    
    
    private func jsonData() throws -> Data {
//        return try JSONEncoder().encode(self)
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .sortedKeys
        return try encoder.encode(self)
    }
    
    
/*
********************************************** jsonString Usage **********************************************

 do{
    let test = try achievedDetails?.data.jsonString()
    print(test)
    }
    catch {
    print(error)
    }
 
********************************************** jsonString Usage **********************************************
*/
    
    
    func jsonString(encoding: String.Encoding = .utf8) -> String? {
        
        do {
            return String(data: try self.jsonData(), encoding: encoding)
        } catch {
            return nil
        }
    }
    
    
    func toJSONData(encoding: String.Encoding = .utf8) -> Data? {

        do {
            let data = try JSONEncoder().encode(self)
            return data
        } catch  {
            return nil
        }
    }
    
    
    func toDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
