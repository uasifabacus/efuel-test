import UIKit
import Realm
import RealmSwift

class MobileGPSData: RealmSwift.Object, Codable {

    @objc dynamic var gps_data_id: Int = 0
    var operating_location_id = RealmOptional<Int>()
    var login_user_id = RealmOptional<Int>()
    var truck_id = RealmOptional<Int>()
    var geocode_lat = RealmOptional<Double>()
    var geocode_long = RealmOptional<Double>()
    var speed = RealmOptional<Double>()
    @objc dynamic var direction: String?
    @objc dynamic var gps_data_time_stamp: String?
    var no_gps_data = RealmOptional<Bool>()
    @objc dynamic var no_gps_data_timestamp: String?
    
    
    
    enum CodingKeys: String, CodingKey {
        case gps_data_id
        case operating_location_id
        case login_user_id
        case truck_id
        case geocode_lat
        case geocode_long
        case speed
        case direction
        case gps_data_time_stamp
        case no_gps_data
        case no_gps_data_timestamp
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "gps_data_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    
    //Incrementa ID
    var IncrementaID : Int {
        let realm = try! Realm()
        if let retNext = realm.objects(MobileGPSData.self).sorted(byKeyPath: "gps_data_id").last?.gps_data_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
