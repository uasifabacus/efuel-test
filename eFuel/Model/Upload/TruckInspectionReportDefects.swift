import UIKit
import Realm
import RealmSwift

class TruckInspectionReportDefects: RealmSwift.Object, Codable {

//    @objc dynamic var inspection_time: String?
    @objc dynamic var truck_inspection_report_defects_id: Int = 0
    @objc dynamic var truck_id: Int = -1
    @objc dynamic var truck_inspection_report_item_id: Int = -1
    @objc dynamic var inspection_time: String?
    
    
    
    
    enum CodingKeys: String, CodingKey {

//        case inspection_time
        case truck_id
        case truck_inspection_report_item_id
        case inspection_time
        case truck_inspection_report_defects_id
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "truck_inspection_report_defects_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    //Incrementa ID
    func IncrementaID() -> Int{
        let realm = try! Realm()
        if let retNext = realm.objects(TruckInspectionReportDefects.self).sorted(byKeyPath: "truck_inspection_report_defects_id").last?.truck_inspection_report_defects_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
