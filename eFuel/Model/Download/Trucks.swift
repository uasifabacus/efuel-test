
import Foundation
import Alamofire
import Realm
import RealmSwift

// MARK: - TruckRoot
class TruckRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array_trucks: [Trucks]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array_trucks = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [Trucks]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array_trucks = data
    }
}


// MARK: - Trucks
class Trucks: RealmSwift.Object, Codable {
    @objc dynamic var truckID: Int = 0
    var operatingLocationID = RealmOptional<Int>()
    var capacityGallons = RealmOptional<Double>()
    var gpsType = RealmOptional<Int>()
    var gpsCollectionInterval = RealmOptional<Int>()
    var gpsErrMaxHDOP = RealmOptional<Int>()
    var gpsCOMPort = RealmOptional<Int>()
    @objc dynamic var efuelMobileDeviceType, efuelMobilePrinterType, truckNumber: String?
    var truckInUse = RealmOptional<Bool>()
    
    
    enum CodingKeys: String, CodingKey {
        case truckID = "truck_id"
        case operatingLocationID = "operating_location_id"
        case truckNumber = "truck_number"
        case capacityGallons = "capacity_gallons"
        case efuelMobileDeviceType = "efuel_mobile_deviceType"
        case efuelMobilePrinterType = "efuel_mobile_printerType"
        case gpsType = "gps_type"
        case gpsCollectionInterval = "gps_collection_interval"
        case gpsCOMPort = "gps_com_port"
        case gpsErrMaxHDOP = "gps_errMax_HDOP"
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "truckID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
