import UIKit
import RealmSwift
import SwiftDate

class VC_summary: BaseViewController {

    @IBOutlet weak var lbl_truckNumb: Label!
    @IBOutlet weak var lbl_numbOfDeliveries: Label!
    @IBOutlet weak var lbl_quantityDelivered: Label!
    @IBOutlet weak var lbl_startTot: Label!
    @IBOutlet weak var lbl_startOdo: Label!
    @IBOutlet weak var lbl_estQtyRem: Label!
    @IBOutlet weak var lbl_strtDate: Label!
    
    @IBOutlet weak var lbl_strtTime: Label!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func setupUI() {
        
        if let truck = RealmHelper.shared.selectedTruckDetails {
            self.lbl_truckNumb.text = truck.truckNumber ?? "-"
        }
        else {
            self.lbl_truckNumb.text = "-"
        }
        
        
        
        let realm = try! Realm()
        
        let predicate = NSPredicate(format: "record_type = 2 OR record_type = 3 OR record_type = 4")
        self.lbl_numbOfDeliveries.text = realm.objects(MobileDeliveryQueue.self).filter(predicate).count.string
        self.lbl_quantityDelivered.text = (realm.objects(MobileDeliveryQueue.self).filter(predicate).sum(ofProperty: "units_delivered") as Double?)?.string
        self.lbl_startTot.text = realm.objects(MobileDeliveryShift.self).first?.totalizer_out.value?.string
        self.lbl_startOdo.text = realm.objects(MobileDeliveryShift.self).first?.odometer_out.value?.string
        self.lbl_estQtyRem.text = realm.objects(MobileAppStatus.self).first?.last_inventory_startQty.value?.string
        

        
        
        if let dateNTime_out = realm.objects(MobileDeliveryShift.self).first?.time_out {
            
            let date = DateInRegion(dateNTime_out, region: Region.local)
            self.lbl_strtDate.text = date!.toFormat(k_MMDDYY)
            self.lbl_strtTime.text = date!.toFormat(k_HHMMA)
        }
        else {
            self.lbl_strtDate.text = "-"
            self.lbl_strtTime.text = "-"
        }
    }
    
    
    
    
    @IBAction func action_print(_ sender: Any) {
    }
}
