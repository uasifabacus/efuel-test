import Foundation
import UIKit
import MessageUI

extension UIViewController  {
    
    func email<T: UIViewController & MFMailComposeViewControllerDelegate>(str: String, vc: T) -> Bool {
        
        
        /*
         guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else { return }
         guard let writePath = NSURL(fileURLWithPath: path).appendingPathComponent("Reports") else { return }
         try? FileManager.default.createDirectory(atPath: writePath.path, withIntermediateDirectories: true)
         let file = writePath.appendingPathComponent(Helper.mmDDYYWithCurrentTime + ".txt")
         try? str.write(to: file, atomically: false, encoding: String.Encoding.utf8)
         
         print(path)
         print(writePath)
         */
        
        
        guard MFMailComposeViewController.canSendMail() else {
            Helper.showBanner(text: "You cannot send email, may be the email is not configured in your device", isSuccess: false)
            return false
        }

        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = vc
        //        mail.setToRecipients(["abc@abc.com"])
        mail.setMessageBody(str, isHTML: false)

        vc.present(mail, animated: true)
        return true
    }
}
