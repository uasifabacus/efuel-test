import Foundation
import Alamofire
import RealmSwift
import Realm

// MARK: - InstalltionStreetAddressesRoot
class InstalltionStreetAddressesRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [InstalltionStreetAddresses]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
}



// MARK: - Datum
class InstalltionStreetAddresses:RealmSwift.Object, Codable {
    @objc dynamic var installationStreetAddressID: Int = 0
    var userAccountID = RealmOptional<Int>()
    var installationGeoCodeLat = RealmOptional<Double>()
    var installationGeoCodeLong = RealmOptional<Double>()
    @objc dynamic var installationStreetAddressLine1, installationStreetAddressLine2, installationCity, installationState, installationZip: String?
    var tanksPricingCreditMonitorStatus = List<TanksPricingCreditMonitorStatus>()
    var owners = LinkingObjects(fromType: UserAccounts.self, property: "installationStreetAddresses")
    
    enum CodingKeys: String, CodingKey {
        case userAccountID = "user_account_id"
        case installationStreetAddressID = "installation_street_address_id"
        case installationStreetAddressLine1 = "installation_street_address_line_1"
        case installationStreetAddressLine2 = "installation_street_address_line_2"
        case installationCity = "installation_city"
        case installationState = "installation_state"
        case installationZip = "installation_zip"
        case installationGeoCodeLat = "installation_GeoCodeLat"
        case installationGeoCodeLong = "installation_GeoCodeLong"
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "installationStreetAddressID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
