import UIKit

class VC_deliveryInfo: BaseViewController {

    @IBOutlet var btns_presetBy: [UIButton]!
    
    @IBOutlet weak var btn_partialFill: UIButton!
    
    
    //MARK:-Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Delivery Info"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    //MARK:- actions
    
    @IBAction func action_presetBy(_ sender: UIButton) {
        
        guard sender.isSelected != true else { return }
        
        for btn in btns_presetBy {
            btn.isSelected = false
        }
        
        sender.isSelected = true
    }
    
    
    @IBAction func action_partialFil(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    
    @IBAction func action_skip(_ sender: Any) {
        self.navigationController?.pushViewController(TVC_skipReason(), animated: true)
    }
    
    
    @IBAction func action_print(_ sender: Any) {
    }
}
