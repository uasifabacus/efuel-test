import UIKit

class Cell_newCaseReason: UITableViewCell {

    @IBOutlet weak var lbl_title: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    func setupUI(str: String) {
        
        self.lbl_title.text = str
    }
    
    func setupUIForPause(str: String) {
        
        self.lbl_title.text = str
    }
}
