import Foundation
import Alamofire
import Realm
import RealmSwift

// MARK: - DeliveryInfoRoot
class DeliveryInfoRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [DeliveryInfo]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [DeliveryInfo]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - Datum
class DeliveryInfo: RealmSwift.Object, Codable {
    
    @objc dynamic var tankID: Int = 0
    @objc dynamic var deliveryReference, deliveryInstructions, shipToName: String?
    
    
    enum CodingKeys: String, CodingKey {
        case tankID = "tank_id"
        case deliveryReference = "delivery_reference"
        case deliveryInstructions = "delivery_instructions"
        case shipToName = "ShipToName"
    }
    
   
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "tankID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
