import UIKit

class Master_order: BaseViewController {

    @IBOutlet weak var segmentC: UISegmentedControl!
    
    var selectedUser : UserAccounts!
    var selectedTank: TanksPricingCreditMonitorStatus?
    
    
    private lazy var vc_tanks: VC_tanks = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_tanks") as! VC_tanks
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    private lazy var vc_status: VC_status = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_status") as! VC_status
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    
    
    
    
    
//MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "New Order"
        
        self.action_changeSegment(self.segmentC)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
//MARK:- Actions
    @IBAction func action_createNewOrder(_ sender: Any) {
    }
    
    
    @IBAction func action_changeSegment(_ sender: UISegmentedControl) {

        self.remove(asChildViewController: self.vc_tanks)
        self.remove(asChildViewController: self.vc_status)
        
        
        if sender.selectedSegmentIndex == 0 {
            self.addViewControllerAsChildVC(childVC: self.vc_tanks)
        }
        if sender.selectedSegmentIndex == 1 {
            self.addViewControllerAsChildVC(childVC: self.vc_status)
        }
        
        /*
        self.vc_tanks.view.isHidden = !(sender.selectedSegmentIndex == 0)
        self.vc_status.view.isHidden = !(sender.selectedSegmentIndex == 1)
        */
    }
    

    

}
