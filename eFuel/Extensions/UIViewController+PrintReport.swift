import Foundation
import UIKit
import SwiftDate
import RealmSwift
import MessageUI

fileprivate let padding = 2



extension UIViewController {
    
    func inspectReportPrint(truckId: Int? = nil) -> String? {
        
        var concatinatedStr = "Deriver's vechicle"
        concatinatedStr.append("\nInspection Report")
        concatinatedStr.append("\n\n")
        
        let realm = try! Realm()
        
        if let truckInspectionReport = realm.objects(TruckInspectionReports.self).first {
             
            let date = DateInRegion(truckInspectionReport.inspection_time!, region: Region.local)
            concatinatedStr.append("Date: \(date!.toFormat(k_MMDDYY))")
            concatinatedStr.append("\n")
            concatinatedStr.append("Time: \(date!.toFormat(k_HHMMA))")
        }
        else {
            Helper.showBanner(text: "The report has already been uploaded to server. Therefore there is no data for printing", isSuccess: false)
            return nil
        }
        
        
        self.concatinateDriverDetail(concatinatedStr: &concatinatedStr, truckId: truckId)
        
        /*
        if let truckNumber = RealmHelper.truckNumber(truckId: truckId) {
            concatinatedStr.append("\n")
            concatinatedStr.append("Truck Number: \(truckNumber)")
        }
        
        
        if let userDesc = RealmHelper.shared.userDesc {
            concatinatedStr.append("\n")
            concatinatedStr.append("Driver Name:")
            concatinatedStr.append("\n")
            concatinatedStr.append("  \(userDesc.inserting(separator: "\n", every: k_maxPageWidth-padding))")
        }
        */
        
        
        concatinatedStr.append("\n")
        concatinatedStr.append("Defects:")
        
        
        let array = realm.objects(TruckInspectionReportDefects.self)
        if array.count > 0 {
            
            array.forEach { (obj) in
                
                if let truckInspectionReportItem = realm.objects(TruckInspectionReportItems.self).filter({$0.truckInspectionReportItemID == obj.truck_inspection_report_item_id}).first {
                
                    concatinatedStr.append("\n")
                    concatinatedStr.append("  \(truckInspectionReportItem.item!.inserting(separator: "\n", every: k_maxPageWidth-padding))")
                }
                
            }
        }
        else {
            concatinatedStr.append("\n")
            concatinatedStr.append("  None")
            concatinatedStr.append("\n")
            concatinatedStr.append("  Condition of Vehicle is Satisfactory".inserting(separator: "\n", every: k_maxPageWidth-padding))
        }
        
        if let truckInspectionReport = realm.objects(TruckInspectionReports.self).first, let remarks = truckInspectionReport.remarks {
            concatinatedStr.append("\n")
            concatinatedStr.append("\n")
            concatinatedStr.append("Remarks:")
            concatinatedStr.append("\n")
            concatinatedStr.append("  \(remarks.inserting(separator: "\n", every: k_maxPageWidth-padding))")
        }
        
        print(concatinatedStr)
//        self.writeToTextFile(concatinatedStr: concatinatedStr)
        return concatinatedStr
    }
    
    
    
    
    var printLoadTruckSummary: String {
        
        var concatinatedStr = "Truck Load Summary"
        concatinatedStr.append("\n")
        
        let realm = try! Realm()
        
        if let mobDeliveryQueue = realm.objects(MobileDeliveryQueue.self).last {
            
            let date = DateInRegion(mobDeliveryQueue.end_time!, region: Region.local)
            concatinatedStr.append("Date: \(date!.toFormat(k_MMDDYY))")
            concatinatedStr.append("\n")
            concatinatedStr.append("Time: \(date!.toFormat(k_HHMMA))")
            concatinatedStr.append("\n")
            self.concatinateDriverDetail(concatinatedStr: &concatinatedStr)
            concatinatedStr.append("\n")
            concatinatedStr.append("Storage code: \(mobDeliveryQueue.warehouse_code?.inserting(separator: "\n", every: k_maxPageWidth) ?? "-"))")
            concatinatedStr.append("\n")
            concatinatedStr.append("Odometer: \(mobDeliveryQueue.current_odometer.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("Totalizer: \(mobDeliveryQueue.start_system_net_totalizer.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("Delivering % Before: \(mobDeliveryQueue.percent_delivering_warehouse_start.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("Delivering % After: \(mobDeliveryQueue.percent_delivering_warehouse_end.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("Receiving % Before: \(mobDeliveryQueue.percent_receiving_warehouse_start.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("Receiving % After: \(mobDeliveryQueue.percent_receiving_warehouse_end.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("Approximate Quantity: \(mobDeliveryQueue.units_delivered.value?.string ?? "-")")
        }
        
        self.writeToTextFile(concatinatedStr: concatinatedStr)
        
        return concatinatedStr
    }

    
    
    
    var printShiftSummaryReport: String {
        
        var concatinatedStr = "Shift Summary Report"
        concatinatedStr.append("\n")
        self.concatinateDriverDetail(concatinatedStr: &concatinatedStr)
        concatinatedStr.append("\n")
        
        
        
        var date = DateInRegion(Helper.mmDDYYWithCurrentTime, region: Region.local)
        concatinatedStr.append("Date: \(date!.toFormat(k_MMDDYY))")
        concatinatedStr.append("\n")
        concatinatedStr.append("Time: \(date!.toFormat(k_HHMMA))")
        concatinatedStr.append("\n")
        concatinatedStr.append("\n")
        
        
        let realm = try! Realm()
        
        let predicate = NSPredicate(format: "record_type = 2 OR record_type = 3 OR record_type = 4")
        let cout = realm.objects(MobileDeliveryQueue.self).filter(predicate).count.string
        let sum = (realm.objects(MobileDeliveryQueue.self).filter(predicate).sum(ofProperty: "units_delivered") as Double?)?.string
        
        
        
        concatinatedStr.append("# of Deliveries: \(cout)")
        concatinatedStr.append("\n")
        concatinatedStr.append("Qty. Delivered: \(sum ?? "")")
        concatinatedStr.append("\n")
        concatinatedStr.append("\n")
        
        
    
        
        
        
        
        if let obj = realm.objects(MobileDeliveryShift.self).last {

            concatinatedStr.append("Start Totalizer: \(obj.totalizer_out.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("End Totalizer: \(obj.totalizer_in.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("\n")
            
            
            
            concatinatedStr.append("Start Odometer: \(obj.odometer_out.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("End Odometer: \(obj.odometer_in.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("Total Miles: \(obj.odometer_in.value! - obj.odometer_out.value!)")
            concatinatedStr.append("\n")
            concatinatedStr.append("\n")
            
            
            
            concatinatedStr.append("Start %: \(obj.inventory_out.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("End %: \(obj.inventory_in.value?.string ?? "-")")
            concatinatedStr.append("\n")
            concatinatedStr.append("\n")
            
            
            
            date = DateInRegion(obj.time_out!, region: Region.local)
            concatinatedStr.append("Start Date: \(date!.toFormat(k_MMDDYY))")
            concatinatedStr.append("\n")
            concatinatedStr.append("Start Time: \(date!.toFormat(k_HHMMA))")
            concatinatedStr.append("\n")
            concatinatedStr.append("\n")
            
            
            date = DateInRegion(obj.time_in!, region: Region.local)
            concatinatedStr.append("End Date: \(date!.toFormat(k_MMDDYY))")
            concatinatedStr.append("\n")
            concatinatedStr.append("End Time: \(date!.toFormat(k_HHMMA))")
            concatinatedStr.append("\n")
        }

        self.writeToTextFile(concatinatedStr: concatinatedStr)
        
        return concatinatedStr
    }
}


/*
 var truck_id = RealmOptional<Int>()
 var login_user_id = RealmOptional<Int>()
 @objc dynamic var login_user_descr: String?
 @objc dynamic var time_out: String?
 @objc dynamic var time_in: String?
 var odometer_out = RealmOptional<Double>()
 var odometer_in = RealmOptional<Double>()
 var totalizer_out = RealmOptional<Double>()
 var totalizer_in = RealmOptional<Double>()
 var inventory_out = RealmOptional<Double>()
 var inventory_in = RealmOptional<Double>()
 var last_manual_sale_number = RealmOptional<Int>()
 @objc dynamic var last_update: String?
 @objc dynamic var app_error_log: String?
 */


extension UIViewController {

    private func concatinateDriverDetail(concatinatedStr: inout String, truckId: Int? = nil) {
        if let truckNumber = RealmHelper.truckNumber(truckId: truckId) {
            concatinatedStr.append("\n")
            concatinatedStr.append("Truck Number: \(truckNumber)")
        }
        
        
        if let userDesc = RealmHelper.shared.userDesc {
            concatinatedStr.append("\n")
            concatinatedStr.append("Driver Name:")
            concatinatedStr.append("\n")
            concatinatedStr.append("  \(userDesc.inserting(separator: "\n", every: k_maxPageWidth-padding))")
        }
    }
    
    private func writeToTextFile(concatinatedStr : String) {
        
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else { return }
        guard let writePath = NSURL(fileURLWithPath: path).appendingPathComponent("Reports") else { return }
        try? FileManager.default.createDirectory(atPath: writePath.path, withIntermediateDirectories: true)
        let file = writePath.appendingPathComponent(Helper.mmDDYYWithCurrentTime + ".txt")
        try? concatinatedStr.write(to: file, atomically: false, encoding: String.Encoding.utf8)
        
        print(path)
        print(writePath)
    }
}
