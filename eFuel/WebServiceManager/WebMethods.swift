import UIKit
import RealmSwift
import MBProgressHUD
import SwifterSwift

class WebMethods: NSObject {
    
    
    
    static func generateToken (view: UIView? = nil, completion: @escaping (_ result: GenerateToken?) -> Void) {
        
        let str = "\(k_grant_type)=\(k_password)&\(k_username)=\(k_userNameGrant)&\(k_password)=\(k_passwordGrant)"
        
        Helper.showHud()
        AlamofireManager.shared.request(Router.generateToken(str: str)).responseGenerateToken { (obj) in
            
            Helper.hideHud()
            
            switch obj.result {
            case .success(let val):
                if val.accessToken != nil {
                    Singleton.shared.generateToken = val
                    completion(val)
                }
                else {
                    Helper.showBanner(text: k_serverSideIssue, isSuccess: false)
                }
                break
            case .failure(let err):
                if err.localizedDescription != k_cancelled {
                    Helper.showBanner(text: err.localizedDescription, isSuccess: false)
                }
                
                break
            }
        }
    }
    
    
    static func authenticateUser(userName: String, pass: String, completion: @escaping (_ result: LoginUser?) -> Void) {
        
        Helper.showHud()
        AlamofireManager.shared.request(Router.AuthenticateUser(userName: userName, pass: pass)).responseAuthenticateUser { (authenticateUser) in
            Helper.hideHud()
            
            if let obj = authenticateUser.arbiGenericResponse() as? AuthenticateUser, let array = obj.data {
                
                guard array.count > 0 else {
                    completion(nil)
                    return
                }
                let userDetail = array[0]
                
                
                do {
                    try RealmHelper.shared.realm.write {
                        RealmHelper.shared.realm.delete(RealmHelper.shared.realm.objects(LoginUser.self))
                        RealmHelper.shared.realm.add(userDetail)
                        completion(userDetail)
                    }
                }
                catch {
                    print("failed to convert data")
                    Helper.showBanner(text: error.localizedDescription, isSuccess: false)
                    completion(nil)
                }
            }
            else {
                completion(nil)
            }
        }
    }
    
    
    static func getDeliveryShiftStatus(truckId: Int, completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        Helper.showHud()
        AlamofireManager.shared.request(Router.GetDeliveryShiftStatus(truck_id: truckId)).responseDeliveryShiftStatusRoot { (res) in
            Helper.hideHud()
            
            if let obj = res.arbiGenericResponse() as? DeliveryShiftStatusRoot, let array = obj.array {
                guard array.count > 0 else {
                    completion(false)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: array, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    
                    completion(true)
                })
            }
            else {
                completion(false)
            }
        }
    }
}



//MARK:- Download work here
extension WebMethods {
    
    
    static func downloadData(level: DownloadLevel, completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        
        
        switch level {
            
        case .appNewlyInstalled:
            downloadNewlyInstalled { (isSucceed) in
                completion(isSucceed)
            }
            break
            
        case .downloadAfterUpload:
            
            downloadNewlyInstalled { (isSucceed) in
                guard isSucceed else {
                    completion(isSucceed)
                    return
                }
                
                ticketsAndSurchargeLabels { (isSucceed) in
                    completion(isSucceed)
                }
            }
            break
            
        case .isShiftStarting:
            
            ticketsAndSurchargeLabels { (isSucceed) in
                completion(isSucceed)
            }
            break
            
            
        case .downloadAfterUploadBG:
            
            downloadInBG { (isSucceed) in
                guard isSucceed else {
                    completion(isSucceed)
                    return
                }
                
                ticketsAndSurchargeLabels { (isSucceed) in
                    completion(isSucceed)
                }
            }
            break
        }
    }
    
    
    private static func downloadInBG (completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        getCallLog { (isSucceed) in
            
            guard isSucceed else {
                completion(isSucceed)
                return
            }
            
            getCallDetail(completion: { (isSucceed) in
                
                guard isSucceed else {
                    completion(isSucceed)
                    return
                }
                
                print("getCallDetail succeed")
                
                
                getDeliveryQueue(completion: { (isSucceed) in
                    completion(isSucceed)
                })
            })
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    private static func ticketsAndSurchargeLabels (completion: @escaping (_ isSucceed: Bool) -> Void) {
        getMobileDeliveryTickets(completion: { (isSucceed) in
            guard isSucceed else {
                completion(isSucceed)
                return
            }
            
            getMobileDeliverySurchargeLabels(completion: { (isSucceed) in
                completion(isSucceed)
            })
        })
    }
    
    
    private static func downloadNewlyInstalled (completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        getCallLog { (isSucceed) in
            
            guard isSucceed else {
                completion(isSucceed)
                return
            }
            
            getCallDetail(completion: { (isSucceed) in
                
                guard isSucceed else {
                    completion(isSucceed)
                    return
                }
                
                
                getLookups(completion: { (isSucceed) in
                    
                    guard isSucceed else {
                        completion(isSucceed)
                        return
                    }
                    
                    
                    getTrucks(completion: { (isSucceed) in
                        
                        guard isSucceed else {
                            completion(isSucceed)
                            return
                        }
                        
                        
                        
                        getetUserAccounts(completion: { (isSucceed) in
                            
                            guard isSucceed else {
                                completion(isSucceed)
                                return
                            }
                            
                            
                            getInstallationStreetAddresses(completion: { (isSucceed) in
                                
                                guard isSucceed else {
                                    completion(isSucceed)
                                    return
                                }
                                
                                getTanksPricingCreditMonitorStatus(completion: { (isSucceed) in
                                    
                                    guard isSucceed else {
                                        completion(isSucceed)
                                        return
                                    }
                                    
                                    getDeliveryInfo(completion: { (isSucceed) in
                                        
                                        guard isSucceed else {
                                            completion(isSucceed)
                                            return
                                        }
                                        
                                        
                                        getDeliverySkipCat(completion: { (isSucceed) in
                                            
                                            guard isSucceed else {
                                                completion(isSucceed)
                                                return
                                            }
                                            
                                            
                                            getDeliverySkipReasons(completion: { (isSucceed) in
                                                
                                                guard isSucceed else {
                                                    completion(isSucceed)
                                                    return
                                                }
                                                
                                                
                                                getTruckInspectionReportItems(completion: { (isSucceed) in
                                                    
                                                    guard isSucceed else {
                                                        completion(isSucceed)
                                                        return
                                                    }
                                                    
                                                    
                                                    getDeliveryQueue(completion: { (isSucceed) in
                                                        completion(isSucceed)
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        }
    }
    
    
    
    private static func getCallLog(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Call Log")
        
        AlamofireManager.shared.request(Router.GetCallLog(opertaionLocId: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseCallLogRoot { (callLogRoot) in
            
            if let obj = callLogRoot.arbiGenericResponse(shouldShowErrorBanner: false) as? CallLogRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }
            
            
            
            
            
            #warning("This is Old Logic, may be we will implement it in future and remove the above one, for every method in this class")
            /*      This is Old Logic, may be we will implement it in future and remove the above one, for every method in this class
             switch callLogRoot.result {
             case .success(let val):
             
             guard let arrray = val.array, arrray.count > 0 else {
             completion(false)
             return
             }
             
             
             RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
             if isSucceed {
             print(isSucceed)
             getCallDetail(completion: { (isSucceed) in
             if isSucceed {
             print("getCallDetail succeed")
             
             getLookups(completion: { (isSucceed) in
             if isSucceed {
             print("getLookups succeed")
             
             getTrucks(completion: { (isSucceed) in
             if isSucceed {
             print("getTrucks succeed")
             
             getetUserAccounts(completion: { (isSucceed) in
             if isSucceed {
             print("getetUserAccounts succeed")
             
             getInstallationStreetAddresses(completion: { (isSucceed) in
             if isSucceed {
             print("getInstallationStreetAddresses succeed")
             
             getTanksPricingCreditMonitorStatus(completion: { (isSucceed) in
             if isSucceed {
             print("getTanksPricingCreditMonitorStatus succeed")
             
             getDeliveryInfo(completion: { (isSucceed) in
             if isSucceed {
             print("getDeliveryInfo succeed")
             
             getDeliverySkipCat(completion: { (isSucceed) in
             if isSucceed {
             print("getDeliverySkipCat succeed")
             
             getDeliverySkipReasons(completion: { (isSucceed) in
             if isSucceed {
             print("getDeliverySkipReasons succeed")
             
             getTruckInspectionReportItems(completion: { (isSucceed) in
             if isSucceed {
             print("getTruckInspectionReportItems succeed")
             completion(true)
             }
             })
             }
             })
             }
             })
             }
             })
             }
             })
             }
             })
             }
             })
             }
             })
             }
             })
             }
             })
             }
             else { completion(false) }
             })
             
             case .failure(let err):
             completion(false)
             print(err)
             }
             */
        }
    }
    
    
    private static func getCallDetail(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Call Detail")
        
        AlamofireManager.shared.request(Router.GetCallDetail(opertaionLocId: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseCallDetailRoot { (obj) in
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? CallDetailRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }
        }
    }
    
    
    private static func getLookups(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading LookUps")
        
        AlamofireManager.shared.request(Router.GetLookups).responseLookUpRoot { (obj) in
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? LookUpRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }
        }
    }
    
    
    private static func getTrucks(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Trucks")
        
        AlamofireManager.shared.request(Router.GetTrucks(operating_location_id: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseTruckRoot { (obj) in
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? TruckRoot {
                guard let arrray = obj.array_trucks, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }        }
    }
    
    
    private static func getDeliveryQueue(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Deliveries")
        
        AlamofireManager.shared.request(Router.GetDeliveryQueue(op_loc_id: Singleton.shared.userDetail.defaultOperatingLocationID.string,
                                                                del_date: Singleton.shared.mmDDYY,
                                                                truck_id: "0")).responseDeliveryQueueRoot { (obj) in
                                                                    
                                                                    if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? DeliveryQueueRoot {
                                                                        guard let arrray = obj.array, arrray.count > 0 else {
                                                                            completion(true)
                                                                            return
                                                                        }
                                                                        
                                                                        
                                                                        RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                                                                            guard isSucceed else {
                                                                                Helper.showBanner(text: k_localDBError, isSuccess: false)
                                                                                completion(false)
                                                                                return
                                                                            }
                                                                            completion(true)
                                                                        })
                                                                    }
                                                                    else {
                                                                        completion(false)
                                                                    }
        }
    }
    
    
    private static func getetUserAccounts(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading User Account")
        
        
        AlamofireManager.shared.request(Router.GetUserAccounts(operating_location_id: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseUserAccountsRoot { (obj) in
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? UserAccountsRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }
        }
    }
    
    
    private static func getInstallationStreetAddresses(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Street Addresses")
        
        AlamofireManager.shared.request(Router.GetInstallationStreetAddresses(operating_location_id: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseInstalltionStreetAddressesRoot { (obj) in
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? InstalltionStreetAddressesRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                RealmHelper.deleteFromDB(type: InstalltionStreetAddresses.self)
                
                arrray.forEach({ (installtionStreetAddress) in

                    if let userAccount = RealmHelper.shared.realm.objects(UserAccounts.self).filter("userAccountID = %@", installtionStreetAddress.userAccountID.value!).first {
                        
                        try! RealmHelper.shared.realm.write {
                            userAccount.installationStreetAddresses.append(installtionStreetAddress)
                        }
                    }
                })
                completion(true)
                
                /*
                 RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                 guard isSucceed else {
                 Helper.showBanner(text: k_localDBError, isSuccess: false)
                 completion(false)
                 return
                 }
                 completion(true)
                 })
                 */
            }
            else {
                completion(false)
            }
        }
    }
    
    
    private static func getTanksPricingCreditMonitorStatus(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Tanks pricing credit monitor status")
        
        AlamofireManager.shared.request(Router.GetTanksPricingCreditMonitorStatus(operating_location_id: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseTanksPricingCreditMonitorStatusRoot { (obj) in
            
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? TanksPricingCreditMonitorStatusRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                RealmHelper.deleteFromDB(type: TanksPricingCreditMonitorStatus.self)
                
                arrray.forEach({ (tanksPricingCreditMonitorStatus) in
                    
//                    print(tanksPricingCreditMonitorStatus)
                    if let installtionStreetAddress = RealmHelper.shared.realm.objects(InstalltionStreetAddresses.self).filter("installationStreetAddressID = %@", tanksPricingCreditMonitorStatus.installationStreetAddressID.value!).first {
                        
                        try! RealmHelper.shared.realm.write {
                            installtionStreetAddress.tanksPricingCreditMonitorStatus.append(tanksPricingCreditMonitorStatus)
                        }
                    }
                })
                completion(true)
                
                
                /*
                 RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                 guard isSucceed else {
                 Helper.showBanner(text: k_localDBError, isSuccess: false)
                 completion(false)
                 return
                 }
                 completion(true)
                 })
                 */
            }
            else {
                completion(false)
            }
        }
    }
    
    
    private static func getDeliveryInfo(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Delivery Info")
        
        AlamofireManager.shared.request(Router.GetDeliveryInfo(operating_location_id: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseDeliveryInfoRoot { (obj) in
            
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? DeliveryInfoRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.deleteFromDB(type: DeliveryInfo.self)
                arrray.forEach({ (deliveryInfo) in
                    
                    if let tanksPricingCreditMonitorStatus = RealmHelper.shared.realm.objects(TanksPricingCreditMonitorStatus.self).filter("tankID = %@", deliveryInfo.tankID).first {
                        
                        try! RealmHelper.shared.realm.write {
                            tanksPricingCreditMonitorStatus.deliveryInfo.append(deliveryInfo)
                        }
                    }
                })

                completion(true)
                
                
                /*
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
                */
            }
            else {
                completion(false)
            }        }
    }
    
    
    private static func getDeliverySkipCat(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Delivery skip categories")
        
        AlamofireManager.shared.request(Router.GetDeliverySkipCategories).responseDeliverySkipCategoriesRoot { (obj) in
            
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? DeliverySkipCategoriesRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    #warning("ye Ahad bhai sy discuss karna hai k, agar Categories nahi aen server sy ... to download data continue karna hai ya nahi?")
                    completion(false)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }
        }
    }
    
    
    private static func getDeliverySkipReasons(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Delivery skip reasons")
        
        AlamofireManager.shared.request(Router.GetDeliverySkipReason).responseDeliverySkipReasonsRoot { (obj) in
            
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? DeliverySkipReasonsRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                //                arrray.sort()
                
                arrray.forEach({ (reason) in
                    
                    if let category = RealmHelper.shared.realm.objects(DeliverySkipCategories.self).filter("deliverySkipCategoryID = %@", reason.deliverySkipCategoryID).first {
                        
                        try! RealmHelper.shared.realm.write {
                            category.reasons.append(reason)
                        }
                    }
                })
                
                
                print(RealmHelper.shared.realm.objects(DeliverySkipCategories.self))
                completion(true)
                
                
                /*
                 RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                 guard isSucceed else {
                 Helper.showBanner(text: k_localDBError, isSuccess: false)
                 completion(false)
                 return
                 }
                 completion(true)
                 })
                 */
            }
            else {
                completion(false)
            }
        }
    }
    
    
    private static func getTruckInspectionReportItems(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Truck inspection report items")
        
        AlamofireManager.shared.request(Router.GetTruckInspectionReportItems(operating_location_id: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseTruckInspectionReportItemsRoot { (obj) in
            
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? TruckInspectionReportItemsRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }
        }
    }
    
    
    
    //    private let k_GetMobileDeliveryTickets = "MobileDeliveryTicket/GetMobileDeliveryTickets/"
    //    private let k_GetMobileDeliverySurchargeLabels = "MobileDeliverySurcharge/GetMobileDeliverySurchargeLabels/"
    
    
    private static func getMobileDeliveryTickets(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Tickets")
        
        
        AlamofireManager.shared.request(Router.GetMobileDeliveryTickets(client_id: Singleton.shared.userDetail.clientID, operating_location_id: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseMobileDeliveryTicketRoot { (obj) in
            
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? MobileDeliveryTicketRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }
        }
    }
    
    
    
    
    
    private static func getMobileDeliverySurchargeLabels(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Loading Truck inspection report items")
        
        AlamofireManager.shared.request(Router.GetMobileDeliverySurchargeLabels(operating_location_id: Singleton.shared.userDetail.defaultOperatingLocationID.string)).responseMobileDeliverySurchargeLabelsRoot { (obj) in
            
            
            if let obj = obj.arbiGenericResponse(shouldShowErrorBanner: false) as? MobileDeliverySurchargeLabelsRoot {
                guard let arrray = obj.array, arrray.count > 0 else {
                    completion(true)
                    return
                }
                
                
                RealmHelper.storeListToDB(array: arrray, completion: { (isSucceed) in
                    guard isSucceed else {
                        Helper.showBanner(text: k_localDBError, isSuccess: false)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                completion(false)
            }
        }
    }
}



//MARK:- UPLOAD work here
extension WebMethods {
    
    
    
    static func uploadData(shouldUploadMobileDelvryShift : Bool = true, isOnlyUpload: Bool, downloadLevel: DownloadLevel = .downloadAfterUpload, shouldRemoveMobDeliveryShiftFromLocal : Bool = true, completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        
        
        uploadRemainingData(isOnlyUpload: isOnlyUpload, downloadLevel: downloadLevel) { (isSucceed) in
            guard isSucceed else {
                completion(isSucceed)
                return
            }
            
            if shouldUploadMobileDelvryShift == true {
                upload_mobile_delivery_shift(shouldRemoveMobDeliveryShiftFromLocal: shouldRemoveMobDeliveryShiftFromLocal) { (isSucceed) in
                    completion(isSucceed)
                }
            }
            else {
                completion (true)
            }
        }
        
        
        /*
         if shouldUploadMobileDelvryShift == true {
         
         uploadRemainingData(isOnlyUpload: isOnlyUpload, downloadLevel: downloadLevel) { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_mobile_delivery_shift(shouldRemoveMobDeliveryShiftFromLocal: shouldRemoveMobDeliveryShiftFromLocal) { (isSucceed) in
         completion(isSucceed)
         }
         }
         }
         else {
         uploadRemainingData(isOnlyUpload: isOnlyUpload, downloadLevel: downloadLevel) { (isSucceed) in
         completion(isSucceed)
         }
         }
         */
        
        /*
         upload_mobile_delivery_shift(shouldRemoveMobDeliveryShiftFromLocal: shouldRemoveMobDeliveryShiftFromLocal) { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_truck_inspection_reports(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_truck_inspection_report_defects(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_mobile_gps_data(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_mobile_sync_log(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_mobile_event_log(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_mobile_call_log(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_mobile_call_detail(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_mobile_delivery_new_tanks(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         upload_mobile_delivery_queue(completion: { (isSucceed) in
         guard isSucceed else {
         completion(isSucceed)
         return
         }
         
         
         guard isOnlyUpload == false else {
         completion(isSucceed)
         return
         }
         
         downloadData(level: downloadLevel, completion: { (isSucceed) in
         completion(isSucceed)
         })
         })
         })
         })
         })
         })
         })
         })
         })
         })
         }
         */
    }
    
    
    
    
    private static func uploadRemainingData (isOnlyUpload: Bool, downloadLevel: DownloadLevel = .downloadAfterUpload, completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        upload_truck_inspection_reports(completion: { (isSucceed) in
            guard isSucceed else {
                completion(isSucceed)
                return
            }
            
            upload_truck_inspection_report_defects(completion: { (isSucceed) in
                guard isSucceed else {
                    completion(isSucceed)
                    return
                }
                
                upload_mobile_gps_data(completion: { (isSucceed) in
                    guard isSucceed else {
                        completion(isSucceed)
                        return
                    }
                    
                    upload_mobile_sync_log(completion: { (isSucceed) in
                        guard isSucceed else {
                            completion(isSucceed)
                            return
                        }
                        
                        upload_mobile_event_log(completion: { (isSucceed) in
                            guard isSucceed else {
                                completion(isSucceed)
                                return
                            }
                            
                            upload_mobile_call_log(completion: { (isSucceed) in
                                guard isSucceed else {
                                    completion(isSucceed)
                                    return
                                }
                                
                                upload_mobile_call_detail(completion: { (isSucceed) in
                                    guard isSucceed else {
                                        completion(isSucceed)
                                        return
                                    }
                                    
                                    upload_mobile_delivery_new_tanks(completion: { (isSucceed) in
                                        guard isSucceed else {
                                            completion(isSucceed)
                                            return
                                        }
                                        
                                        upload_mobile_delivery_queue(completion: { (isSucceed) in
                                            guard isSucceed else {
                                                completion(isSucceed)
                                                return
                                            }
                                            
                                            
                                            guard isOnlyUpload == false else {
                                                completion(isSucceed)
                                                return
                                            }
                                            
                                            downloadData(level: downloadLevel, completion: { (isSucceed) in
                                                completion(isSucceed)
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    }
    
    
    
    static func upload_mobile_delivery_shift(shouldRemoveMobDeliveryShiftFromLocal: Bool, completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileDeliveryShift.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading delivery Shift")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed && shouldRemoveMobDeliveryShiftFromLocal { RealmHelper.deleteFromDB(type: MobileDeliveryShift.self) }
                if isSucceed {createMobEventLog(eventLogCase: .startShift)}
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    static func update_mobile_delivery_shift(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        let realm = try! Realm()
        let array = realm.objects(MobileDeliveryShift.self)
        guard array.count > 0 else {
            completion(true)
            return
        }
        
        if let jsonData = array.toJSONData() {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Updating delivery Shift")
            
            uploadGenricMethod(router: Router.UpdateMobileDeliveryShift(data: jsonData)) { (isSucceed) in
                
                if isSucceed { createMobEventLog(eventLogCase: .stopShift) }
                else { removeInValuesFromMobDeliveryShift() }
                completion(isSucceed)
            }
        }
    }
    
    
    private static func removeInValuesFromMobDeliveryShift() {
        
        let realm = try! Realm()
        try! realm.write {
            if let objExists = realm.objects(MobileDeliveryShift.self).first {
                objExists.time_in = nil
                objExists.odometer_in.value = nil
                objExists.totalizer_in.value = nil
                objExists.inventory_in.value = nil
            }
        }
    }
    
    
    
    static func upload_truck_inspection_reports(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: TruckInspectionReports.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading Truck inspection reports")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: TruckInspectionReports.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private static func upload_truck_inspection_report_defects(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: TruckInspectionReportDefects.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading Truck inspection reports defects")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: TruckInspectionReportDefects.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private static func upload_mobile_gps_data(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileGPSData.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading GPS data")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileGPSData.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private static func upload_mobile_sync_log(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileSyncLog.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading sync log")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileSyncLog.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private static func upload_mobile_event_log(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        
        if let router = fetchFromRealm(type: MobileEventLog.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading event log")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileEventLog.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private static func upload_mobile_call_log(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileCallLog.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading call log")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileCallLog.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private static func upload_mobile_call_detail(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileCallDetail.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading call detail")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileCallDetail.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private static func upload_mobile_delivery_new_tanks(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileDeliveryNewTanks.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading new tanks")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileDeliveryNewTanks.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private static func upload_mobile_delivery_queue(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileDeliveryQueue.self) {
            
            NotificationCenter.default.post(name: Notification.Name(k_notification_someDataDownloaded), object: "Uploading delivery queue")
            
            uploadGenricMethod(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileDeliveryQueue.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    private static func uploadGenricMethod(router: Router, completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        AlamofireManager.shared.request(router).responseJSON { (obj) in
            
            guard obj.arbiGenericResponse(shouldShowErrorBanner: false) != nil else {
                completion(false)
                return
            }
            completion(true)
        }
    }
    
    
    private static func fetchFromRealm<T: Object>(type: T.Type) -> Router? {
        
        let realm = try! Realm()
        
        let array = realm.objects(type.self)
        guard array.count > 0 else { return nil }
        
        switch array {
        case let tim as Results<MobileDeliveryShift>:
            if let jsonData = tim.toJSONData() { return Router.InsertMobileDeliveryShift(data: jsonData) }
            return nil
        case let tim as Results<TruckInspectionReports>:
            if let jsonData = tim.toJSONData() { return Router.InsertTruckInspectionReport(data: jsonData) }
            return nil
        case let tim as Results<TruckInspectionReportDefects>:
            if let jsonData = tim.toJSONData() { return Router.InsertTruckInspectionReportDefects(data: jsonData) }
            return nil
        case let tim as Results<MobileGPSData>:
            if let jsonData = tim.toJSONData() { return Router.InsertMobileGPS(data: jsonData) }
            return nil
        case let tim as Results<MobileSyncLog>:
            if let jsonData = tim.toJSONData() { return Router.InsertMobileSyncLog(data: jsonData) }
            return nil
        case let tim as Results<MobileEventLog>:
            if let jsonData = tim.toJSONData() { return Router.InsertMobileEventLog(data: jsonData) }
            return nil
        case let tim as Results<MobileCallLog>:
            if let jsonData = tim.toJSONData() { return Router.InsertMobileCallLog(data: jsonData) }
            return nil
        case let tim as Results<MobileCallDetail>:
            if let jsonData = tim.toJSONData() { return Router.InsertMobileCallDetail(data: jsonData) }
            return nil
        case let tim as Results<MobileDeliveryNewTanks>:
            if let jsonData = tim.toJSONData() { return Router.InsertMobileDeliveryNewTank(data: jsonData) }
            return nil
        case let tim as Results<MobileDeliveryQueue>:
            if let jsonData = tim.toJSONData() { return Router.InsertMobileDeliveryQueue(data: jsonData) }
            return nil
            
        default:
            return nil
        }
    }
}




//MARK:- UPLOAD "Logs" work backGround Synchronization
extension WebMethods {
    
    static func uploadLogsInBG() {
        
//        guard RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists != nil else { return }
        
        if RealmHelper.shared.isFullShiftSyncEnabled {
            
            WebMethods.uploadData(shouldUploadMobileDelvryShift: false, isOnlyUpload: false, downloadLevel: .downloadAfterUploadBG, completion: { (isSucceed) in
                
                RealmHelper.writeLogToDB(isSucceed: isSucceed ? k_ok : k_fail, reason: isSucceed ? "N/A" : "full shift Sync")
                uploadSyncLogInBG()
            })
        }
        else {
            var isSuccessFinal = true
            
            uploadGPSDataInBG { (isSucceed) in
                print("uploadGPSDataInBG isSucceed = \(isSucceed)")
                isSuccessFinal = isSucceed
                
                uploadEvenLogInBG { (isSucceed) in
                    print("uploadEvenLogInBG isSucceed = \(isSucceed)")
                    if isSuccessFinal == true { isSuccessFinal = isSucceed }
                    
                    RealmHelper.writeLogToDB(isSucceed: isSuccessFinal ? k_ok : k_fail, reason: isSuccessFinal ? "N/A" : "GPS Sync")
                    uploadSyncLogInBG()
                }
            }
        }
    }
    
    
    
    static private func uploadGPSDataInBG(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileGPSData.self) {
            uploadGenricMethodBG(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileGPSData.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    static private func uploadEvenLogInBG(completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        if let router = fetchFromRealm(type: MobileEventLog.self) {
            uploadGenricMethodBG(router: router) { (isSucceed) in
                if isSucceed { RealmHelper.deleteFromDB(type: MobileEventLog.self) }
                completion(isSucceed)
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    static private func uploadSyncLogInBG() {
        
        if let router = fetchFromRealm(type: MobileSyncLog.self) {
            uploadGenricMethodBG(router: router) { (isSucceed) in
                
                print("uploadSyncLogInBG isSucceed = \(isSucceed)")
                if isSucceed { RealmHelper.deleteFromDB(type: MobileSyncLog.self) }
            }
        }
    }
    
    
    
    
    private static func uploadGenricMethodBG(router: Router, completion: @escaping (_ isSucceed: Bool) -> Void) {
        
        AlamofireManager.shared.request(router).responseJSON { (obj) in
            completion(obj.arbiGenericResponse(shouldShowErrorBanner: false) != nil)
        }
    }
}



extension WebMethods {
    
    static func createMobEventLog(eventLogCase: EventLogCases) {
        
        let currentTime = Helper.mmDDYYWithCurrentTime
        RealmHelper.createMobileEventLog(eventName: eventLogCase,
                                         startTime: currentTime,
                                         endTime: currentTime,
                                         note: "truck# \(RealmHelper.truckNumber() ?? ""), PDA: ",
            efuelMobileDeliveryShiftId: RealmHelper.shared.efuelMobDelvryShiftIdFromDeliveryShiftStatus)
    }
}
