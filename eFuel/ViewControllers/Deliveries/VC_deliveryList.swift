import UIKit
import RealmSwift


class VC_deliveryList: BaseViewController {

    private var array_r : [Remaining_Skip_Completed] = []
    private var array_c : [Remaining_Skip_Completed] = []
    private var array_s : [Remaining_Skip_Completed] = []
    
    
    
    
    @IBOutlet weak var tableV: UITableView!
    @IBOutlet weak var segmentC: UISegmentedControl!
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Delivery List"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadDeliveries()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func loadDeliveries() {
        let realm = try! Realm()
        
        var array_mixData : [Remaining_Skip_Completed] = []
        
        let existingCustomerLocal = realm.objects(MobileDeliveryQueue.self).filter("delivery_queue_id != nil AND record_type != \(RecordType.LoadTruck.rawValue) AND record_type != \(RecordType.unLoadTruck.rawValue)")
        
        existingCustomerLocal.forEach { (mobDeliveryQueue) in
            let obj = Remaining_Skip_Completed(delivery_skip_reason_id: mobDeliveryQueue.delivery_skip_reason_id.value, tank_id: mobDeliveryQueue.tank_id.value)
            array_mixData.append(obj)
        }
        
        let deliveryQueueIds = Array(existingCustomerLocal.map({$0.delivery_queue_id.value!}))
        
        let pred = NSPredicate(format: "NOT deliveryQueueId IN %@", deliveryQueueIds)
        
        let fromServer = realm.objects(DeliveryQueue.self).filter(pred)
        
        
        fromServer.forEach { (deliveryQueue) in
            
            let obj = Remaining_Skip_Completed(delivery_queue_id: deliveryQueue.deliveryQueueId,
                                               delivery_open_status_code: deliveryQueue.deliveryOpenStatusCode.value,
                                               Delivery_datetime: deliveryQueue.deliveryDatetime,
                                               truck_stop_number: deliveryQueue.truckStopNumber.value,
                                               tank_id: deliveryQueue.tankId.value,
                                               efuel_mobile_delivery_shift_id: deliveryQueue.efuelMobileDeliveryShiftId.value)

            array_mixData.append(obj)
        }
        
        
        array_mixData.forEach { (rem_skip_compltd) in
            print(rem_skip_compltd)
        }


        RealmHelper.fetchUsersByTankId(remaining_Skip_Completed: &array_mixData)

        self.array_r.removeAll()
        self.array_c.removeAll()
        self.array_s.removeAll()

        self.array_r = array_mixData.filter({$0.delivery_skip_reason_id == nil && $0.delivery_open_status_code == DeliveryStatus.open.rawValue})
        self.array_c = array_mixData.filter({$0.delivery_skip_reason_id == nil && $0.delivery_open_status_code == DeliveryStatus.completed.rawValue})
        self.array_s = array_mixData.filter({$0.delivery_skip_reason_id != nil})

        print(self.array_s)
        print(self.array_c)
        print(self.array_r)
        
        self.tableV.reloadData()
    }




    //MARK:- actions
    @IBAction func action_tabs(_ sender: UISegmentedControl) {
        self.tableV.reloadData()
    }
    
    
    
    
    @IBAction func action_sync(_ sender: Any) {
    }
}


//MARK:- UITableViewDataSource
extension VC_deliveryList : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch self.segmentC.selectedSegmentIndex {
        case 0:
            return self.array_r.count
        case 1:
            return self.array_c.count
        case 2:
            return self.array_s.count
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_deliveries_home", for:indexPath) as! Cell_deliveries_home
        
        switch self.segmentC.selectedSegmentIndex {
        case 0:
            cell.setupUI(obj: self.array_r[indexPath.row])
            break
        case 1:
            cell.setupUI(obj: self.array_c[indexPath.row])
            break
        case 2:
            cell.setupUI(obj: self.array_s[indexPath.row])
            break
        default:
            break
        }
        
        return cell
    }
}
