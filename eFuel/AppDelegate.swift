import UIKit
import SwiftDate


import IQKeyboardManagerSwift
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    var backgroundTaskTimer:Timer! = Timer()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        LocationManagr.shared.checkLocationPermission(shouldAskToUserForPermission: true)
        IQKeyboardManager.shared.enable = true
        SwiftDate.defaultRegion = Region.local // set region to local device attributes
        self.setupRealm()
        RealmHelper.uncheckTruckInspectReportItems()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        /*
        Singleton.stopBGTimerForLatLong()
        self.doBackgroundTask()
        */
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        /*
        if self.backgroundTaskTimer.isValid {
            self.backgroundTaskTimer.invalidate()
            self.backgroundTaskTimer = nil
        }
        
        Singleton.startBGTimerForLatLong()
        */
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


    private func setupRealm() {
        
        // Inside your application(application:didFinishLaunchingWithOptions:)
//
//        Realm.Configuration(fileURL: <#T##URL?#>,
//                            inMemoryIdentifier: <#T##String?#>,
//                            syncConfiguration: <#T##SyncConfiguration?#>,
//                            encryptionKey: RealmKey.getKey(),
//                            readOnly: <#T##Bool#>,
//                            schemaVersion: <#T##UInt64#>,
//                            migrationBlock: <#T##MigrationBlock?##MigrationBlock?##(Migration, UInt64) -> Void#>,
//                            deleteRealmIfMigrationNeeded: <#T##Bool#>,
//                            shouldCompactOnLaunch: <#T##((Int, Int) -> Bool)?##((Int, Int) -> Bool)?##(Int, Int) -> Bool#>,
//                            objectTypes: <#T##[Object.Type]?#>)
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).

                        
//            encryptionKey: RealmKey.getKey() as Data,                 // Encrypt Database
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                    
                    // The enumerateObjects(ofType:_:) method iterates
                    // over every Person object stored in the Realm file
                    /*
                    migration.enumerateObjects(ofType: Person.className()) { oldObject, newObject in
                        // combine name fields into a single field
                        let firstName = oldObject!["firstName"] as! String
                        let lastName = oldObject!["lastName"] as! String
                        newObject!["fullName"] = "\(firstName) \(lastName)"
                    }
                    */
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        
        let _ = try! Realm()
        
        print("RealM Location: \(String(describing: Realm.Configuration.defaultConfiguration.fileURL))")
        print("RealM schemaVersion: \(String(describing: Realm.Configuration.defaultConfiguration.schemaVersion))")
    }
}



//MARK:- timer when App in Background
extension AppDelegate {
    // MARK: - getLocationInBackground
    func doBackgroundTask() {
        
        guard let timeInterval = RealmHelper.shared.realm.objects(LoginUser.self).first?.gpsCollectionInterval, timeInterval > 0 else { return }
        DispatchQueue.main.async {
            self.beginBackgroundTask()
            
            if self.backgroundTaskTimer != nil {
                self.backgroundTaskTimer.invalidate()
                self.backgroundTaskTimer = nil
            }
            
            
            self.backgroundTaskTimer = Timer.scheduledTimer(timeInterval: TimeInterval(timeInterval), target: self, selector: #selector(self.startTracking), userInfo: nil, repeats: true)
            RunLoop.current.add(self.backgroundTaskTimer, forMode: RunLoop.Mode.default)
            RunLoop.current.run()
            
            // End the background task.
            self.endBackgroundTask()
        }
    }
    
    func beginBackgroundTask() {
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(withName: "Track trip", expirationHandler: {
            print("end background task")
            self.endBackgroundTask()
        })
    }
    
    func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskIdentifier.invalid
    }
    
    @objc func startTracking()
    {
        print("get location in background")
        //location update
        NotificationCenter.default.post(name: Notification.Name("LocationUpdateNotification"), object: nil)
        
        DispatchQueue.main.async {
            let backgroundTimeRemaining = UIApplication.shared.backgroundTimeRemaining
            print("backgroundTimeRemaining: \(backgroundTimeRemaining)")
            if Double(backgroundTimeRemaining) < 30
            {
                print("bg task end in 30 secs")
                if self.backgroundTaskTimer != nil {
                    self.backgroundTaskTimer.invalidate()
                    self.backgroundTaskTimer = nil
                }
                //location update significant
                NotificationCenter.default.post(name: Notification.Name("StartMonitoringSignificantLocation"), object: nil)
            }
        }
    }
}
