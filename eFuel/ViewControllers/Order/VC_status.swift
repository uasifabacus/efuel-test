import UIKit
import SwiftDate
import SwifterSwift

class VC_status: BaseViewController {

    @IBOutlet weak var lbl_gmId: Label!
    @IBOutlet weak var lbl_lastGMCall: Label!
    @IBOutlet weak var lbl_DMID: Label!
    @IBOutlet weak var lbl_lastPersonReading: Label!
    @IBOutlet weak var lbl_lastReading: Label!
    
    @IBOutlet weak var img_fillStatus: UIImageView!
    @IBOutlet weak var img_lowLvlAlert: UIImageView!
    @IBOutlet weak var img_sensorStatus: UIImageView!
    @IBOutlet weak var img_insideTempAlert: UIImageView!
    @IBOutlet weak var img_unusualUsg: UIImageView!
    @IBOutlet weak var img_radioQuality: UIImageView!
    
    
    
    private var selectedTank: TanksPricingCreditMonitorStatus? {
        didSet {
            self.updateUI()
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func setupUI() {
        let vc = self.parent as! Master_order
        self.selectedTank = vc.selectedTank
    }
    
    private func updateUI() {
        
        if let obj = self.selectedTank {
            
            var date = obj.gmLastCallinLocalTimeAtGMLocation?.toDate()
            
            
            
            self.lbl_gmId.text = obj.gwmInstallationID.value?.string
            self.lbl_lastGMCall.text = "\(date?.toFormat(k_MMDDYY) ?? "-") \(date?.toFormat(k_HHMMA) ?? "-")"
            self.lbl_DMID.text = obj.dmInstallationID.value?.string
            self.lbl_lastPersonReading.text = obj.floatLevel.value?.string
            date = obj.readingTime?.toDate()
            self.lbl_lastReading.text = "\(date?.toFormat(k_MMDDYY) ?? "-") \(date?.toFormat(k_HHMMA) ?? "-")"
            
            let timeInterval = DateInRegion()
            if let dateTime = obj.gmLastCallinLocalTimeAtGMLocation?.toDate() {
                
                let mySpan = dateTime - timeInterval
                if let numbOfDays = mySpan.toUnit(Calendar.Component.day) {
                    
                    if numbOfDays > 2 {
                        self.lbl_lastGMCall.textColor = .red
                    }
                    else {
                        self.lbl_lastGMCall.textColor = UIColor.green.darken()
                    }
                }
            }
            else {
                self.lbl_lastGMCall.textColor = UIColor.green.darken()
            }
            
            
            if let readingTime = obj.readingTime?.toDate() {
                let mySpan = timeInterval - readingTime
                if let numbOfDays = mySpan.toUnit(Calendar.Component.day) {

                    if numbOfDays > 2 {
                        self.lbl_lastReading.textColor = .red
                    }
                    else {
                        self.lbl_lastReading.textColor = UIColor.green.darken()
                    }
                }
            }
            else {
                self.lbl_lastReading.textColor = UIColor.green.darken()
            }
            
            self.img_fillStatus.image = SetImage(rawValue: obj.fillTriggerLast24_Hours?.trimmed ?? "")?.getImage()
            self.img_lowLvlAlert.image = SetImage(rawValue: obj.lowLevelAlert?.trimmed ?? "")?.getImage()
            self.img_sensorStatus.image = SetImage(rawValue: obj.sensorStatus?.trimmed ?? "")?.getImage()
            self.img_insideTempAlert.image = SetImage(rawValue: obj.insideTempAlert?.trimmed ?? "")?.getImage()
            self.img_unusualUsg.image = SetImage(rawValue: obj.unusualUsageAlert?.trimmed ?? "")?.getImage()
            self.img_radioQuality.image = SetImage(rawValue: obj.commLinkAlert?.trimmed ?? "")?.getImage()
        }
    }
}
