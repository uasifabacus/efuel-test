import Foundation
import CoreLocation


class LocationManagr : NSObject {
    
    static let shared = LocationManagr()
    
    
//    private lazy var locationManager = CLLocationManager()
    
    private lazy var locationManager: CLLocationManager = {
        var locationManager_ = CLLocationManager()
        locationManager_.delegate = self
        locationManager_.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager_.allowsBackgroundLocationUpdates = true
        locationManager_.pausesLocationUpdatesAutomatically = false
        locationManager_.requestAlwaysAuthorization()
        
        return locationManager_
    }()
    
    
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(startLocation), name: Notification.Name("LocationUpdateNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(startMonitoringSignificantLocation), name: Notification.Name("StartMonitoringSignificantLocation"), object: nil)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @discardableResult
    func checkLocationPermission(shouldAskToUserForPermission: Bool) -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            
            case .notDetermined, .restricted, .denied:
                if shouldAskToUserForPermission {
                    let _ = LocationManagr.shared.locationManager
//                    getLocationPermmission()
                }
                break
                
            case .authorizedAlways, .authorizedWhenInUse:
                let _ = LocationManagr.shared.locationManager
                if shouldAskToUserForPermission == false {
                    startLocation()
                }
                return true
                
            @unknown default:
                print("error in location")
                break
            }
        }
        else
        {
//            getLocationPermmission()
        }
        
        return false
    }
    
    
    
//    func getLocationPermmission()
//    {
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        locationManager.allowsBackgroundLocationUpdates = true
//        locationManager.pausesLocationUpdatesAutomatically = false
//        locationManager.requestAlwaysAuthorization()
//    }
    
    
    @objc func startLocation()
    {
        LocationManagr.shared.locationManager.delegate = self
        LocationManagr.shared.locationManager.startUpdatingLocation()
    }
    
    func stopLocation()
    {
        LocationManagr.shared.locationManager.stopUpdatingLocation()
        LocationManagr.shared.locationManager.delegate = nil
    }
    
    
    @objc func startMonitoringSignificantLocation()
    {
        // Set an accuracy level. The higher, the better for energy.
        LocationManagr.shared.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        /* Notify changes when device has moved x meters.
         * Default value is kCLDistanceFilterNone: all movements are reported.
         */
        LocationManagr.shared.locationManager.distanceFilter = 10.0;
        //self.locationManager?.activityType = .automotiveNavigation
        LocationManagr.shared.locationManager.allowsBackgroundLocationUpdates = true
        LocationManagr.shared.locationManager.pausesLocationUpdatesAutomatically = false
        LocationManagr.shared.locationManager.delegate = self
        LocationManagr.shared.locationManager.startMonitoringSignificantLocationChanges()
    }
}


// MARK: - CLLocationManagerDelegate Delegates
extension LocationManagr : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        print("didChangeAuthorization: \(status.rawValue)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let newLocation = locations.last {
            print("new locations \(newLocation)")
            
            
            self.stopLocation()
            
//            let speedToKPH = newLocation.speed * 3.6
//            print(speedToKPH)
            
            RealmHelper.GPSDataLog(lat: newLocation.coordinate.latitude,
                                   long: newLocation.coordinate.longitude)
//
//            postLocationinBackground(message: String(" Time: \(getCurrentTime()) Speed: \(speedToKPH) Kph latitude: \(newLocation.coordinate.latitude) longitude: \(newLocation.coordinate.longitude)"))
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("error: \(error)")
    }
}

