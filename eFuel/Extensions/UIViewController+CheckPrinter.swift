import Foundation
import UIKit

extension UIViewController {
    
    func isPrinterConnected(truckId:Int? = nil, isRecursive: Bool, completion: @escaping (Bool)->()) {
        
        
        if let truckId = truckId {
            let (deviceName, _, _) = ConfigManager.retrieveDeviceAndPrinter(truckId: truckId)
            
            guard deviceName.lowercased() != k_simulator else {
                completion( true )
                return
            }
        }
        
        
        
        
        if ConfigManager.isPrinterConnected() == false {
            showAlert(title: "Error",
                      message: "Printer is not connected, connect it and try again, Do you want to try again?",
                      buttonTitles: ["Yes", "No"],
                      highlightedButtonIndex: 1) { [weak self] (buttonTag) in
                        
                        guard let self_ = self else {return}
                        
                        if buttonTag == 0 {
                            self_.isPrinterConnected(isRecursive: isRecursive, completion: { (isSucceed) in})
                        }
                        else {
                            if isRecursive {
                                self_.askForEndShif()
                            }
                            
                            completion(false)
                        }
            }
        }
        else {
            completion(true)
        }
    }
    
    
    
    private func askForEndShif() {
        
        self.showAlert(title: "Do you want to end Shift?",
                       message: nil,
                       buttonTitles: ["Yes", "No"],
                       highlightedButtonIndex: nil,
                       completion: { [weak self] (buttonTag) in
                        
                        guard let self_ = self else {return}
                        if buttonTag == 0 {
                            self_.printShiftSummary()
                        }
                        else {
                            self_.navigationController?.popToRootViewController(animated: false)
                        }
        })
    }
    
    
    private func printShiftSummary() {
        
        self.showAlert(title: "Do you want to Print and End of Shift Summary?",
                       message: nil,
                       buttonTitles: ["Yes", "No"],
                       highlightedButtonIndex: nil,
                       completion: { [weak self] (buttonTag) in
                        
                        guard let self_ = self else {return}
                        if buttonTag == 0 {
                            
                            self?.isPrinterConnected(isRecursive: false, completion: { (isSucceed) in
                                if isSucceed {
                                    #warning("write in the notepad file if Simulator")
                                }
                                else {
                                    self_.mustUpload()
                                }
                            })
                        }
                        else {
                            
                            self_.mustUpload()
                        }
        })
    }
    
    
    
    private func mustUpload () {
        self.showAlert(title: nil,
                       message: "You must upload your End of Shift data before you can start a new shift. If you would like to upload it now, please connect to the internet, then press “YES”. Otherwise press “No” and you can upload at later time",
                       buttonTitles: ["Yes", "No"],
                       highlightedButtonIndex: nil,
                       completion: { (buttonTag) in
                        
                        if buttonTag == 0 {
                            WebMethods.uploadData(isOnlyUpload: true) { [weak self] (isSucceed) in      // first upload, then download, downloading happening inside "uploadData" method
                                
                                Helper.hideHud()
                                
                                guard let self_ = self, isSucceed else {
                                    Helper.showBanner(text: "Error in synchronization, please try again", isSuccess: false)
                                    return
                                }
                                
//                                self_.navigationController?.popToRootViewController(animated: false)
                                self_.navigationController?.dismiss(animated: true, completion: nil)
                            }
                        }
                        else {
//                            self.navigationController?.popToRootViewController(animated: false)
                            self.navigationController?.dismiss(animated: true, completion: nil)
                        }
        })
    }
}





extension UIViewController {
    private func isPrinterConnectedForShiftSummary(completion: ((Bool) -> Void)? = nil) {
        
        
        if let truckId = RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists {
            let (deviceName, _, _) = ConfigManager.retrieveDeviceAndPrinter(truckId: truckId)
            
            guard deviceName.lowercased() != k_simulator else {
                completion?( true )
                return
            }
        }
        
        
        
        
        if ConfigManager.isPrinterConnected() == false {
            showAlert(title: "Error",
                      message: "Printer is not connected, connect it and try again, Do you want to try again?",
                      buttonTitles: ["Yes", "No"],
                      highlightedButtonIndex: 1) { [weak self] (buttonTag) in
                        
                        guard let self_ = self else {return}
                        
                        if buttonTag == 0 {
                            self_.isPrinterConnectedForShiftSummary()
                        }
                        else {
                            completion?(false)
                        }
            }
        }
        else {
            completion?(true)
        }
    }
    
    
    func isPrinterFoundForShiftSummary(completion: @escaping (Bool)->()) {
        
        self.showAlert(title: "Do you want to Print an End of Shift Summary?",
                       message: nil,
                       buttonTitles: ["Yes", "No"],
                       highlightedButtonIndex: nil,
                       completion: { [weak self] (buttonTag) in
                        
                        if buttonTag == 0 {
                            
                            self?.isPrinterConnectedForShiftSummary( completion: { (isSucceed) in
                                completion(isSucceed)
                            })
                        }
                        else {

                            completion(false)
                        }
        })
    }
    
    
    
    private func mustUploadForShiftSummary () {
        self.showAlert(title: nil,
                       message: "You must upload your End of Shift data before you can start a new shift.if you would like to upload it now, please connect to the internet, then press “YES”. Otherwise press “No” and you can upload at later time",
                       buttonTitles: ["Yes", "No"],
                       highlightedButtonIndex: nil,
                       completion: { (buttonTag) in
                        
                        if buttonTag == 0 {
                            WebMethods.uploadData(isOnlyUpload: true) { [weak self] (isSucceed) in      // first upload, then download, downloading happening inside "uploadData" method
                                
                                Helper.hideHud()
                                
                                guard let self_ = self, isSucceed else {
                                    Helper.showBanner(text: "Error in synchronization, please try again", isSuccess: false)
                                    return
                                }
                                
                                self_.navigationController?.popToViewController((self_.navigationController?.viewControllers[1])!, animated: true)
                            }
                        }
                        else {
                            self.navigationController?.dismiss(animated: true, completion: nil)
                        }
        })
    }
}


extension UIViewController {
    
    
    func printLoadUnloadSummary(message: String, completion: @escaping ((Bool, DeviceType?)->(Void))) {
        
        self.showAlert(title: message,
                       message: nil,
                       buttonTitles: ["Yes", "No"],
                       highlightedButtonIndex: nil,
                       completion: { [weak self] (buttonTag) in
                        
                        if buttonTag == 0 {
                            
                            self?.isPrinterConected(truckId: RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists, completion: { (isSucceed, deviceType) in
                                completion(isSucceed, deviceType)
                            })
                        }
                        else {
                            completion(true, nil)
//                            self?.navigationController?.popViewController(animated: true)
                        }
        })
    }
}





//MARK:- Extension for VC_register
extension UIViewController {
    
    func isPrinterConected(truckId:Int? = nil, completion: @escaping (Bool, DeviceType?)->()) {
        
        
        if let truckId = truckId {
            let (deviceName, _, _) = ConfigManager.retrieveDeviceAndPrinter(truckId: truckId)
            
            guard deviceName.lowercased() != k_simulator else {
                completion( true, .simulator )
                return
            }
        }
        
        
        
        
        if ConfigManager.isPrinterConnected() == false {
            showAlert(title: "Error",
                      message: "Unable to communicate with electronic register. Please make sure the cable is connected.",
                      buttonTitles: ["Ok"],
                      highlightedButtonIndex: 1) { (buttonTag) in
                        
                        completion(false, nil)
            }
        }
        else {
            completion(true, .LCR)
        }
    }
}
