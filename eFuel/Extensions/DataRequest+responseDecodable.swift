import Alamofire



// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers



extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseGenerateToken(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<GenerateToken>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseCallDetailRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<CallDetailRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    
    @discardableResult
    func responseAuthenticateUser(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<AuthenticateUser>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseCallLogRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<CallLogRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseLookUpRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<LookUpRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseTruckRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<TruckRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseUserAccountsRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<UserAccountsRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseInstalltionStreetAddressesRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<InstalltionStreetAddressesRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseTanksPricingCreditMonitorStatusRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<TanksPricingCreditMonitorStatusRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseDeliveryInfoRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<DeliveryInfoRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseDeliverySkipCategoriesRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<DeliverySkipCategoriesRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseDeliverySkipReasonsRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<DeliverySkipReasonsRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseTruckInspectionReportItemsRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<TruckInspectionReportItemsRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseDeliveryQueueRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<DeliveryQueueRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseMobileDeliveryTicketRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<MobileDeliveryTicketRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseMobileDeliverySurchargeLabelsRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<MobileDeliverySurchargeLabelsRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseDeliveryShiftStatusRoot(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<DeliveryShiftStatusRoot>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
