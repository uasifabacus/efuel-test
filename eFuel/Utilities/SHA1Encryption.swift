import UIKit
import CryptoSwift

import CommonCrypto

//extension String {
//    func sha1() -> String {
//        let data = Data(self.utf8)
//        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
//        data.withUnsafeBytes {
//            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
//        }
//        let hexBytes = digest.map { String(format: "%02hhx", $0) }
//        return hexBytes.joined()
//    }
//}



class TestCrypto: NSObject {
    
    private let k_key = "D30A408F-C989-4984-8A65-51F2CE53s44BF"
    
    
    /*
    func myEncrypt(encryptData:String) -> NSData?{
        
        var myKeyData : Data = Data(base64Encoded: k_key, options: .utf8)
        var myRawData : NSData = encryptData.dataUsingEncoding(NSUTF8StringEncoding)!
        var iv : [UInt8] = [56, 101, 63, 23, 96, 182, 209, 205]  // I didn't use
        var buffer_size : size_t = myRawData.length + kCCBlockSize3DES
        var buffer = UnsafeMutablePointer<NSData>.alloc(buffer_size)
        var num_bytes_encrypted : size_t = 0
        
        let operation: CCOperation = UInt32(kCCEncrypt)
        let algoritm:  CCAlgorithm = UInt32(kCCAlgorithm3DES)
        let options:   CCOptions   = UInt32(kCCOptionECBMode + kCCOptionPKCS7Padding)
        let keyLength        = size_t(kCCKeySize3DES)
        
        var Crypto_status: CCCryptorStatus = CCCrypt(operation, algoritm, options, myKeyData.bytes, keyLength, nil, myRawData.bytes, myRawData.length, buffer, buffer_size, &num_bytes_encrypted)
        
        if UInt32(Crypto_status) == UInt32(kCCSuccess){
            
            var myResult: NSData = NSData(bytes: buffer, length: num_bytes_encrypted)
            
            free(buffer)
            println("my result \(myResult)") //This just prints the data
            
            let keyData: NSData = myResult
            let hexString = keyData.toHexString()
            println("hex result \(hexString)") // I needed a hex string output
            
            
            myDecrypt(myResult) // sent straight to the decryption function to test the data output is the same
            return myResult
        }else{
            free(buffer)
            return nil
        }
    }
    
    
    func myDecrypt(decryptData : NSData) -> NSData?{
        
        var mydata_len : Int = decryptData.length
        var keyData : NSData = ("myEncryptionKey" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
        
        var buffer_size : size_t = mydata_len+kCCBlockSizeAES128
        var buffer = UnsafeMutablePointer<NSData>.alloc(buffer_size)
        var num_bytes_encrypted : size_t = 0
        
        var iv : [UInt8] = [56, 101, 63, 23, 96, 182, 209, 205]  // I didn't use
        
        let operation: CCOperation = UInt32(kCCDecrypt)
        let algoritm:  CCAlgorithm = UInt32(kCCAlgorithm3DES)
        let options:   CCOptions   = UInt32(kCCOptionECBMode + kCCOptionPKCS7Padding)
        let keyLength        = size_t(kCCKeySize3DES)
        
        var decrypt_status : CCCryptorStatus = CCCrypt(operation, algoritm, options, keyData.bytes, keyLength, nil, decryptData.bytes, mydata_len, buffer, buffer_size, &num_bytes_encrypted)
        
        if UInt32(decrypt_status) == UInt32(kCCSuccess){
            
            var myResult : NSData = NSData(bytes: buffer, length: num_bytes_encrypted)
            free(buffer)
            println("decrypt \(myResult)")
            
            var stringResult = NSString(data: myResult, encoding:NSUTF8StringEncoding)
            println("my decrypt string \(stringResult!)")
            return myResult
        }else{
            free(buffer)
            return nil
            
        }
    }
    */
    
    
    
    
    
    
    
    
    func encrypt(str: String) {
        
        if let base64cipher = try? Rabbit(key: "1234567890123456"),
            let base64 = try? str.encryptToBase64(cipher: base64cipher) {
            print("encrypted base64: \(base64)")
            
            let decrypted = try? base64.decryptBase64ToString(cipher: base64cipher)
            print("decrypted base64: \(decrypted!)")
        }
        
        
        
        
        let key:Array<UInt8> = Array(k_key.utf8)
        let bytes: Array<UInt8> = Array(str.utf8)
        do {
            let obj = try HMAC(key: k_key, variant: .sha1).authenticate(bytes)
            if let string = String(bytes: obj, encoding: .utf8) {
                print(string)
            } else {
                print("not a valid UTF-8 sequence")
            }
            
            
        } catch  {
            print(error)
        }
        
        
        
        
        
        
        
        
//        let bytes: Array<UInt8> = Array(str.utf8)
        do {
            let obj = try HMAC(key: k_key, variant: .sha1).authenticate(bytes)
            print(obj)
            
            if let string = String(bytes: obj, encoding: .utf8) {
                print(string)
            } else {
                print("not a valid UTF-8 sequence")
            }
            
        } catch {
            print(errno)
            print(error)
        }
        
        
//        let encryptedStr = str.sha1()
//        print(encryptedStr)
    }
}
