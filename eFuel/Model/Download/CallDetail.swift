import Foundation
import Alamofire
import RealmSwift
import Realm

// MARK: - CallDetail
class CallDetailRoot: Codable {
    @objc dynamic var message, errorDetail: String?
    @objc dynamic var statusCode: Int = 0
    @objc dynamic var array: [CallDetail]?
    
    private enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    /*
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [CallDetailList]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.data = data
    }
    */
    
    //MARK:- realm
//    override static func primaryKey() -> String? {
//        return "id"
//    }
    
    
//    required init() {
//        super.init()
//    }
//    
//    required init(value: Any, schema: RLMSchema) {
//        super.init(value: value, schema: schema)
//    }
//    
//    required init(realm: RLMRealm, schema: RLMObjectSchema) {
//        super.init(realm: realm, schema: schema)
//    }
    //MAKR:- realm
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - CallDetailList
class CallDetail: RealmSwift.Object, Codable {
    @objc dynamic var callDetailID: Int = 0
    @objc dynamic var calllogSQLUid: Int = 0
    @objc dynamic var callNotes, loginUserName, callCompletionDate, lastInsertUpdateTimeStamp: String?
    var callDateFromJSON: String?
    @objc dynamic var callDate: Date? = nil
    
    enum CodingKeys: String, CodingKey {
        case callDetailID = "call_detail_id"
        case calllogSQLUid = "calllog_sql_uid"
        case callDateFromJSON = "call_date"
        case callNotes = "call_notes"
        case loginUserName = "login_user_name"
        case callCompletionDate = "call_completion_date"
        case lastInsertUpdateTimeStamp = "last_insert_update_time_stamp"
    }
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "callDetailID"
    }
    
    
    override class func ignoredProperties() -> [String] {
        return ["callDateFromJSON"]
    }
    
    
    required init() {
        super.init()
    }
    
    
    
    convenience init(callDetailID: Int, calllogSQLUid: Int, callNotes: String?, loginUserName: String?, callCompletionDate: String?, lastInsertUpdateTimeStamp: String?, callDate: String?) {
        
        self.init()
        self.callDetailID = callDetailID
        self.calllogSQLUid = calllogSQLUid
        self.callNotes = callNotes
        self.loginUserName = loginUserName
        self.callCompletionDate = callCompletionDate
        self.lastInsertUpdateTimeStamp = lastInsertUpdateTimeStamp
        self.callDate = callDate?.toDate()?.date
    }
    
    
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
