import UIKit

class Master_utilities: BaseViewController {

    
    @IBOutlet weak var sgControl: UISegmentedControl!
    
    private lazy var vc_syncStatus: VC_syncStatus = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_syncStatus") as! VC_syncStatus
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    private lazy var vc_summary: VC_summary = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_summary") as! VC_summary
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    private lazy var vc_inspectionReport: VC_inspectionReport = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_inspectionReport") as! VC_inspectionReport
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    private lazy var vc_register: VC_register = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_register") as! VC_register
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    
    
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Utilities"
        
        
        self.action_changeSegment(self.sgControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    private func enableDisableSummarySection() {
        
        if RealmHelper.shared.inProgressFlag == false {
            self.sgControl.setEnabled(false, forSegmentAt: 1)
        }
    }
    
    
    
    //MARK:- Actions
    @IBAction func action_changeSegment(_ sender: UISegmentedControl) {
        
        self.remove(asChildViewController: self.vc_syncStatus)
        self.remove(asChildViewController: self.vc_summary)
        self.remove(asChildViewController: self.vc_inspectionReport)
        self.remove(asChildViewController: self.vc_register)
        
        if sender.selectedSegmentIndex == 0 {
            self.addViewControllerAsChildVC(childVC: self.vc_syncStatus)
        }
        if sender.selectedSegmentIndex == 1 {
            self.addViewControllerAsChildVC(childVC: self.vc_summary)
        }
        if sender.selectedSegmentIndex == 2 {
            self.addViewControllerAsChildVC(childVC: self.vc_inspectionReport)
        }
        if sender.selectedSegmentIndex == 3 {
            self.addViewControllerAsChildVC(childVC: self.vc_register)
        }
    }
}




//"New Case"
//
//Call_log table is parent
//Call_detail is child which contains additional notes
