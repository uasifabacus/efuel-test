import UIKit

class Cell_order_tanks: UITableViewCell {

    @IBOutlet weak var lbl_serialNumb: UILabel!
    @IBOutlet weak var lbl_size: UILabel!
    @IBOutlet weak var lbl_wc: UILabel!
    @IBOutlet weak var lbl_tankNumb: UILabel!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setupUI(obj: TanksPricingCreditMonitorStatus) {
        self.lbl_serialNumb.text = obj.serialNumOnTank
        self.lbl_wc.text = obj.willCallFlag
        self.lbl_size.text = obj.tankSizeGals.value?.string ?? "-"
        self.lbl_tankNumb.text = obj.tankNum.value?.string ?? "-"
    }
}
