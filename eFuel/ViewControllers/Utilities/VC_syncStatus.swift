import UIKit
import RealmSwift
import Alamofire

class VC_syncStatus: BaseViewController {

    private let k_green = UIImage(named: "green-dot")
    private let k_red = UIImage(named: "orange-dot")
    
    @IBOutlet weak var img_GPSConnection: UIImageView!
    @IBOutlet weak var img_syncStatus: UIImageView!
    @IBOutlet weak var img_syncEnabled: UIImageView!
    
    
    @IBOutlet weak var lbl_lastSync: Label!
    @IBOutlet weak var lbl_lastAttempt: Label!
    @IBOutlet weak var lbl_syncInterval: Label!
    @IBOutlet weak var lbl_GPSInterval: Label!
    @IBOutlet weak var lbl_comPort: Label!
    @IBOutlet weak var lbl_connectionType: Label!
    
    
    let user = try! Realm().objects(LoginUser.self).first
    let syncData = try! Realm().objects(MobileSyncLog.self)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func setupUI() {

        if LocationManagr.shared.checkLocationPermission(shouldAskToUserForPermission: false) {
            self.img_GPSConnection.image = k_green
        }
        else {
            self.img_GPSConnection.image = k_red
        }


        if let interval = user?.dataSyncInterval, interval > 0, Singleton.shared.timer_bg != nil {
            self.img_syncEnabled.image = k_green
        }
        else {
            self.img_syncEnabled.image = k_red
        }

        
        let predicate = NSPredicate(format: "sync_status = %@", k_ok)
        if let obj = self.syncData.filter(predicate).sorted(byKeyPath: "sync_time").last {
            self.lbl_lastSync.text = obj.sync_time ?? "-"
        }
        else {
            self.lbl_lastSync.text = "-"
        }
        
        
        if let obj = self.syncData.sorted(byKeyPath: "sync_time").last {
            self.lbl_lastAttempt.text = obj.sync_time ?? "-"
        }
        else {
            self.lbl_lastAttempt.text = "-"
        }
        
        
        if let obj = self.user {
            self.lbl_syncInterval.text = obj.dataSyncInterval.string
        }
        else {
            self.lbl_syncInterval.text = "-"
        }
        
        
        
        if let truck = RealmHelper.shared.selectedTruckDetails {
            
            if let colInterval = truck.gpsCollectionInterval.value?.string {
                self.lbl_GPSInterval.text = colInterval
            }
            else {
                self.lbl_GPSInterval.text = "-"
            }
            
            
            if let comPort = truck.gpsCOMPort.value?.string {
                self.lbl_comPort.text = comPort
            }
            else {
                self.lbl_comPort.text = "-"
            }
        }
        else {
            self.lbl_GPSInterval.text = "-"
            self.lbl_comPort.text = "-"
        }
        
        
        self.isNetworkReachable()
    }
    
    
    private func isNetworkReachable() {
        
        let net = NetworkReachabilityManager()
        
        net?.listener = { status in
            if net?.isReachable ?? false {
                
                switch status {
                    
                case .reachable(.ethernetOrWiFi):
                    print("The network is reachable over the WiFi connection")
                    self.lbl_connectionType.text = "WiFi"
                    
                case .reachable(.wwan):
                    print("The network is reachable over the WWAN connection")
                    self.lbl_connectionType.text = "WWAN"
                    
                case .notReachable:
                    print("The network is not reachable")
                    self.lbl_connectionType.text = "No Connection"
                    
                case .unknown :
                    print("It is unknown whether the network is reachable")
                    self.lbl_connectionType.text = "No Connection"
                    
                }
                
                net?.stopListening()
            }
        }
        
        net?.startListening()
    }
}
