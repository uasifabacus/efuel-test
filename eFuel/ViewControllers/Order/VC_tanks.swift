import UIKit

class VC_tanks: BaseViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lbl_customer: Label!
    @IBOutlet weak var lbl_installationStreetAddress: Label!
    @IBOutlet weak var tf_date: TextField!
    
    
    
    
    private var array_data : [InstalltionStreetAddresses]! {
        didSet {
            self.tblView.reloadData {
                if self.array_data.count > 0 {
                    self.defaultSelectedRow()
                }
            }
        }
    }
    
    private var selectedAddress: InstalltionStreetAddresses! {
        didSet {
            
            self.updateParentPropertyOfAddress()
            
            var strAddress : String = ""
            
            if let addressLine1 = self.selectedAddress.installationStreetAddressLine1 {
                strAddress = addressLine1.trimmed
            }
            else {
                if let addressLine2 = self.selectedAddress.installationStreetAddressLine2 {
                    strAddress = addressLine2.trimmed
                }
            }
            
            self.lbl_installationStreetAddress.text = "\(strAddress) \n\(self.selectedAddress.installationCity ?? "") \n\(self.selectedAddress.installationState ?? "") \n\(self.selectedAddress.installationZip ?? "")"
        }
    }

    
    
    //MARK:- Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func setupUI() {
        let vc = self.parent as! Master_order
        self.array_data = vc.selectedUser.installationStreetAddresses.filter({$0.tanksPricingCreditMonitorStatus.count > 0 })
        
        self.lbl_customer.text = vc.selectedUser.userAccountName ?? "-"
        self.tf_date.text = Date().toFormat(k_MMDDYY)
    }

    private func updateParentPropertyOfAddress () {
        let vc = self.parent as! Master_order
        vc.selectedTank = self.selectedAddress.tanksPricingCreditMonitorStatus.first
    }
    
    
    private func defaultSelectedRow() {
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.tblView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        self.tblView.delegate?.tableView?(self.tblView, didSelectRowAt: indexPath)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? VC_calendar {
            vc.popOverSetting(widthPercent: 70, heightPercent: 70)
            vc.delegate_ = self
        }
    }
}



//MARK:- UITableViewDataSource
extension VC_tanks : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_order_tanks", for:indexPath) as! Cell_order_tanks
        cell.setupUI(obj: (self.array_data?[indexPath.row].tanksPricingCreditMonitorStatus.first)!)
        return cell
    }
}



//MARK:- UITableViewDelegate
extension VC_tanks : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedAddress = self.array_data[indexPath.row]
    }
}



//MARK:- UITextFieldDelegate
extension VC_tanks : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.performSegue(withIdentifier: "segue_calendar", sender: nil)
        return false
    }
}


//MARK:- PassDataPopoverToParent
extension VC_tanks : PassDataPopoverToParent {
    func passData(obj: Any?, vc: UIViewController) {
        
        if let date = obj as? Date {
            self.tf_date.text = date.toFormat(k_MMDDYY)
        }
    }
}
