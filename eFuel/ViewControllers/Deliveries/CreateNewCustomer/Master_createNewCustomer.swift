import UIKit

class Master_createNewCustomer: BaseViewController {

    
    @IBOutlet weak var sgControl: UISegmentedControl!
    
    private lazy var vc_newCustomerAcc: VC_newCustomerAcc = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_newCustomerAcc") as! VC_newCustomerAcc
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    private lazy var vc_newCustomerTanks: VC_newCustomerTanks = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_newCustomerTanks") as! VC_newCustomerTanks
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    private lazy var vc_newCustomerNotes: VC_newCustomerNotes = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_newCustomerNotes") as! VC_newCustomerNotes
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Add New Customer & Tanks"
        
        self.action_changeSegment(self.sgControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    //MARK:- Actions
    @IBAction func action_changeSegment(_ sender: UISegmentedControl) {
        
        self.vc_newCustomerAcc.view.isHidden = !(sender.selectedSegmentIndex == 0)
        self.vc_newCustomerTanks.view.isHidden = !(sender.selectedSegmentIndex == 1)
        self.vc_newCustomerNotes.view.isHidden = !(sender.selectedSegmentIndex == 2)
    }
}
