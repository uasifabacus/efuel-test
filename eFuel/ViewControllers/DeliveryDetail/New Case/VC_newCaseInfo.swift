import UIKit

class VC_newCaseInfo: BaseViewController {

    private static let array_assignedTo = ["Urgent", "Normal", "Low", "Low", "Low"]
    private static let array_priority = ["Urgent", "Normal", "Low"]
    
    
    @IBOutlet weak var table_assignedTo: UITableView!
    @IBOutlet weak var table_priority: UITableView!
    
    
    
    
    //MARK:- Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



//MARK:- UITableViewDataSource
extension VC_newCaseInfo : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.table_assignedTo {
            return VC_newCaseInfo.array_assignedTo.count
        }
        
        return VC_newCaseInfo.array_priority.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_newCaseReason") as! Cell_newCaseReason
        
        if tableView == self.table_assignedTo {
            cell.lbl_title.text = VC_newCaseInfo.array_assignedTo[indexPath.row]
            return cell
        }
        else {
            cell.lbl_title.text = VC_newCaseInfo.array_priority[indexPath.row]
            return cell
        }
    }
}



//MARK:- UITableViewDelegate
extension VC_newCaseInfo : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.table_assignedTo {
            print("Assigned to")
        }
        else {
            print("Priority")
        }
    }
}
