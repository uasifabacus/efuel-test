import UIKit
import SwiftDate

class VC_login: BaseViewController {

    @IBOutlet weak var tf_userName: TextField!
    @IBOutlet weak var tf_password: TextField!
    
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addNotifier()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        PDFManager.makePDFAndSendToPrinter(view: self.view)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    //MARK:- action login
    @IBAction func action_login(_ sender: Any) {
        
        guard let userName = Helper.validateAndTrimText(TF: self.tf_userName, message: "Enter User Name") else { return }
        guard let pass = Helper.validateAndTrimText(TF: self.tf_password, message: "Enter password") else { return }


        WebMethods.generateToken { (obj) in
            
            if RealmHelper.isUserAtLocalDB(userName: userName, password: pass) == false {
                
                WebMethods.authenticateUser(userName: userName, pass: pass) { [weak self] (userDetail) in
                    
                    guard let self_ = self, userDetail != nil else { return }
                    self_.checkShift()
                }
            }
            else {
                self.checkShift()
            }
        }
    }
    
    
    private func checkShift() {
        
        if RealmHelper.shared.isDisableEfuelMobile {
            
            switch RealmHelper.shared.isShiftInProgress {
            case .err:
                Helper.showBanner(text: "Error checking shift status", isSuccess: false)
                break
                
            case .completeShift:
                
                showAlert(title: "Warning",
                          message: "An existing completed shift needs to be upload, Upload Now?",
                          buttonTitles: ["Yes", "No"],
                          highlightedButtonIndex: 0) { (buttonTag) in
                            if buttonTag == 0 {
                                
                                WebMethods.uploadData(shouldUploadMobileDelvryShift: false, isOnlyUpload: true, completion: { [weak self] (isSucceed) in
                                    Helper.hideHud()
                                    guard let self_ = self else { return }
                                    if isSucceed {
                                        RealmHelper.deleteFromDB(type: MobileDeliveryShift.self)
                                        self_.goToDownload()
                                    }
                                })
                            }
                }
                break
                
            case .openShiftNewUser:
                Helper.showBanner(text: "Unauthorized User", isSuccess: false)
                break
                
            case .openShiftOldUser:
                self.goToDownload()
                break
                
            case .noShift:
                self.goToDownload()
                break
            }
        }
    }
    
    
    private func goToDownload() {
        
        Singleton.shared.loginTime = Helper.mmDDYYWithCurrentTime
        self.performSegue(withIdentifier: "segue_nav_downloadData", sender: nil)
    }
}
