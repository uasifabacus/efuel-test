import UIKit
import DeviceKit

class Label: UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        print("Device.current: \(Device.current)")
        
        if Device.current == .simulator(.iPadAir) ||
            Device.current == .iPadAir {
            self.font = self.font.withSize(24.0)
        }
    }
}
