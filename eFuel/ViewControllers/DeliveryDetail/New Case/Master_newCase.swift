import UIKit

class Master_newCase: BaseViewController {

    @IBOutlet weak var sgControl: UISegmentedControl!
    
    private lazy var vc_newCaseInfo: VC_newCaseInfo = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_newCaseInfo") as! VC_newCaseInfo
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    private lazy var vc_newCaseReason: VC_newCaseReason = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_newCaseReason") as! VC_newCaseReason
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    private lazy var vc_newCaseAction: VC_newCaseAction = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_newCaseAction") as! VC_newCaseAction
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    private lazy var vc_newCaseNotes: VC_newCaseNotes = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_newCaseNotes") as! VC_newCaseNotes
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "New Case"
        
        self.action_changeSegment(self.sgControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- Actions
    @IBAction func action_changeSegment(_ sender: UISegmentedControl) {
        
        self.vc_newCaseInfo.view.isHidden = !(sender.selectedSegmentIndex == 0)
        self.vc_newCaseReason.view.isHidden = !(sender.selectedSegmentIndex == 1)
        self.vc_newCaseAction.view.isHidden = !(sender.selectedSegmentIndex == 2)
        self.vc_newCaseNotes.view.isHidden = !(sender.selectedSegmentIndex == 3)
    }
}
