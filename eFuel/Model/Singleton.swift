import UIKit
import SwiftDate
import RealmSwift

class Singleton: NSObject {

    static let shared = Singleton()
    
    var timer_bg : RepeatingTimer?
    var timer_bg_lat_long : RepeatingTimer?
    
    
    var generateToken: GenerateToken?
    var fromTableDeviceType: Int = -1
    var fromTablePrinterType: Int = -1
    var XmlDeviceType: Int = -1
    var XmlPrinterType: Int = -1

    
    var loginTime: String!
    var syncRegisterTime: String?
    
    
    var userDetail : LoginUser {
        
        let realm = try! Realm()
        return realm.objects(LoginUser.self)[0]
    }
    
    
    
    
    
    var concatinatedToken: String {
        
        if let generateToken = Singleton.shared.generateToken, let token = generateToken.accessToken {
            return "bearer \(token)"
        }
        else {
            return ""
        }
    }
    
    var mmDDYY : String {
        let date = DateInRegion()
        return date.toString(DateToStringStyles.custom("MM-dd-yyyy"))
    }
    
    
    /*
    lazy var mmDDYYWithCurrentTime = { () -> String in
        let date = DateInRegion()

        return date.toString(DateToStringStyles.custom("MM-dd-yyyy hh:mm:ss.SSSS"))
    }()
    */
    
    
    
    
}





//MARK:- BG Timer For User activity Logging
extension Singleton {
    
    private static func startBGTimerForLogging() {
        
        if let interval = RealmHelper.shared.realm.objects(LoginUser.self).first?.dataSyncInterval, interval > 0 {
            
            guard Singleton.shared.timer_bg == nil else {
                Singleton.shared.timer_bg!.resume()
                return
            }
            
            Singleton.shared.timer_bg = RepeatingTimer(timeInterval: TimeInterval(interval*60))
            Singleton.shared.timer_bg!.eventHandler = {
                print("Timer Fired for loggin into DB at: \(Helper.mmDDYYWithCurrentTime)")
                WebMethods.uploadLogsInBG()
            }
            Singleton.shared.timer_bg!.resume()
        }
    }
    
    
    
    private static func stopBGTimerForLogging() {
        if let timer = Singleton.shared.timer_bg {
            timer.suspend()
        }
    }
}






//MARK:- BG Timer For Lat Long
extension Singleton {
    
    private static func startBGTimerForLatLong() {

        guard RealmHelper.shared.isGPSEnabled else { return }
        guard let interval = RealmHelper.shared.realm.objects(LoginUser.self).first?.gpsCollectionInterval, interval > 0 else {return}
        
        guard Singleton.shared.timer_bg_lat_long == nil else {
            Singleton.shared.timer_bg_lat_long!.resume()
            return
        }
        
        Singleton.shared.timer_bg_lat_long = RepeatingTimer(timeInterval: TimeInterval(interval))
        Singleton.shared.timer_bg_lat_long!.eventHandler = {
            print("Timer Fired for Lat long at: \(Helper.mmDDYYWithCurrentTime)")
            LocationManagr.shared.checkLocationPermission(shouldAskToUserForPermission: false)
        }
        Singleton.shared.timer_bg_lat_long!.resume()
    }
    
    
    
    private static func stopBGTimerForLatLong() {
        if let timer = Singleton.shared.timer_bg_lat_long {
            timer.suspend()
        }
    }
}


//MARK:- Start/Stop Timmers
extension Singleton {
    static func startTimmers() {
        startBGTimerForLatLong()
        startBGTimerForLogging()
    }
    
    static func stopTimmers() {
        stopBGTimerForLatLong()
        stopBGTimerForLogging()
    }
}
