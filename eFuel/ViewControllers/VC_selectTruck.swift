import UIKit

class VC_selectTruck: BaseViewController {

    
    
    private lazy var array = {
        RealmHelper.shared.realm.objects(Trucks.self)
    }()
    
    
    
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addNotifier()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? VC_truckInspection, let truck = sender as? Trucks {
            vc.truck = truck
        }
        
        if let vc = segue.destination as? VC_shiftSummary, let truck = sender as? Trucks{
            vc.truck = truck
        }
    }
}



//MARK:- UITableViewDataSource
extension VC_selectTruck : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_deliveries_home", for:indexPath) as! Cell_deliveries_home
        cell.setupUI(obj: array[indexPath.row])
        return cell
    }
}




//MARK:-UITableViewDelegate
extension VC_selectTruck : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.deSelect(table: tableView)
        
        
        
        
        guard ConfigManager.isDeviceVerified(truckId: self.array[indexPath.row].truckID) else { return }
        
        
        
        WebMethods.getDeliveryShiftStatus(truckId: self.array[indexPath.row].truckID) { [weak self] (isSucceed) in
            guard isSucceed, let self_ = self else { return }
            

//            let obj = self_.array[indexPath.row]
//            
//            
//            #warning("ye Abdul Ahad sy discuss karna hai, ye meny apni taraf sy kia hai")
//            try! RealmHelper.shared.realm.write {
//                obj.truckInUse.value = true
//            }
            
            var nothing: Int = -1
            
            switch RealmHelper.findShiftStatusInfo(last_manual_sale_number: &nothing, generateOne: false) {
            case .login?:
                print("login")
                self_.showAlert(title: nil,
                                message: "You have an open shift that must be closed before starting a new shift. The shift is either open on another Device or it may have been destroyed, in which case transactions may be lost. Contact your office or The Fuel Web to continue.",
                                buttonTitles: ["Ok"],
                                highlightedButtonIndex: nil,
                                completion: {(buttonTag) in
                                    self_.navigationController?.dismiss(animated: true, completion: nil)
                })
                break
            case .truck?:
                self_.showAlert(title: nil,
                                message: "There is an open shift for that truck that must be closed before starting a new shift. The shift is either open on another Device or it may have been destroyed, in which case transactions may be lost.  Contact your office or The Fuel Web to continue.",
                                buttonTitles: ["Ok"],
                                highlightedButtonIndex: nil,
                                completion: {(buttonTag) in
                                    self_.navigationController?.dismiss(animated: true, completion: nil)
                })
                
                
                break
            case .newId?:
                print("newId")
                WebMethods.downloadData(level: .isShiftStarting, completion: { [weak self] (isSucceed) in
                    
                    Helper.hideHud()
                    
                    guard let self_ = self, isSucceed else {
                        Helper.showBanner(text: "Unable to fetch Tickets & Surcharge Labels", isSuccess: false)
                        return
                    }
                    
                    self_.performSegue(withIdentifier: "segue_truckInspection", sender: self?.array[indexPath.row])
                    
//                    WebMethods.upload_mobile_delivery_shift(shouldRemoveMobDeliveryShiftFromLocal: false, completion: { [weak self] (isSucceed) in
//                        guard let self_ = self, isSucceed else {
//                            Helper.showBanner(text: "Error in upload Mobile Delivery Shift", isSuccess: false)
//                            return
//                        }
//
//                        self_.performSegue(withIdentifier: "segue_truckInspection", sender: self?.array[indexPath.row])
//                    })
                        
                        
                        
                    /*
                    if RealmHelper.createOrUpdateMobileDeliveryShifts(timeOut: Helper.mmDDYYWithCurrentTime,
                                                              truckId: self_.array[indexPath.row].truckID) {
                        
                        WebMethods.upload_mobile_delivery_shift(shouldRemoveMobDeliveryShiftFromLocal: false, completion: { [weak self] (isSucceed) in
                            guard let self_ = self, isSucceed else {
                                Helper.showBanner(text: "Error in upload Mobile Delivery Shift", isSuccess: false)
                                return
                            }

                            self_.performSegue(withIdentifier: "segue_truckInspection", sender: nil)
                        })
                    }
                    */
                })
                break
            case .last_manual_sale_number?:
                print("last_manual_sale_number")
                Helper.showBanner(text: "last manual sale number", isSuccess: false)
                break
            default:
                print("Error")
                Helper.showBanner(text: "Error in finding shift status info.", isSuccess: false)
                break
            }
            
            

            
//            self_.performSegue(withIdentifier: "segue_VC_shiftSummary", sender: nil)
        }
    }
}
