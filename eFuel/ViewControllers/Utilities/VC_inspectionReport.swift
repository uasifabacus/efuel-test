import UIKit
import SwiftDate
import RealmSwift
import MessageUI

class VC_inspectionReport: BaseViewController {

    @IBOutlet weak var lbl_truckNumb: Label!
    @IBOutlet weak var lbl_dateTime: Label!
    
    private let padding = 2
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lbl_truckNumb.text = RealmHelper.shared.selectedTruckDetails?.truckNumber ?? "-"
        
        let date = DateInRegion(region: Region.local)
        self.lbl_dateTime.text = date.toFormat(k_MMDDYY)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    @IBAction func action_print(_ sender: Any) {
        if let inspectReportPrint = self.inspectReportPrint() {
            if self.email(str: inspectReportPrint, vc: self) == false {
            }
        }
    }
}


extension VC_inspectionReport : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
