import UIKit

class VC_pauseReason: BaseViewController {

    private static let array_reasons = ["Customer Meeting","Lunch Break","Refuel Truck","Sales Call","Take a Rest","Traffic Delay","Truck Problem"]
    
    
    
    //MARK:-cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Event List"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? VC_pauseNote_end, let str = sender as? String {
            vc.heading = str
        }
    }
}


//MARK:-UITableViewDataSource
extension VC_pauseReason : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VC_pauseReason.array_reasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_newCaseReason", for: indexPath) as! Cell_newCaseReason
        cell.setupUIForPause(str: VC_pauseReason.array_reasons[indexPath.row])
        return cell
    }
}



//MARK:-UITableViewDelegate
extension VC_pauseReason : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if VC_pauseReason.array_reasons[indexPath.row] == k_refuelTruck {
            self.performSegue(withIdentifier: "segue_VC_pauseRefuelTruck_end", sender: nil)
        }
        else {
            self.performSegue(withIdentifier: "segue_VC_pauseNote_end", sender: VC_pauseReason.array_reasons[indexPath.row])
        }
    }
}
