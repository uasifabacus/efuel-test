import UIKit
import RealmSwift
import MBProgressHUD

class VC_downloadAppData: BaseViewController {

    @IBOutlet weak var btn_byPass: UIButton!
    
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Download Data"
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addNotifier()
        self.setupButton()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    private func setupButton () {
        
/*      Old Logic ************************************
 
 
        if RealmHelper.isDownloadedDataEmpty == true {
            self.btn_byPass.isEnabled = false
            self.btn_byPass.alpha = 0.4
        }
 Old Logic *************************************/
 
        if RealmHelper.shared.createNewShift {
            self.btn_byPass.isSelected = false
        }
        else {
            self.btn_byPass.isSelected = true
        }
        
        if RealmHelper.shared.isDownloadByPassAllowed && (!RealmHelper.shared.differenOPLogin) {
            self.btn_byPass.isEnabled = true
        }
        else {
            self.btn_byPass.isEnabled = false
            self.btn_byPass.isSelected = false
        }
    }
    
    
    
    
    
    
    
    //MARK:- Actions
    @IBAction func action_byPassCheckMark(_ sender: UIButton) {
        print(sender.isSelected)
        sender.isSelected = !sender.isSelected
    }
    
    
    @IBAction func action_continue(_ sender: Any) {
        
        if self.btn_byPass.isSelected == false {
            
            if RealmHelper.shared.createNewShift == true {

                WebMethods.downloadData(level: .appNewlyInstalled) { [weak self] (isSucceed) in
                    
                    Helper.hideHud()
                    
                    guard let self_ = self, isSucceed else {
                        Helper.showBanner(text: "Error downloading data, please try again", isSuccess: false)
                        RealmHelper.removePreviousDownloadedData()
                        return
                    }
                    
                    print("is Succeed in VC_downloadAppData: \(isSucceed)")
                    
                    self_.verifyHardWareInDB()
                }
            }
            else {
                
                
                WebMethods.uploadData(isOnlyUpload: false) { [weak self] (isSucceed) in      // first upload, then download, downloading happening inside "uploadData" method
                    
                    Helper.hideHud()
                    
                    guard let self_ = self, isSucceed else {
                        Helper.showBanner(text: "Error in synchronization, please try again", isSuccess: false)
                        RealmHelper.removePreviousDownloadedData()
                        return
                    }
                    
                    self_.verifyHardWareInDB()
                }
            }
        }
        else {
            
            self.verifyHardWareInDB()
        }
    }
    
    
    private func verifyHardWareInDB() {
        
        var truckId_: Int?
        if RealmHelper.shared.inProgressFlag || RealmHelper.shared.startDataIncompleteFlag {
            
            guard let truckId = RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists else {
                Helper.showBanner(text: "Shift exists but Truck id doesn't found in Database", isSuccess: false)
                return
            }
            truckId_ = truckId
            guard ConfigManager.isDeviceVerified(truckId: truckId) else { return }
        }
        
        
        self.verifyHardwareIsConnected(truckId: truckId_)
    }
    
    
    private func verifyHardwareIsConnected(truckId : Int?) {

        if RealmHelper.shared.startDataIncompleteFlag {

            self.isPrinterConnected(truckId: truckId, isRecursive: true) { (isSucceed) in
                if isSucceed {
                    self.goToNextScreen(isShiftManager: true)
                }
            }
        }
        else {
            self.goToNextScreen()
        }
    }
    
    
    
    
    
    
    
    
    
    private func goToNextScreen(isShiftManager: Bool = false) {
        self.performSegue(withIdentifier: "segue_menu", sender: isShiftManager)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let isShiftManager = sender as? Bool, isShiftManager == true {
            if let vc_menu = segue.destination as? VC_menu {
                vc_menu.isShiftOpened = true
            }
        }
    }
}



//self.performSegue(withIdentifier: "segue_mainMenu", sender: nil)
