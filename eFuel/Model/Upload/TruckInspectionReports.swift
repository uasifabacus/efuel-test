import UIKit
import Realm
import RealmSwift

class TruckInspectionReports: RealmSwift.Object, Codable {

    @objc dynamic var truck_inspection_report_id: Int = 0
    @objc dynamic var inspection_time: String?
    var truck_id = RealmOptional<Int>()
    var login_user_id = RealmOptional<Int>()
    @objc dynamic var remarks: String?
    
    
    
    
    enum CodingKeys: String, CodingKey {
        case truck_inspection_report_id
        case inspection_time
        case truck_id
        case login_user_id
        case remarks
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "truck_inspection_report_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    //Incrementa ID
    var IncrementaID : Int {
        let realm = try! Realm()
        if let retNext = realm.objects(TruckInspectionReports.self).sorted(byKeyPath: "truck_inspection_report_id").last?.truck_inspection_report_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
