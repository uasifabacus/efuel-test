import Foundation
import Alamofire
import Realm
import RealmSwift

// MARK: - TanksPricingCreditMonitorStatusRoot
class TanksPricingCreditMonitorStatusRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [TanksPricingCreditMonitorStatus]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - Datum
class TanksPricingCreditMonitorStatus:RealmSwift.Object, Codable {
    
    @objc dynamic var tankID: Int = 0
    var tankNum = RealmOptional<Int>()
    var installationStreetAddressID = RealmOptional<Int>()
    var productType = RealmOptional<Int>()
    var tankSizeGals = RealmOptional<Int>()
    var maxFillGals = RealmOptional<Int>()
    var stopNumber = RealmOptional<Int>()
    var surcharge2 = RealmOptional<Int>()
    var surcharge3 = RealmOptional<Int>()
    var unitOfMeasureID = RealmOptional<Int>()
    var minDelDollars = RealmOptional<Int>()
    var termsCode = RealmOptional<Int>()
    var creditStatusCode = RealmOptional<Int>()
    var balance = RealmOptional<Int>()
    var dmInstallationID = RealmOptional<Int>()
    var gwmInstallationID = RealmOptional<Int>()
    var floatLevel = RealmOptional<Int>()
    var unitsDelivered = RealmOptional<Int>()
    
    var salesTax = RealmOptional<Double>()
    var regulatoryFee = RealmOptional<Double>()
    var surcharge1 = RealmOptional<Double>()
    var pricePerUnit = RealmOptional<Double>()
    var termsCreditLimit = RealmOptional<Double>()
    var ageing1 = RealmOptional<Double>()
    var ageing2 = RealmOptional<Double>()
    var ageing3 = RealmOptional<Double>()
    var ageing4 = RealmOptional<Double>()
    var ageingCurrent = RealmOptional<Double>()
    var lastPaymentAmt = RealmOptional<Double>()
    var deliveryInfo = List<DeliveryInfo>()
    var owners = LinkingObjects(fromType: InstalltionStreetAddresses.self, property: "tanksPricingCreditMonitorStatus")
    
    
    @objc dynamic var sensorStatus, unusualUsageAlert, lowLevelAlert, insideTempAlert, fillTriggerLast24_Hours, commLinkAlert, lastDeliveryTime, readingTime, gmLastCallinLocalTimeAtGMLocation, lastPaymentDate, balanceDate, unitsOfMeasureDescr, creditStatusDescription, termsDescriptions, routedFlag, willCallFlag, serialNumOnTank, activeFlag: String?
    
    enum CodingKeys: String, CodingKey {
        case tankID = "tank_id"
        case tankNum = "tank_num"
        case installationStreetAddressID = "installation_street_address_id"
        case serialNumOnTank = "serial_num_on_tank"
        case activeFlag = "active_flag"
        case productType = "product_type"
        case tankSizeGals = "tank_size_gals"
        case routedFlag = "routed_flag"
        case willCallFlag = "will_call_flag"
        case maxFillGals = "max_fill_gals"
        case stopNumber = "stop_number"
        case salesTax = "sales_tax"
        case regulatoryFee = "regulatory_fee"
        case surcharge1 = "surcharge_1"
        case surcharge2 = "surcharge_2"
        case surcharge3 = "surcharge_3"
        case pricePerUnit = "price_per_unit"
        case unitOfMeasureID = "unit_of_measure_id"
        case minDelDollars = "min_del_dollars"
        case termsCode = "terms_code"
        case termsDescriptions = "terms_descriptions"
        case creditStatusCode = "credit_status_code"
        case creditStatusDescription = "credit_status_description"
        case unitsOfMeasureDescr = "units_of_measure_descr"
        case termsCreditLimit = "terms_credit_limit"
        case ageing1 = "ageing_1"
        case ageing2 = "ageing_2"
        case ageing3 = "ageing_3"
        case ageing4 = "ageing_4"
        case ageingCurrent = "ageing_current"
        case balance
        case balanceDate = "balance_date"
        case lastPaymentAmt = "last_payment_amt"
        case lastPaymentDate = "last_payment_date"
        case dmInstallationID = "DM_installation_id"
        case gwmInstallationID = "GWM_installation_id"
        case readingTime = "reading_time"
        case gmLastCallinLocalTimeAtGMLocation = "GM_last_callin_local_time_at_GM_location"
        case floatLevel = "float_level"
        case lastDeliveryTime = "Last_delivery_time"
        case unitsDelivered = "units_delivered"
        case fillTriggerLast24_Hours = "fill_trigger_last_24_hours"
        case sensorStatus = "sensor_status"
        case unusualUsageAlert = "unusual_usage_alert"
        case lowLevelAlert = "low_level_alert"
        case insideTempAlert = "inside_temp_alert"
        case commLinkAlert = "comm_link_alert"
    }
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "tankID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
