import Foundation

extension NSError  {
    
    private static func createWithLocalizedDesription(withCode code:Int = 204,localizedDescription:String) -> NSError{
        return  NSError(domain: "com.swiftallmighty.eFuel", code:code, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    static var NoInternet : NSError {
        return createWithLocalizedDesription(withCode: -1009,localizedDescription:"Please check your internet connection")
    }
}



enum MyError: Error {
    case runtimeError
    case unableToFetchProductType
}
