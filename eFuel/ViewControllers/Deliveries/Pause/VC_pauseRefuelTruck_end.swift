import UIKit

class VC_pauseRefuelTruck_end: BaseViewController {    
    
    
    @IBOutlet weak var tf_gallons: TextField!
    @IBOutlet weak var tf_cost: TextField!
    
    
    
    //MARK:-cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Refuel Truck"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:-Actions
    @IBAction func action_done(_ sender: Any) {
        
        guard let gl = Helper.validateAndTrimText(TF: self.tf_gallons), let gallons = gl.double() else {
            Helper.showBanner(text: "Enter valid Gallons value", isSuccess: false)
            return
        }
        
        guard let cst = Helper.validateAndTrimText(TF: self.tf_cost), let cost = cst.double() else {
            Helper.showBanner(text: "Enter valid Cost", isSuccess: false)
            return
        }
        
        
        #warning("Abdul Ahad sy pochna hai k event name kia aega yahan? aur efuelMobileDeliveryShiftId kia hogi?")
        RealmHelper.createMobileEventLog(eventName: .pump,
                                         gallons: gallons,
                                         cost: cost,
                                         efuelMobileDeliveryShiftId: RealmHelper.shared.mobileDeliveryShiftId)
        
        
        self.navigationController?.popViewController(animated: true)
    }
}
