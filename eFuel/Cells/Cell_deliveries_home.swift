import UIKit

class Cell_deliveries_home: UITableViewCell {

    @IBOutlet weak var lbl_stopNumber: UILabel!
    @IBOutlet weak var lbl_customerName: UILabel!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setupUI(obj: Remaining_Skip_Completed) {
        self.lbl_customerName.text = obj.user_account_name
        self.lbl_stopNumber.text = obj.stopNumber?.string
    }
    
    
    func setupUI(obj: Trucks) {
        self.lbl_customerName.text = obj.capacityGallons.value?.string
        self.lbl_stopNumber.text = obj.truckNumber ?? "-"
    }
}
