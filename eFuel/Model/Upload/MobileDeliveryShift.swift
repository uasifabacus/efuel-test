import UIKit
import Realm
import RealmSwift


class MobileDeliveryShift: RealmSwift.Object, Codable {

    @objc dynamic var mobile_delivery_shift_id: Int = 0
    var efuel_mobile_delivery_shift_id = RealmOptional<Int>()
    var truck_id = RealmOptional<Int>()
    var login_user_id = RealmOptional<Int>()
    @objc dynamic var login_user_descr: String?
    @objc dynamic var time_out: String?
    @objc dynamic var time_in: String?
    var odometer_out = RealmOptional<Double>()
    var odometer_in = RealmOptional<Double>()
    var totalizer_out = RealmOptional<Double>()
    var totalizer_in = RealmOptional<Double>()
    var inventory_out = RealmOptional<Double>()
    var inventory_in = RealmOptional<Double>()
    var last_manual_sale_number = RealmOptional<Int>()
    @objc dynamic var last_update: String?
    @objc dynamic var app_error_log: String?
    
    
    
    
    enum CodingKeys: String, CodingKey {
        case mobile_delivery_shift_id
        case efuel_mobile_delivery_shift_id
        case truck_id
        case login_user_id
        case login_user_descr
        case time_out
        case time_in
        case odometer_out
        case odometer_in
        case totalizer_out
        case totalizer_in
        case inventory_out
        case inventory_in
        case last_manual_sale_number
        case last_update
        case app_error_log
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "mobile_delivery_shift_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    //Incrementa ID
    var IncrementaID : Int {
        let realm = try! Realm()
        if let retNext = realm.objects(MobileDeliveryShift.self).sorted(byKeyPath: "mobile_delivery_shift_id").last?.mobile_delivery_shift_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
