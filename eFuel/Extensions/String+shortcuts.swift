import Foundation
import CryptoSwift
import Alamofire



extension String {
    
    var formattedPrice: String {return self + " USD" }
    
    func aesEncrypt(key: String, iv: String) throws -> String {
        
        let data = self.data(using: .utf8)!
        let encrypted = try! AES(key: key.bytes, blockMode: CBC(iv: iv.bytes), padding: Padding.pkcs7).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }
    
    func aesDecrypt(key: String, iv: String) throws -> String {
        
        let data = Data(base64Encoded: self)!
        let decrypted = try! AES(key: key.bytes, blockMode: CBC(iv: iv.bytes), padding: Padding.pkcs7).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData, encoding: .utf8) ?? "Could not decrypt"
    }
    
    
    func fromLocaleCurrency() throws -> NSNumber {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        
        
        guard let obj = formatter.number(from: self) else {
            throw MyError.runtimeError
        }
        return obj
    }
    
    

/*
 ********************************************** convertToDictionary Usage **********************************************
     
     do {
     
     let xyz = try charitylist?.jsonString(encoding: .utf8)
     
     print(xyz?.convertToDictionary())
     }
     catch {
     print(error)
     }
 
********************************************** convertToDictionary Usage **********************************************
 */
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    
    func isValidAmount() -> Bool {
        
        
        if self.count(of: ".") > 1 {
            return false
        }
        
        if self.count <= 0 {
            return false
        }
        
        
        return true
    }
}











