import Foundation
import Alamofire
import Realm
import RealmSwift

// MARK: - DeliverySkipCategoriesRoot
class DeliverySkipCategoriesRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [DeliverySkipCategories]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [DeliverySkipCategories]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - Datum
class DeliverySkipCategories: RealmSwift.Object, Codable {
    
    @objc dynamic var deliverySkipCategoryID: Int = 0
    @objc dynamic var skipCategoryDescriptions: String?
    @objc dynamic var collapsed: Bool = true
    
    var reasons = List<DeliverySkipReasons>()
    
    
    
    enum CodingKeys: String, CodingKey {
        case deliverySkipCategoryID = "delivery_skip_category_id"
        case skipCategoryDescriptions = "skip_category_descriptions"
    }
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "deliverySkipCategoryID"
    }
    
    
//    override static func ignoredProperties() -> [String] {
//        return ["collapsed", "umair"]
//    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
