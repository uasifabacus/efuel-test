import Foundation
import Alamofire
import Realm
import RealmSwift

// MARK: - AuthenticateUser
class AuthenticateUser: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let data: [LoginUser]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case data
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [LoginUser]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.data = data
    }
}


// MARK: - Datum
class LoginUser: RealmSwift.Object, Codable {
    @objc dynamic var loginUserID: Int = 0
    @objc dynamic var clientID: Int = 0
    @objc dynamic var clientName, loginUserName, loginUserPassword, loginUserDescr: String?
    @objc dynamic var defaultOperatingLocationID: Int = 0
    @objc dynamic var disableEfuelMobile, enableDialup: String?
    @objc dynamic var dataSyncInterval: Int = 0
    @objc dynamic var gpsCollectionInterval: Int = 0
    @objc dynamic var gpsType: Int = 0
    @objc dynamic var gpsCOMPort: Int = 0
    @objc dynamic var allowDownloadBypass, enableFullShiftSync, operatingLocationName: String?
    @objc dynamic var numDaysToForecast: Int = 0
    @objc dynamic var primaryPhoneNum, streetAddressLine1, city, state: String?
    @objc dynamic var zip: String?
    @objc dynamic var geoCodeLat : Double = 0
    @objc dynamic var geoCodeLong: Double = 0
    @objc dynamic var maxFillPercentage: Int = 0
    @objc dynamic var operatingLocationPhoneNum, efuelMobileSqlceLogin: String?
    @objc dynamic var hoursToSubtractFromGMTToComputeLocalTime: Int = 0
    @objc dynamic var efuelMobileSuppressClientName, efuelMobileSuppressOplocInfo, efuelMobileSignatureLine, enablePriceChanges: String?
    
    enum CodingKeys: String, CodingKey {
        case loginUserID = "login_user_id"
        case clientID = "client_id"
        case clientName = "client_name"
        case loginUserName = "login_user_name"
        case loginUserPassword = "login_user_password"
        case loginUserDescr = "login_user_descr"
        case defaultOperatingLocationID = "default_operating_location_id"
        case disableEfuelMobile = "disable_efuel_mobile"
        case enableDialup = "enable_dialup"
        case dataSyncInterval = "data_sync_interval"
        case gpsCollectionInterval = "gps_collection_interval"
        case gpsType = "gps_type"
        case gpsCOMPort = "gps_com_port"
        case allowDownloadBypass = "allow_download_bypass"
        case enableFullShiftSync = "enable_full_shift_sync"
        case operatingLocationName = "operating_location_name"
        case numDaysToForecast = "num_days_to_forecast"
        case primaryPhoneNum = "primary_phone_num"
        case streetAddressLine1 = "Street_address_line_1"
        case city = "City"
        case state = "State"
        case zip = "Zip"
        case geoCodeLat = "GeoCodeLat"
        case geoCodeLong = "GeoCodeLong"
        case maxFillPercentage = "max_fill_percentage"
        case operatingLocationPhoneNum = "operating_location_phone_num"
        case efuelMobileSqlceLogin = "efuel_mobile_sqlce_login"
        case hoursToSubtractFromGMTToComputeLocalTime = "hours_to_subtract_from_GMT_to_compute_local_time"
        case efuelMobileSuppressClientName = "efuel_mobile_suppress_client_name"
        case efuelMobileSuppressOplocInfo = "efuel_mobile_suppress_oploc_info"
        case efuelMobileSignatureLine = "efuel_mobile_signature_line"
        case enablePriceChanges = "enable_price_changes"
    }
    
    
    /*
    init(loginUserID: Int?, clientID: Int?, clientName: String?, loginUserName: String?, loginUserPassword: String?, loginUserDescr: String?, defaultOperatingLocationID: Int?, disableEfuelMobile: String?, enableDialup: String?, dataSyncInterval: Int?, gpsCollectionInterval: Int?, gpsType: Int?, gpsCOMPort: Int?, allowDownloadBypass: String?, enableFullShiftSync: String?, operatingLocationName: String?, numDaysToForecast: Int?, primaryPhoneNum: String?, streetAddressLine1: String?, city: String?, state: String?, zip: String?, geoCodeLat: Double?, geoCodeLong: Double?, maxFillPercentage: Int?, operatingLocationPhoneNum: String?, efuelMobileSqlceLogin: String?, hoursToSubtractFromGMTToComputeLocalTime: Int?, efuelMobileSuppressClientName: String?, efuelMobileSuppressOplocInfo: String?, efuelMobileSignatureLine: String?, enablePriceChanges: String?) {
        self.loginUserID = loginUserID
        self.clientID = clientID
        self.clientName = clientName
        self.loginUserName = loginUserName
        self.loginUserPassword = loginUserPassword
        self.loginUserDescr = loginUserDescr
        self.defaultOperatingLocationID = defaultOperatingLocationID
        self.disableEfuelMobile = disableEfuelMobile
        self.enableDialup = enableDialup
        self.dataSyncInterval = dataSyncInterval
        self.gpsCollectionInterval = gpsCollectionInterval
        self.gpsType = gpsType
        self.gpsCOMPort = gpsCOMPort
        self.allowDownloadBypass = allowDownloadBypass
        self.enableFullShiftSync = enableFullShiftSync
        self.operatingLocationName = operatingLocationName
        self.numDaysToForecast = numDaysToForecast
        self.primaryPhoneNum = primaryPhoneNum
        self.streetAddressLine1 = streetAddressLine1
        self.city = city
        self.state = state
        self.zip = zip
        self.geoCodeLat = geoCodeLat
        self.geoCodeLong = geoCodeLong
        self.maxFillPercentage = maxFillPercentage
        self.operatingLocationPhoneNum = operatingLocationPhoneNum
        self.efuelMobileSqlceLogin = efuelMobileSqlceLogin
        self.hoursToSubtractFromGMTToComputeLocalTime = hoursToSubtractFromGMTToComputeLocalTime
        self.efuelMobileSuppressClientName = efuelMobileSuppressClientName
        self.efuelMobileSuppressOplocInfo = efuelMobileSuppressOplocInfo
        self.efuelMobileSignatureLine = efuelMobileSignatureLine
        self.enablePriceChanges = enablePriceChanges
    }
    */
    
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "loginUserID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}

