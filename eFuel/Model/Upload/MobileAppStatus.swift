import UIKit
import Realm
import RealmSwift

class MobileAppStatus: RealmSwift.Object, Codable {
    
    @objc dynamic var mobile_app_status_id: Int = 0
    @objc dynamic var last_data_download : Date? = nil
    var mobile_app_state = RealmOptional<Int>()
    var mobile_app_data_status = RealmOptional<Int>()
    @objc dynamic var last_delivery_queue_download : Date? = nil
    @objc dynamic var last_call_log_download : Date? = nil
    @objc dynamic var last_call_detail_download : Date? = nil
    var last_inventory_startValue = RealmOptional<Double>()
    var last_inventory_startQty = RealmOptional<Double>()
    

    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "mobile_app_status_id"
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    
    //Incrementa ID
    var IncrementaID : Int {
        let realm = try! Realm()
        if let retNext = realm.objects(MobileAppStatus.self).sorted(byKeyPath: "mobile_app_status_id").last?.mobile_app_status_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
