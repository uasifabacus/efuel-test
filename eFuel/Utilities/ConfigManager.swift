import Foundation
import UIKit
import RealmSwift

class ConfigManager: NSObject {
    
    static let supportERDevices = ["Manual", "simulator", "LCR"]
    static let supportPRDevices = ["Register", "File", "Epson", "Epson-TMU295", "Epson-TMU220D", "", "", "", "", "", "Zebra", "zebra_blueTooth", "blueTooth"]
    
   
    
    
    
    
    
    static func isDeviceVerified(truckId: Int) -> Bool {
        
        let (deviceName, printerName, truckNumber) = ConfigManager.retrieveDeviceAndPrinter(truckId: truckId)
        
        guard verifyDevice(deviceName: deviceName, truckNumber: truckNumber) else { return false }
        guard verifyPrinter(printerName: printerName, truckNumber: truckNumber) else { return false }
        return true
    }
    
    
    
    
    static func verifyDevice(deviceName: String, truckNumber: String) -> Bool {
        
        if let deviceType = DeviceType(rawValue: deviceName) {
            
            switch deviceType {
                
            case .simulator:
                return true
                
            case .LCR:
                return true
                
            case .Manual:
                return false
            }
        }
        else {
            checkLengthAndShowError(truckNumber: truckNumber, type: "Device Type", str: deviceName)
            return false
        }
    }
    
    
    
    
    
    private static func verifyPrinter(printerName: String, truckNumber: String) -> Bool {
        
        guard verifyDeviceInList(array: supportPRDevices, deviceName: printerName) else {
            checkLengthAndShowError(truckNumber: truckNumber, type: "Printer Type", str: printerName)
            return false
        }
        
        return true
    }
    
    
    private static func checkLengthAndShowError(truckNumber: String, type: String, str: String) {
        
        guard str.count > 0 else {
            Helper.showBanner(text: "No \(type) specified for Truck# \(truckNumber)", isSuccess: false)
            return
        }
        
        Helper.showBanner(text: "\(type) specified for Truck# \(truckNumber) is different", isSuccess: false)
    }
    
    
    
    
    static func retrieveDeviceAndPrinter (truckId: Int) -> (String, String, String) {
        
        let realm = try! Realm()
        
        guard realm.objects(Trucks.self).count > 0 else { return ("", "", "") }
        
        
        if let filteredObj = RealmHelper.shared.realm.objects(Trucks.self).filter({$0.truckID == truckId}).first {
            
            try! realm.write {
                filteredObj.efuelMobileDeviceType?.trim()
                filteredObj.efuelMobilePrinterType?.trim()
                filteredObj.truckNumber?.trim()
            }
            
            return (filteredObj.efuelMobileDeviceType ?? "", filteredObj.efuelMobilePrinterType ?? "", filteredObj.truckNumber ?? "")
        }
        else {
            return ("", "", "")
        }
    }
    
    
    private static func verifyDeviceInList (array: [String], deviceName: String) -> Bool {
        return (array.filter({$0 == deviceName}).count > 0)
    }
}











/*
    
    static func validateDeviceConfig(truckId: Int) -> tfwCrashType {
        var retCode: Int = 0
        
        let (deviceName, printerName) = DriverWorkInfo.retrieveDeviceAndPrinter(truckId: truckId)
        
        
        
        var fromTableDeviceType = -1
        var fromTablePrinterType = -1
        
        parseConfigToken(strDeviceName: deviceName, arrayDevices: supportERDevices, intDeviceType: &fromTableDeviceType)
        parseConfigToken(strDeviceName: printerName, arrayDevices: supportPRDevices, intDeviceType: &fromTablePrinterType)
        
        
        if fromTableDeviceType == -1 {
            return .eFuel_mobile_not_specified_deviceType
        }
        else if fromTableDeviceType == -2 {
            return .eFuel_mobile_trucks_bad_deviceType
        }
        else {
            if validatePrinterConfig(deviceType: fromTableDeviceType, retCode: &retCode) == false {
                return .AppSTOP
            }
        }
        
        return .AppSTOP
    }
 
    
    
    
    
    static func validatePrinterConfig(deviceType:Int, retCode: inout Int) -> Bool {
        
        if let enum_ = erType(rawValue: deviceType) {
            
            switch enum_ {
            case erType.er_Manual:
                break
            case erType.er_simulator, .er_LCR:
                break
            }
        }
        
        #warning("neechy return true sirf error remove karney k liye likha hai bas, jab k yahan ye nahi balkey actual logic aegi")
        return true
    }
    
    
    
    
    // -1 = not specified device type
    // -2 = wrong specified device type
    //  > 0 valid device
    
    
    static func parseConfigToken(strDeviceName: String, arrayDevices: [String], intDeviceType:inout Int) {
        
        var myDevice : Int = 0
        
        if strDeviceName == "" {
            intDeviceType = -1
        }
        else {
            
            if let obj = arrayDevices.firstIndex(of: strDeviceName) {
                myDevice = obj
            }
            else {
                myDevice = -2
            }
            
            
            if myDevice < 0 {
                intDeviceType = -2
            }
            else {
                intDeviceType = myDevice
            }
        }
    }
*/




//MARK:- is printer connected
extension ConfigManager {
    
    static func isPrinterConnected() -> Bool {
        return false
    }
}
