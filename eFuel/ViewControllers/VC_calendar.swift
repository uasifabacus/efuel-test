import UIKit
import FSCalendar

class VC_calendar: BaseViewController {

    weak var delegate_ : PassDataPopoverToParent?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}



extension VC_calendar : FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.dismiss(animated: true) {
            self.delegate_?.passData(obj: date, vc: self)
        }
    }
}
