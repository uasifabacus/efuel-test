import Foundation
import Alamofire



private let k_baseUrl = "http://192.168.70.198:82/"       // virtual machine

//private let k_baseUrl = "http://192.168.70.28:82/"          // abdul ahad machine

//private let k_baseUrl = "http://172.18.100.65:82/" // KPBS test staging







private let k_subBaseUrl = "eFuelRestService/"



private let k_generatetoken = "generatetoken"
private let k_AuthenticateUser = "LoginUser/AuthenticateUser?"


private let k_GetCallLog = "CallLog/GetCallLog/"
private let k_GetCallDetail = "CallDetail/GetCallDetail/"
private let k_GetDeliveryQueue = "DeliveryQueue/GetDeliveryQueue?"
private let k_GetLookups = "Lookup/GetLookups"
private let k_GetTrucks = "Truck/GetTrucks/"
private let k_GetUserAccounts = "UserAccounts/GetUserAccounts/"
private let k_GetInstallationStreetAddresses = "InstallationStreetAddress/GetInstallationStreetAddresses/"
private let k_GetTanksPricingCreditMonitorStatus = "TanksPricingCreditMonitorStatus/GetTanksPricingCreditMonitorStatus/"
private let k_GetDeliveryInfo = "DeliveryInfo/GetDeliveryInfo/"
private let k_GetDeliverySkipCategories = "DeliverySkipCategory/GetDeliverySkipCategories/"
private let k_GetDeliverySkipReason = "DeliverySkipReason/GetDeliverySkipReason"
private let k_GetTruckInspectionReportItems = "TruckInspectionReportItem/GetTruckInspectionReportItems/"
private let k_GetMobileDeliveryTickets = "MobileDeliveryTicket/GetMobileDeliveryTickets?"
private let k_GetMobileDeliverySurchargeLabels = "MobileDeliverySurcharge/GetMobileDeliverySurchargeLabels/"
private let k_GetDeliveryShiftStatus = "DeliveryShiftStatus/GetDeliveryShiftStatus?"
//http://192.168.70.198:82/eFuelRestService/DeliveryShiftStatus/GetDeliveryShiftStatus?login_user_id=698&truck_id=615





private let k_InsertMobileDeliveryShift = "MobileDeliveryShift/InsertMobileDeliveryShift/"
private let k_updateMobileDeliveryShift = "MobileDeliveryShift/UpdateMobileDeliveryShift/"
private let k_InsertTruckInspectionReport = "TruckInspectionReport/InsertTruckInspectionReport/"
private let k_InsertTruckInspectionReportDefects = "TruckInspectionReportDefect/InsertTruckInspectionReportDefects/"
private let k_InsertMobileGPS = "MobileGPS/InsertMobileGPS/"
private let k_InsertMobileSyncLog = "MobileSyncLog/InsertMobileSyncLog/"
private let k_InsertMobileEventLog = "MobileEventLog/InsertMobileEventLog/"
private let k_InsertMobileCallLog = "MobileCallLog/InsertMobileCallLog/"
private let k_InsertMobileCallDetail = "MobileCallDetail/InsertMobileCallDetail/"
private let k_InsertMobileDeliveryNewTank = "MobileDeliveryNewTank/InsertMobileDeliveryNewTank/"
private let k_InsertMobileDeliveryQueue = "MobileDelivery/InsertMobileDeliveryQueue/"







enum Router : URLRequestConvertible {

    case generateToken(str: String)
    case AuthenticateUser(userName: String, pass: String)
    case GetCallLog(opertaionLocId: String)
    case GetCallDetail(opertaionLocId: String)
    case GetDeliveryQueue(op_loc_id: String, del_date: String, truck_id: String)
    case GetLookups
    case GetTrucks(operating_location_id: String)
    case GetUserAccounts(operating_location_id: String)
    case GetInstallationStreetAddresses(operating_location_id: String)
    case GetTanksPricingCreditMonitorStatus(operating_location_id: String)
    case GetDeliveryInfo(operating_location_id: String)
    case GetDeliverySkipCategories
    case GetDeliverySkipReason
    case GetTruckInspectionReportItems(operating_location_id: String)
    case GetMobileDeliveryTickets(client_id: Int, operating_location_id: String)
    case GetMobileDeliverySurchargeLabels(operating_location_id: String)
    case GetDeliveryShiftStatus(truck_id: Int)
    
    case InsertMobileDeliveryShift(data: Data)
    case InsertTruckInspectionReport(data: Data)
    case InsertTruckInspectionReportDefects(data: Data)
    case InsertMobileGPS(data: Data)
    case InsertMobileSyncLog(data: Data)
    case InsertMobileEventLog(data: Data)
    case InsertMobileCallLog(data: Data)
    case InsertMobileCallDetail(data: Data)
    case InsertMobileDeliveryNewTank(data: Data)
    case InsertMobileDeliveryQueue(data: Data)

    case UpdateMobileDeliveryShift(data: Data)


    
    
    
    private var method: HTTPMethod
    {
        switch self
        {
        case .AuthenticateUser,
             .GetCallLog,
             .GetCallDetail,
             .GetDeliveryQueue,
             .GetLookups,
             .GetTrucks,
             .GetUserAccounts,
             .GetInstallationStreetAddresses,
             .GetTanksPricingCreditMonitorStatus,
             .GetDeliveryInfo,
             .GetDeliverySkipCategories,
             .GetDeliverySkipReason,
             .GetTruckInspectionReportItems,
             .GetMobileDeliveryTickets,
             .GetMobileDeliverySurchargeLabels,
             .GetDeliveryShiftStatus:
            return .get
            
        case .generateToken,
             .InsertMobileDeliveryShift,
             .InsertTruckInspectionReport,
             .InsertTruckInspectionReportDefects,
             .InsertMobileGPS,
             .InsertMobileSyncLog,
             .InsertMobileEventLog,
             .InsertMobileCallLog,
             .InsertMobileCallDetail,
             .InsertMobileDeliveryNewTank,
             .InsertMobileDeliveryQueue:
            return .post
            
        case .UpdateMobileDeliveryShift:
            return .put
        }
    }
    
    
    
    private var serverMethod: String {
        
        switch self
        {
        case .generateToken:
            return k_generatetoken
        case .AuthenticateUser:
            return k_AuthenticateUser
        case .GetCallLog:
            return k_GetCallLog
        case .GetCallDetail:
            return k_GetCallDetail
        case .GetDeliveryQueue:
            return k_GetDeliveryQueue
        case .GetLookups:
            return k_GetLookups
        case .GetTrucks:
            return k_GetTrucks
        case .GetUserAccounts:
            return k_GetUserAccounts
        case .GetInstallationStreetAddresses:
            return k_GetInstallationStreetAddresses
        case .GetTanksPricingCreditMonitorStatus:
            return k_GetTanksPricingCreditMonitorStatus
        case .GetDeliveryInfo:
            return k_GetDeliveryInfo
        case .GetDeliverySkipCategories:
            return k_GetDeliverySkipCategories
        case .GetDeliverySkipReason:
            return k_GetDeliverySkipReason
        case .GetTruckInspectionReportItems:
            return k_GetTruckInspectionReportItems
        case .GetMobileDeliveryTickets:
            return k_GetMobileDeliveryTickets
        case .GetMobileDeliverySurchargeLabels:
            return k_GetMobileDeliverySurchargeLabels
        case .GetDeliveryShiftStatus:
            return k_GetDeliveryShiftStatus
        case .InsertMobileDeliveryShift:
            return k_InsertMobileDeliveryShift
        case .InsertTruckInspectionReport:
            return k_InsertTruckInspectionReport
        case .InsertTruckInspectionReportDefects:
            return k_InsertTruckInspectionReportDefects
        case .InsertMobileGPS:
            return k_InsertMobileGPS
        case .InsertMobileSyncLog:
            return k_InsertMobileSyncLog
        case .InsertMobileEventLog:
            return k_InsertMobileEventLog
        case .InsertMobileCallLog:
            return k_InsertMobileCallLog
        case .InsertMobileCallDetail:
            return k_InsertMobileCallDetail
        case .InsertMobileDeliveryNewTank:
            return k_InsertMobileDeliveryNewTank
        case .InsertMobileDeliveryQueue:
            return k_InsertMobileDeliveryQueue
        case .UpdateMobileDeliveryShift:
            return k_updateMobileDeliveryShift
        }
    }
    
    
    
    private var subBaseUrl: String {
        switch self
        {
        case .generateToken:
            return ""
            
        case .AuthenticateUser,
             .GetCallLog,
             .GetCallDetail,
             .GetDeliveryQueue,
             .GetLookups,
             .GetTrucks,
             .GetUserAccounts,
             .GetInstallationStreetAddresses,
             .GetTanksPricingCreditMonitorStatus,
             .GetDeliveryInfo,
             .GetDeliverySkipCategories,
             .GetDeliverySkipReason,
             .GetTruckInspectionReportItems,
             .GetMobileDeliveryTickets,
             .GetMobileDeliverySurchargeLabels,
             .InsertMobileDeliveryShift,
             .InsertTruckInspectionReport,
             .InsertTruckInspectionReportDefects,
             .InsertMobileGPS,
             .InsertMobileSyncLog,
             .InsertMobileEventLog,
             .InsertMobileCallLog,
             .InsertMobileCallDetail,
             .InsertMobileDeliveryNewTank,
             .InsertMobileDeliveryQueue,
             .GetDeliveryShiftStatus,
             .UpdateMobileDeliveryShift:
            return k_subBaseUrl
        }
    }
    
    
    
    private var params: String {
        switch self
        {
            
        case .GetLookups,
             .GetDeliverySkipCategories,
             .GetDeliverySkipReason,
             .InsertMobileDeliveryShift,
             .InsertTruckInspectionReport,
             .InsertTruckInspectionReportDefects,
             .InsertMobileGPS,
             .InsertMobileSyncLog,
             .InsertMobileEventLog,
             .InsertMobileCallLog,
             .InsertMobileCallDetail,
             .InsertMobileDeliveryNewTank,
             .InsertMobileDeliveryQueue,
             .UpdateMobileDeliveryShift:
            return ""
            
        case .generateToken:
            return "" //k_grant_type+"="+k_password+"&"+k_username+"="+k_userNameGrant+"&"+k_password+"="+k_passwordGrant
            
        case .AuthenticateUser(let userName, let pass):
            return "username=\(userName)&password=\(pass)"

        case .GetCallLog(let opertaionLocId):
            return opertaionLocId
            
        case .GetCallDetail(let opertaionLocId):
            return opertaionLocId
                
        case let .GetDeliveryQueue(op_loc_id,  del_date, truck_id):
//            http://192.168.70.198:82/eFuelRestService/DeliveryQueue/GetDeliveryQueue?op_loc_id=99&del_date=08-27-2019&truck_id=0
            return "op_loc_id=\(op_loc_id)&del_date=\(del_date)&truck_id=\(truck_id)"
            
        case .GetTrucks(let operating_location_id):
            return operating_location_id
                
        case .GetUserAccounts(let operating_location_id):
            return operating_location_id
            
        case .GetInstallationStreetAddresses(let operating_location_id):
            return operating_location_id
                
        case .GetTanksPricingCreditMonitorStatus(let operating_location_id):
            return operating_location_id
            
        case .GetDeliveryInfo(let operating_location_id):
            return operating_location_id
            
        case . GetTruckInspectionReportItems(let operating_location_id):
            return operating_location_id
            
        case let .GetMobileDeliveryTickets(client_id, operating_location_id):
            return "client_id=\(client_id)&operating_location_id=\(operating_location_id)"
            
        case .GetMobileDeliverySurchargeLabels(let operating_location_id):
             return operating_location_id
            
        case .GetDeliveryShiftStatus(let truck_id):
            return "login_user_id=\(Singleton.shared.userDetail.loginUserID)&truck_id=\(truck_id)"
        }
    }
    
    
    
    private var concatinateURL : String{
        
        switch self {
            
        case .generateToken,
             .AuthenticateUser,
             .GetCallLog,
             .GetCallDetail,
             .GetDeliveryQueue,
             .GetLookups,
             .GetTrucks,
             .GetUserAccounts,
             .GetInstallationStreetAddresses,
             .GetTanksPricingCreditMonitorStatus,
             .GetDeliveryInfo,
             .GetDeliverySkipCategories,
             .GetDeliverySkipReason,
             .GetTruckInspectionReportItems,
             .GetMobileDeliveryTickets,
             .GetMobileDeliverySurchargeLabels,
             .InsertMobileDeliveryShift,
             .InsertTruckInspectionReport,
             .InsertTruckInspectionReportDefects,
             .InsertMobileGPS,
             .InsertMobileSyncLog,
             .InsertMobileEventLog,
             .InsertMobileCallLog,
             .InsertMobileCallDetail,
             .InsertMobileDeliveryNewTank,
             .InsertMobileDeliveryQueue,
             .GetDeliveryShiftStatus,
             .UpdateMobileDeliveryShift:
            
            return "\(subBaseUrl)\(serverMethod)\(params)"
        }
    }
    
    

    private func isBody( request: inout URLRequest) {
        
        switch self {
            
        case .generateToken(let str):
            let data = str.data(using: .utf8)
            return request.httpBody = data
            
            
        case .InsertTruckInspectionReport(let data):
            return request.httpBody = data
            
        case .InsertMobileDeliveryShift(let data):
            return request.httpBody = data
//            return request = try! JSONEncoding.default.encode(request, withJSONObject: data)
            
        case .InsertMobileGPS(let data):
            return request.httpBody = data
            
        case .InsertMobileEventLog(let data):
            return request.httpBody = data
            
        case .InsertMobileSyncLog(let data):
            return request.httpBody = data
            
        case .UpdateMobileDeliveryShift(let data):
            return request.httpBody = data
            
        case .InsertTruckInspectionReportDefects(let data):
            return request.httpBody = data
            
        case .InsertMobileDeliveryQueue(let data):
            return request.httpBody = data
            
        default:
            break
        }
        
        
        /*
         var dict:[String: Any] = [:]
         
         switch self {
         
         case .generateToken:
         
         let dict = [k_username: k_userNameGrant, k_password: k_passwordGrant, k_grant_type: k_password]
         
         //            return request = try! JSONEncoding.default.encode(request, with: dict)
         
         case .GetCallDetail:
         break
         }
         
         
         request = try! JSONEncoding.default.encode(request, with: dict)
         */
    }
    
    
    
    
    
    
    func asURLRequest() throws -> URLRequest {
        
        
        let baseUrl = NSURL(string : k_baseUrl)!
        
        //        let encodedURL = concatinateURL.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        
        
        let url = NSURL(string: concatinateURL, relativeTo:baseUrl as URL)!
        var request = URLRequest(url: url as URL)
        request.httpMethod = method.rawValue
        
        
        Helper.cancelSamePrevRequest(currentURL: self.serverMethod)
        
        
        self.isBody(request: &request)
        
        
        
        //************************************************************************************************** you can add Header in this way as well
        //        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //************************************************************************************************** you can add Header in this way as well
        
        print("\nrequest.url : \(request.url!)")
        print("request method type: \(String(describing: request.httpMethod))")
        if let bdy = request.httpBody {
            print("body: \(NSString(data: bdy, encoding: String.Encoding.utf8.rawValue) ?? "body is empty")")
        }
        
        
        return request as URLRequest
    }
}
