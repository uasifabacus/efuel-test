import UIKit
import RealmSwift
import SwiftDate

class VC_searchCustomer: BaseViewController {
    
    
    @IBOutlet weak var tf_name: TextField!
    @IBOutlet weak var tf_serialNumb: TextField!
    @IBOutlet weak var tf_accountNumb: TextField!
    @IBOutlet weak var tf_address: TextField!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet var radioButtons: [UIButton]!
    
    @IBOutlet weak var sgControl: UISegmentedControl!
    
    @IBOutlet weak var lbl_accNumb: Label!
    
    @IBOutlet weak var lbl_customerName: Label!
    
    
    var arrayUnScheduled : [UserAccounts] = []
    var arrayScheduled: [Dictionary<String, Any?>] = []
    
    
    
    //MARK:-cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tf_name.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.tf_serialNumb.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.tf_accountNumb.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.tf_address.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        self.title = "Search"
    }
    
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {

        return (self.sgControl.selectedSegmentIndex == 0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? Master_order, let obj = sender as? UserAccounts {
            vc.selectedUser = obj
        }
    }
    
    
    
    @objc func textFieldDidChange(textField: UITextField){
        
        print("Text changed")
        self.makeQuery()
    }
    
    
    func makeQuery() {

        self.unScheduled()
        
        if self.sgControl.selectedSegmentIndex == 1 { self.scheduled() }
        
        self.tblView.reloadData()
    }
    
    private func scheduled() {
        
        let realm = try! Realm()

        self.arrayScheduled.removeAll()
        
        /*
        let dateInRegion = DateInRegion()
        let truckId = RealmHelper.shared.mobileDeliveryShiftTruckIdIfExists!
        let shiftId = RealmHelper.shared.mobileDeliveryShiftId!
        */
        
        self.arrayUnScheduled.forEach { (userAccount) in
            
            userAccount.installationStreetAddresses.forEach { (installtionStreetAddresses) in
                
                if let tanksPricingCreditMonitorStatus = installtionStreetAddresses.tanksPricingCreditMonitorStatus.last {
                    
                    
                    let dQ = realm.objects(DeliveryQueue.self).filter("tankId == %d AND deliveryOpenStatusCode = \(RecordType.newDelivery_skip_before_sync.rawValue)", tanksPricingCreditMonitorStatus.tankID)
                    
                    dQ.forEach { (deliveryQueue) in
                        self.arrayScheduled.append([k_deliveryDate: deliveryQueue.deliveryDatetime?.toFormat(k_dateFormatToShow),
                                                    k_username: userAccount.userAccountName])
                    }
                    
                    
                    
                    /*
                    if let dQ = realm.objects(DeliveryQueue.self).filter("tankId == %d AND deliveryOpenStatusCode = \(RecordType.newDelivery_skip_before_sync.rawValue) AND deliveryTruckId == %d AND (efuelMobileDeliveryShiftId == nil OR efuelMobileDeliveryShiftId == %d) OR (deliveryQueueId < 1000 AND deliveryOpenStatusCode = \(RecordType.newDelivery_skip_before_sync.rawValue))",
                        tanksPricingCreditMonitorStatus.tankID,
                        truckId,
                        shiftId).last {
                        
                        if let date = dQ.dateTime_delivery {
                            print(date >= dateInRegion)
                        }
                        
                    }
                    */
                }
            }
        }
        
        
        /*
                     ( d.Delivery_datetime >= '09/30/2019' AND d.Delivery_datetime <= '09/30/2019' ) AND d.delivery_truck_id = 483 )
                 AND ( d.efuel_mobile_delivery_shift_id IS NULL OR d.efuel_mobile_delivery_shift_id = 270 OR ( d.delivery_queue_id < 1000 AND d.delivery_open_status_code = 6 )
         */
        
        
//        print("deliveryQueue \(deliveryQueue)")
    }
    
    
    
    
    private func unScheduled() {
        
        self.arrayUnScheduled.removeAll()
        let realm = try! Realm()
        
        switch self.radioButtons.filter({$0.isSelected == true}).first?.tag {
        
        case 1:
            if let name = Helper.validateAndTrimText(TF: self.tf_name) {
                self.arrayUnScheduled.append(contentsOf: realm.objects(UserAccounts.self).filter("userAccountName contains[c] %@", name))
            }
            break
            
        case 2:
            if let serialNumb = Helper.validateAndTrimText(TF: self.tf_serialNumb) {
                
                let array_tanksPricingCreditMonitorStatus = realm.objects(TanksPricingCreditMonitorStatus.self).filter("serialNumOnTank contains[c] %@", serialNumb).filter({$0.owners.count > 0})
                
                array_tanksPricingCreditMonitorStatus.forEach { (tanksPricingCreditMonitorStatus) in
                    let installtionStreetAddresses = tanksPricingCreditMonitorStatus.owners
                    
                    installtionStreetAddresses.forEach { (installtionStreetAddress) in
                        self.arrayUnScheduled.append(contentsOf: installtionStreetAddress.owners)
                    }
                }
            }
            break
        
        case 3:
            if let accountNumb = Helper.validateAndTrimText(TF: self.tf_accountNumb), let int_ = accountNumb.int {
                self.arrayUnScheduled.append(contentsOf: realm.objects(UserAccounts.self).filter("accountNum == %d", int_))
            }
            break
            
        case 4:
            if let address = Helper.validateAndTrimText(TF: self.tf_address) {
                let temp_array = realm.objects(InstalltionStreetAddresses.self).filter("installationStreetAddressLine1 contains[c] %@", address)
                temp_array.forEach { (installtionStreetAddress) in
                    self.arrayUnScheduled.append(contentsOf: installtionStreetAddress.owners)
                }
            }
            break
            
        default:
            break
        }
    }
    
    
    
    
    
    
    
    private func radioButtonChanged(tag: Int, shouldMakeQuery: Bool = true) {
        guard self.radioButtons.filter({$0.isSelected == true}).first?.tag != tag else { return }
        
        
        self.radioButtons.forEach { (btn) in
            btn.isSelected = false
        }
        
        self.radioButtons.filter({$0.tag == tag}).first?.isSelected = true
        
        if shouldMakeQuery { self.makeQuery() }
        else { self.tblView.reloadData() }
    }
    
    @IBAction func action_radioButtonTapped(_ sender: UIButton) {
        
        self.radioButtonChanged(tag: sender.tag, shouldMakeQuery: false)
    }
    
    
    @IBAction func sgControlValChanged(_ sender: UISegmentedControl) {
        

        switch self.sgControl.selectedSegmentIndex {
        case 0:
            self.lbl_accNumb.text = "Acct. #"
            self.lbl_customerName.text = "Customer Name"
            break
        case 1:
            self.lbl_accNumb.text = "User Name"
            self.lbl_customerName.text = "Delivery Date"
            break
        default:
            break
        }
        self.tblView.reloadData()
    }
}



//MARK:-UITableViewDataSource
extension VC_searchCustomer: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.sgControl.selectedSegmentIndex == 0 {
            return self.arrayUnScheduled.count
        }
        return self.arrayScheduled.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_searchCustomer", for:indexPath) as! Cell_searchCustomer
        if self.sgControl.selectedSegmentIndex == 0 {
            cell.setupUI(userAccount: self.arrayUnScheduled[indexPath.row])
        }
        else {
            cell.setupUIForScheduled(obj: self.arrayScheduled[indexPath.row])
        }
        
        return cell
    }
}


//MARK:-UITableViewDelegate
extension VC_searchCustomer: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.deSelect(table: tableView)
        
        self.performSegue(withIdentifier: "segue_orderDetail", sender: self.arrayUnScheduled[indexPath.row])
    }
}



//MARK:-UITextFieldDelegate
extension VC_searchCustomer : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.radioButtonChanged(tag: textField.tag)
    }
}
