import UIKit
import Alamofire

class AlamofireManager: SessionManager
{
    static let shared = { () -> SessionManager in 
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.adapter = RequestInterceptor()
        
        return sessionManager
    }()
    
    
    
    
    static let sharedForBackground = { () -> SessionManager in
        
        let configuration = URLSessionConfiguration.background(withIdentifier: "com.abacusoft.eFuel")
        configuration.timeoutIntervalForRequest = 30 // seconds
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        sessionManager.adapter = RequestInterceptor()
        sessionManager.startRequestsImmediately = true
        
        return sessionManager
    }()
    
    


    override var backgroundCompletionHandler: (() -> Void)? {
        
        get {
//            CheckInOut.scheduleLocNotification(title: "get alamofire backgroundCompletionHandler",
//                                               body: AlamofireManager.sharedForBackground.backgroundCompletionHandler.debugDescription)
            return AlamofireManager.sharedForBackground.backgroundCompletionHandler
        }
        set {
//            CheckInOut.scheduleLocNotification(title: "set alamofire backgroundCompletionHandler",
//                                               body: newValue.debugDescription)
            AlamofireManager.sharedForBackground.backgroundCompletionHandler = newValue
        }
    }
}
