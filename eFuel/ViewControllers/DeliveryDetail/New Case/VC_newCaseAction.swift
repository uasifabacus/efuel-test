import UIKit

class VC_newCaseAction: BaseViewController {

    private static let array_reasons = ["No action",
                                        "Ship a replacement",
                                        "Send out field service",
                                        "ye sab Database sy aengy, aur DB me server sy aengy"]
    
    
    //MARK:- Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}





//MARK:- UITableViewDataSource
extension VC_newCaseAction : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VC_newCaseAction.array_reasons.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_newCaseReason", for:indexPath) as! Cell_newCaseReason
        cell.setupUI(str: VC_newCaseAction.array_reasons[indexPath.row])
        return cell
    }
}
