import Foundation
import Alamofire
import RealmSwift
import Realm

// MARK: - LookUp
class LookUpRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [LookUp]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [LookUp]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}



// MARK: - Datum
class LookUp: RealmSwift.Object, Codable {
    @objc dynamic var sqlUid: Int = 0
    @objc dynamic var textDef, listDef: String?
    
    enum CodingKeys: String, CodingKey {
        case sqlUid = "SQL_UID"
        case textDef = "TextDef"
        case listDef = "ListDef"
    }
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "sqlUid"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
