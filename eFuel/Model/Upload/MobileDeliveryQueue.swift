import UIKit
import Realm
import RealmSwift

class MobileDeliveryQueue: RealmSwift.Object, Codable {

    @objc dynamic var mobile_delivery_queue_id: Int = 0
    var efuel_mobile_delivery_queue_id = RealmOptional<Int>()// auto generated
    var efuel_mobile_delivery_shift_id = RealmOptional<Int>()
    var truck_id = RealmOptional<Int>()
    var mobile_delivery_shift_id = RealmOptional<Int>()
    var login_user_id = RealmOptional<Int>()
    var delivery_queue_id = RealmOptional<Int>()
    var tank_id = RealmOptional<Int>()
    @objc dynamic var created_by: String?
    @objc dynamic var record_create_time: String?
    @objc dynamic var inserted_to_tankdata: String?
    var record_type = RealmOptional<Int>()
    var sale_number = RealmOptional<Int>()
    var ticket_number = RealmOptional<Int>()
    var product_type = RealmOptional<Int>()
    @objc dynamic var start_time: String?
    @objc dynamic var end_time: String?
    var starting_tank_percent = RealmOptional<Int>()
    var final_tank_percent = RealmOptional<Int>()
    @objc dynamic var short_fill: String?
    @objc dynamic var taxable_sale: String?
    var units_of_measure_id = RealmOptional<Int>()
    var units_delivered = RealmOptional<Double>()
    var start_system_gross_totalizer = RealmOptional<Double>()
    var end_system_gross_totalizer = RealmOptional<Double>()
    var start_system_net_totalizer = RealmOptional<Double>()
    var end_system_net_totalizer = RealmOptional<Double>()
    var price_per_unit = RealmOptional<Double>()
    var net_sale_amount = RealmOptional<Double>()
    var sales_tax_amount = RealmOptional<Double>()
    var regulatory_fee = RealmOptional<Double>()
    var surcharge_1 = RealmOptional<Double>()
    var surcharge_2 = RealmOptional<Double>()
    var surcharge_3 = RealmOptional<Double>()
    var delivery_charge = RealmOptional<Double>()
    var gross_sale = RealmOptional<Double>()
    var cash_amount_received = RealmOptional<Double>()
    var check_amount_received = RealmOptional<Double>()
    @objc dynamic var check_date: String?
    var check_number = RealmOptional<Int>()
    var credit_card_amount_received = RealmOptional<Double>()
    var credit_card_num = RealmOptional<Int>()
    @objc dynamic var credit_card_exp_date: String?
    @objc dynamic var credit_card_name: String?
    @objc dynamic var credit_card_type: String?
    @objc dynamic var payment_method: String?
    var current_odometer = RealmOptional<Double>()
    var geocode_lat = RealmOptional<Double>()
    var geocode_long = RealmOptional<Double>()
    @objc dynamic var driver_signature: Data?
    @objc dynamic var customer_signature: Data?
    var delivery_skip_reason_id = RealmOptional<Int>()
    @objc dynamic var efuel_mobile_manual_delivery_date: String?
    @objc dynamic var warehouse_code: String?
    @objc dynamic var warehouse_tank_reference: String?
    var percent_receiving_warehouse_start = RealmOptional<Double>()
    var percent_receiving_warehouse_end = RealmOptional<Double>()
    var percent_delivering_warehouse_start = RealmOptional<Double>()
    var percent_delivering_warehouse_end = RealmOptional<Double>()
    
    
    
    
    
    enum CodingKeys: String, CodingKey {
        
        case efuel_mobile_delivery_queue_id
        case efuel_mobile_delivery_shift_id
        case truck_id
        case mobile_delivery_shift_id
        case login_user_id
        case delivery_queue_id
        case tank_id
        case created_by
        case record_create_time
        case inserted_to_tankdata
        case record_type
        case sale_number
        case ticket_number
        case product_type
        case start_time
        case end_time
        case starting_tank_percent
        case final_tank_percent
        case short_fill
        case taxable_sale
        case units_of_measure_id
        case units_delivered
        case start_system_gross_totalizer
        case end_system_gross_totalizer
        case start_system_net_totalizer
        case end_system_net_totalizer
        case price_per_unit
        case net_sale_amount
        case sales_tax_amount
        case regulatory_fee
        case surcharge_1
        case surcharge_2
        case surcharge_3
        case delivery_charge
        case gross_sale
        case cash_amount_received
        case check_date
        case check_number
        case credit_card_amount_received
        case credit_card_num
        case credit_card_exp_date
        case credit_card_name
        case credit_card_type
        case payment_method
        case current_odometer
        case geocode_lat
        case geocode_long
        case driver_signature
        case customer_signature
        case delivery_skip_reason_id
        case efuel_mobile_manual_delivery_date
        case warehouse_code
        case warehouse_tank_reference
        case percent_receiving_warehouse_start
        case percent_receiving_warehouse_end
        case percent_delivering_warehouse_start
        case percent_delivering_warehouse_end
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "mobile_delivery_queue_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    //Incrementa ID
    var IncrementaID : Int {
        let realm = try! Realm()
        if let retNext = realm.objects(MobileDeliveryQueue.self).sorted(byKeyPath: "mobile_delivery_queue_id").last?.mobile_delivery_queue_id {
            return retNext + 1
        }else{
            return 1
        }
    }
    
    
    
    var efuelMobileDeliveryQueueId : Int {
        let realm = try! Realm()
        return (realm.objects(MobileDeliveryQueue.self).count + 1)
    }
}
