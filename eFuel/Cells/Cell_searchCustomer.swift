import UIKit

class Cell_searchCustomer: UITableViewCell {

    @IBOutlet weak var lbl_acctNumb: UILabel!
    @IBOutlet weak var lbl_customerName: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setupUI(userAccount: UserAccounts) {
        self.lbl_acctNumb.text = userAccount.accountNum.value?.string ?? "-"
        self.lbl_customerName.text = userAccount.userAccountName
    }
    
    func setupUIForScheduled(obj: Dictionary<String, Any?>) {
    }
}
