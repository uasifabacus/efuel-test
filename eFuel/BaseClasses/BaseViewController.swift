import UIKit

class BaseViewController: UIViewController {

    
    @IBOutlet weak var view_container: UIView!
    
    private lazy var timeLeft = k_maxIdleTime
    private var timer: Timer?
    private var isMenuScree : Bool {
        return ((self as? VC_menu) != nil)
    }
    
    
    
    
    //MARK:- Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.hexStringToUIColor(hex: "E6E6FA")
        
        print("Controller Name: \(self)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if !(self is VC_tanks) {
            let tables = self.view.subviews.compactMap { $0 as? UITableView }
            
            for table in tables {
                deSelect(table: table)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.isMenuScree {
            self.setupTimer()
        }
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMenuScree {
            self.stopTimer()
            self.timeLeft = k_maxIdleTime
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.removeDownloadDataNotifier()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    //MARK:- to quit the App
    private func setupTimer() {
        
        self.stopTimer()
        self.timer = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] (timer) in
            
            self?.timeLeft -= 1
            print("self?.timeLeft : \(String(describing: self?.timeLeft))")
            if self?.timeLeft ?? 0 <= 0 {
                self?.stopTimer()
                exit(0)
            }
        }
    }
    
    
    
    private func stopTimer() {
        
        if let timer = self.timer, timer.isValid {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    
    
    
    
    
    //MARK:- addNotifier
    func addNotifier() {
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.someDataDownloaded),
            name: Notification.Name(k_notification_someDataDownloaded),
            object: nil)
    }
    
    
    @objc private func someDataDownloaded(notification: NSNotification) {
        
        //        print("notification.object: \(notification.object)")
        //        print("notification.userInfo: \(notification.userInfo)")
        
        if let str = notification.object as? String {
            Helper.showHud(str: str)
        }
    }
    
    
    func removeDownloadDataNotifier() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(k_notification_someDataDownloaded), object: nil)
    }
    
    
    
    func deSelect(table: UITableView) {
        if let selectedRow = table.indexPathForSelectedRow {
            table.deselectRow(at: selectedRow, animated: true)
        }
    }
    
    
    
    
    //MARK:- Private Functions
    internal func addViewControllerAsChildVC(childVC: UIViewController) {
        self.addChild(childVC)
        self.view_container.addSubview(childVC.view)
        childVC.view.frame = self.view_container.bounds
        childVC.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        childVC.didMove(toParent: self)
//        childVC.view.isHidden = true
    }
    
    
    internal func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)

        // Remove Child View From Superview
        viewController.view.removeFromSuperview()

        // Notify Child View Controller
        viewController.removeFromParent()
    }
}
