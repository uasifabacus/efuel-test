import UIKit
import SwiftDate

class VC_about: BaseViewController {

    @IBOutlet weak var lbl_version: Label!
    @IBOutlet weak var lbl_dateTime: Label!
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "About Us"
        
        if let bundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            self.lbl_version.text = "Version : \(bundleVersion)"
        }
        else {
            self.lbl_version.text = "Version : -"
        }

        self.lbl_dateTime.text = k_buildDate
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
