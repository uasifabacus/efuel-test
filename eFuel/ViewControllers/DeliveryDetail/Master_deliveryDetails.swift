import UIKit

class Master_deliveryDetails: BaseViewController {

    @IBOutlet weak var segmentC: UISegmentedControl!
    
    private lazy var vc_customer: VC_customer = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_customer") as! VC_customer
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    private lazy var vc_fees: VC_fees = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_fees") as! VC_fees
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    private lazy var vc_delivery: VC_delivery = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_delivery") as! VC_delivery
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    private lazy var vc_monitor: VC_monitor = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_monitor") as! VC_monitor
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    private lazy var vc_service: VC_service = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_service") as! VC_service
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    private lazy var vc_GPS: VC_GPS = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC_GPS") as! VC_GPS
        self.addViewControllerAsChildVC(childVC: vc)
        return vc
    }()
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Delivery Details"
        
        
        self.action_changeSegment(self.segmentC)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    //MARK:- Actions
    @IBAction func action_changeSegment(_ sender: UISegmentedControl) {
        
        self.vc_customer.view.isHidden = !(sender.selectedSegmentIndex == 0)
        self.vc_fees.view.isHidden = !(sender.selectedSegmentIndex == 1)
        self.vc_delivery.view.isHidden = !(sender.selectedSegmentIndex == 2)
        self.vc_monitor.view.isHidden = !(sender.selectedSegmentIndex == 3)
        self.vc_service.view.isHidden = !(sender.selectedSegmentIndex == 4)
        self.vc_GPS.view.isHidden = !(sender.selectedSegmentIndex == 5)
    }
}
