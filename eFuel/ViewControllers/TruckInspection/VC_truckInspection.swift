import UIKit
import RealmSwift
import MessageUI


//SecondViewController
protocol PassDataPopoverToParent : class {
    func passData(obj: Any?, vc: UIViewController)
}



class VC_truckInspection: BaseViewController {
    
    
    var truck: Trucks!
    
    private var array_filtered : Results<TruckInspectionReportItems>? = nil {
        didSet {
            self.tblView.reloadData {
                self.tblView.scrollToTop(animated: false)
                self.tblView.layoutIfNeeded()
            }
        }
    }
    
    var array_areAllTraverse = [false, false, false, false]
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var segmntControl: UISegmentedControl!
    
    
    
    
    
    //MARK:- cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Truck Inspection"
        self.action_segmentChanged(self.segmntControl)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblView.reloadData()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        RealmHelper.uncheckTruckInspectReportItems()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? VC_shiftSummary {
            vc.truck = self.truck
        }
        
        if let vc = segue.destination as? VC_truckInspectionRemarks {

            vc.popOverSetting(widthPercent: k_popOverWidthPercent, heightPercent: k_popOverHeightPercent)
            vc.delegate_ = self
        }
        
        if let vc = segue.destination as? VC_verifyDefects {
            vc.array_defects = RealmHelper.shared.realm.objects(TruckInspectionReportItems.self).filter({$0.isChecked == true})
            vc.popOverSetting(widthPercent: 50, heightPercent: 70)
            vc.delegate_ = self
        }
    }
    
    
    @IBAction func action_segmentChanged(_ sender: UISegmentedControl) {
        
        self.tblView.scrollToTop(animated: false)
        switch sender.selectedSegmentIndex {
            
        case 0:
            //A-D
            self.array_filtered = RealmHelper.shared.realm.objects(TruckInspectionReportItems.self).filter("item BEGINSWITH 'A' OR item BEGINSWITH 'B' OR item BEGINSWITH 'C' OR item BEGINSWITH 'D'").sorted(byKeyPath: "item")
            self.array_areAllTraverse[0] = true
            break
        case 1:
            //e to h
            self.array_filtered = RealmHelper.shared.realm.objects(TruckInspectionReportItems.self).filter("item BEGINSWITH 'E' OR item BEGINSWITH 'F' OR item BEGINSWITH 'G' OR item BEGINSWITH 'H'").sorted(byKeyPath: "item")
            self.array_areAllTraverse[1] = true
            break
        case 2:
            //i to s
            self.array_filtered = RealmHelper.shared.realm.objects(TruckInspectionReportItems.self).filter("item BEGINSWITH 'I' OR item BEGINSWITH 'J' OR item BEGINSWITH 'K' OR item BEGINSWITH 'L' OR item BEGINSWITH 'M' OR item BEGINSWITH 'N' OR item BEGINSWITH 'O' OR item BEGINSWITH 'P' OR item BEGINSWITH 'Q' OR item BEGINSWITH 'R' OR item BEGINSWITH 'S'").sorted(byKeyPath: "item")
            self.array_areAllTraverse[2] = true
            break
        case 3:
            //t to z
            self.array_filtered = RealmHelper.shared.realm.objects(TruckInspectionReportItems.self).filter("item BEGINSWITH 'T' OR item BEGINSWITH 'U' OR item BEGINSWITH 'V' OR item BEGINSWITH 'W' OR item BEGINSWITH 'X' OR item BEGINSWITH 'Y' OR item BEGINSWITH 'Z'").sorted(byKeyPath: "item")
            self.array_areAllTraverse[3] = true
            break
        default:
            break
        }
    }
    
    
    @IBAction func action_continue(_ sender: Any) {


        guard self.array_areAllTraverse.filter({$0 == false}).count == 0 else {
            Helper.showBanner(text: "Traverse all Tabs", isSuccess: false)
            return
        }




        if RealmHelper.shared.realm.objects(TruckInspectionReportItems.self).filter({$0.isChecked == true}).count > 0 {
            self.performSegue(withIdentifier: "segue_verifyDefects", sender: self.truck)
        }
        else {
            self.showAlert(title: nil,
                           message: "No defective items checked. Yes to continue?",
                           buttonTitles: ["Yes", "No"],
                           highlightedButtonIndex: nil) { (buttonTag) in
                            
                            if buttonTag == 0 {
                                self.showPrinterMessage(remarks: "")
                            }
            }
        }
    }
    
    
    private func saveDefectsToDB() {
        
//******************************************** selected defects will go in TruckInspectionReportDefects table
        
        let realm = try! Realm()
        realm.objects(TruckInspectionReportItems.self).filter({$0.isChecked == true}).forEach { (objItem) in
            
            let obj = TruckInspectionReportDefects()
            obj.truck_inspection_report_defects_id = obj.IncrementaID()
            obj.inspection_time = realm.objects(TruckInspectionReports.self).last?.inspection_time
            obj.truck_id = RealmHelper.shared.realm.objects(Trucks.self).filter({$0.truckInUse.value == true}).first!.truckID
            obj.truck_inspection_report_item_id = objItem.truckInspectionReportItemID
            
            try! realm.write {
                realm.add(obj)
            }
        }
    }
    
    
    
    private func saveReportToDB(remarks: String) {
        
        
//******************************************** Remarks will go in TruckInspectionReports table
        
        
        let realm = try! Realm()
        
        let obj = TruckInspectionReports()
        obj.truck_inspection_report_id = obj.IncrementaID
        obj.inspection_time = Helper.mmDDYYWithCurrentTime
        obj.truck_id.value = self.truck.truckID
        obj.login_user_id.value = RealmHelper.shared.userId
        obj.remarks = remarks
        
        try! realm.write {
            realm.add(obj)
        }
    }
    
    
    func saveDatatoDB(remarks: String) {
        
        RealmHelper.deleteFromDB(type: TruckInspectionReports.self)
        RealmHelper.deleteFromDB(type: TruckInspectionReportDefects.self)
        
        self.saveReportToDB(remarks: remarks)
        self.saveDefectsToDB()
    }
}


//MARK:- UITableViewDataSource
extension VC_truckInspection : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_filtered?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: Cell_truckInspection.self), for: indexPath) as! Cell_truckInspection
        
        if let obj = self.array_filtered?[indexPath.row] { cell.setupUI(obj: obj) }
        
        return cell
    }
}




//MARK:- UITableViewDataSource
extension VC_truckInspection : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.deSelect(table: tblView)
        
        if let obj = self.array_filtered?[indexPath.row] {
            
            try! RealmHelper.shared.realm.write {
                obj.isChecked = !obj.isChecked
            }
            
            tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
}




//MARK:- PassDataPopoverToParent protocol implementation
extension VC_truckInspection : PassDataPopoverToParent {
    
    func passData(obj: Any?, vc: UIViewController) {

        if let remarks = obj as? String, vc is VC_truckInspectionRemarks{
            self.showPrinterMessage(remarks: remarks)
        }
        
        if vc is VC_verifyDefects {
            self.performSegue(withIdentifier: "segue_remarksPopOver", sender: self.truck)
        }
    }

    
    
    private func showPrinterMessage(remarks: String) {
        
        self.showAlert(title: nil,
                       message: "The Driver's daily inspection report is complete. Please insert a blank sheet of paper folded in half the long way if you want to print it now. \nDo you want to print it now?",
                       buttonTitles: ["Yes", "No"],
                       highlightedButtonIndex: nil) { (buttonTag) in
                        if buttonTag == 0 {
                            self.isPrinterConected(truckId: self.truck.truckID) { (isSucceed, deviceType) in
                                if isSucceed {

                                    self.saveDatatoDB(remarks: remarks)
                                    
                                    if let reportToPrint = self.inspectReportPrint(truckId: self.truck.truckID) {
                                        if self.email(str: reportToPrint, vc: self) == false {
                                            self.goToShiftSummary()
                                        }
                                    }
                                    else {
                                        self.goToShiftSummary()
                                    }
                                }
                            }
                        }
                        else {
                            self.saveDatatoDB(remarks: remarks)
                            self.goToShiftSummary()
                        }
        }
    }


    
    
    private func goToShiftSummary() {
        
        self.performSegue(withIdentifier: "segue_shiftSummary", sender: self.truck)
    }
}


//MARK:- VC_truckInspection MFMailComposeViewControllerDelegate
extension VC_truckInspection : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
            self.goToShiftSummary()
        }
    }
}







//MARK:- VC_truckInspectionRemarks
class VC_truckInspectionRemarks: BaseViewController {

    weak var delegate_ : PassDataPopoverToParent?
    
    @IBOutlet weak var TV_remarks: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.TV_remarks.becomeFirstResponder()
    }
    
    @IBAction func action_ok(_ sender: Any) {
        
        let remarks: String!
        
        if let remarks_ = Helper.validateAndTrimText(str: self.TV_remarks.text) { remarks = remarks_ }
        else { remarks = "" }
        
        self.dismiss(animated: true) {
            self.delegate_?.passData(obj: remarks, vc: self)
        }
    }
}

extension VC_truckInspectionRemarks : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 100    // 10 Limit Value
    }
}





//MARK:- VC_verifyDefects
class VC_verifyDefects: BaseViewController {

    var array_defects : [TruckInspectionReportItems]!
    weak var delegate_ : PassDataPopoverToParent?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func action_ok(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate_?.passData(obj: nil, vc: self)
        }
    }
}



extension VC_verifyDefects : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_defects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_truckInspection", for: indexPath) as! Cell_truckInspection
        cell.lbl_item.text = self.array_defects[indexPath.row].item
        return cell
    }
}
