import UIKit
import Realm
import RealmSwift

class MobileSyncLog: RealmSwift.Object, Codable {

    @objc dynamic var sync_log_id: Int = 0
    var operating_location_id = RealmOptional<Int>()
    var login_user_id = RealmOptional<Int>()
    var truck_id = RealmOptional<Int>()
    @objc dynamic var sync_type: String?
    @objc dynamic var sync_status: String?
    @objc dynamic var sync_time: String?
    @objc dynamic var sync_failure_reason: String?
    @objc dynamic var connection_type: String?
    
    
    
    enum CodingKeys: String, CodingKey {
        
        case sync_log_id
        case operating_location_id
        case login_user_id
        case truck_id
        case sync_type
        case sync_status
        case sync_time
        case sync_failure_reason
        case connection_type
    }
    
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "sync_log_id"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
    
    
    //Incrementa ID
    func IncrementaID() -> Int{
        let realm = try! Realm()
        if let retNext = realm.objects(MobileSyncLog.self).sorted(byKeyPath: "sync_log_id").last?.sync_log_id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
