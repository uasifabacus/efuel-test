import Alamofire
import RealmSwift

extension DataResponse {
    
    @discardableResult
    func arbiGenericResponse(shouldShowErrorBanner: Bool = true) -> Any? {
        
        
        switch self.result {
            
        case .success(let data):
            
            
            switch data {
            case let dic as NSDictionary:
                
                /*
                if (dic.string_status == k_success) {
                    
                    return dic.string_message
                }
                else {
                    Helper.showBanner(text: dic.string_message ?? k_serverSideIssue, isSuccess: false)
                    return nil
                }
                */

                print("dic response: \(dic)")

                if dic[k_message] as? String == k_succeed ||
                    dic[k_message] as? String == k_dataSavedSuccessfully ||
                    dic[k_message] as? String == k_succeedWithNoData {
                    
                    return dic[k_message] as? String
                }
                else {
                    self.showBanner(shouldShowErrorBanner: shouldShowErrorBanner, str: dic[k_message] as? String ?? k_serverSideIssue)
                    print(dic)
                    return nil
                }
                
                
                
                
                
            case let codable as Codable:
                if let jsonString = codable.jsonString() {
                    
                    if let dic = jsonString.convertToDictionary() {

                        
                        if dic[k_message] as? String == k_succeed ||
                            dic[k_message] as? String == k_succeedWithNoData {
                            return data
                        }
                        else {
                            print(dic)
                            self.showBanner(shouldShowErrorBanner: shouldShowErrorBanner, str: (dic[k_message] as? String) ?? k_serverSideIssue)
                            return nil
                        }
                    }
                    else {
                        self.showBanner(shouldShowErrorBanner: shouldShowErrorBanner, str: "Not able to convert to Dictionary")
                    }
                }
                else {
                    self.showBanner(shouldShowErrorBanner: shouldShowErrorBanner, str: "Not able to convert to JSON String")
                }
                
                
            case let realmObject as RealmSwift.Object:
                if let status = realmObject.value(forKey: k_statusCode) {
                    
                    if status as! String == k_succeed {
                        return data
                    }
                    else {
                        Helper.hideHud()
                        return nil
                    }
                }
                else {
                    self.showBanner(shouldShowErrorBanner: shouldShowErrorBanner, str: (realmObject.value(forKey: k_message) as? String) ?? k_serverSideIssue)
                    return nil
                }
                
                
            default:
                break
            }
            
            return data
            
            
        case .failure(let error):
            
            print(error.localizedDescription)
            print(error)
            
            if error.localizedDescription != k_cancelled {
                
                self.showBanner(shouldShowErrorBanner: shouldShowErrorBanner, str: error.localizedDescription)
            }
            
            return nil
        }
    }
    
    
    
    /*
    @discardableResult
    func arbiGenericResponseBackground(shouldShowErrorBanner: Bool = true) -> (Bool, String) {
        
        
        switch self.result {
            
        case .success(let data):
            
            
            switch data {
            case let dic as NSDictionary:
                
                
                if dic[k_message] as? String == k_succeed ||
                    dic[k_message] as? String == k_dataSavedSuccessfully ||
                    dic[k_message] as? String == k_succeedWithNoData {
                    
                    return ( true, dic[k_message] as? String ?? "Succeed")
                }
                else {
                    self.showBanner(shouldShowErrorBanner: shouldShowErrorBanner, str: dic[k_message] as? String ?? k_serverSideIssue)
                    return (false, dic[k_message] as? String ?? k_serverSideIssue)
                }
                
                
            default:
                break
            }
            
            
        case .failure(let error):
            
            print(error.localizedDescription)
            print(error)
            
            if error.localizedDescription != k_cancelled {
                
                self.showBanner(shouldShowErrorBanner: shouldShowErrorBanner, str: error.localizedDescription)
                return (false, error.localizedDescription)
            }
        }
        
        return (false , "")
    }
    */
    
    
    
    
    
    private func showBanner(shouldShowErrorBanner: Bool, str: String) {
        
        Helper.hideHud()
        if shouldShowErrorBanner {
            Helper.showBanner(text: str, isSuccess: false)
        }
    }
}




/*
protocol ArbiResponseBaseProtocol {
    var string_message : String? { get }
    var string_status : String? { get }
}



extension NSDictionary : ArbiResponseBaseProtocol {
    
    var string_status: String? {
        get {
            return self.value(forKey: k_status) as? String
        }
    }
    var string_message: String? {
        get {
            return self.value(forKey: k_message) as? String
        }
    }
}
*/
