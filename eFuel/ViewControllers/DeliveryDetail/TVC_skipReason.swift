import UIKit

//
// MARK: - View Controller
//
class TVC_skipReason: UITableViewController {
    
//    var sections = sectionsData
    
    
    private lazy var arrayCategories = {
        RealmHelper.shared.realm.objects(DeliverySkipCategories.self)
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Auto resizing the height of the cell
        tableView.estimatedRowHeight = 44.0
        
        self.title = "Skip Reasons"
    }
    
}

//
// MARK: - View Controller DataSource and Delegate
//
extension TVC_skipReason {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayCategories.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayCategories[section].collapsed ? 0 : self.arrayCategories[section].reasons.count
    }
    
    // Cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: Cell_newCaseReason = tableView.dequeueReusableCell(withIdentifier: "Cell_newCaseReason") as! Cell_newCaseReason
        
        let item = self.arrayCategories[indexPath.section].reasons[indexPath.row]
        
        cell.lbl_title.text = item.reason
        
        return cell
    }
    
    
    // Header
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? Header_TVC_skipReason ?? Header_TVC_skipReason(reuseIdentifier: "header")
        
        header.titleLabel.text = self.arrayCategories[section].skipCategoryDescriptions
        header.arrowLabel.text = ">"
        header.setCollapsed(self.arrayCategories[section].collapsed)
        
        header.section = section
        header.delegate = self
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 74.0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
}

//
// MARK: - Section Header Delegate
//
extension TVC_skipReason: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: Header_TVC_skipReason, section: Int) {
        let collapsed = !self.arrayCategories[section].collapsed
        
        // Toggle collapse
        try! RealmHelper.shared.realm.write {
            self.arrayCategories[section].collapsed = collapsed
        }
        
        header.setCollapsed(collapsed)
        
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
