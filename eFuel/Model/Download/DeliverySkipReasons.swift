import Foundation
import Alamofire
import Realm
import RealmSwift

// MARK: - DeliverySkipReasonsRoot
class DeliverySkipReasonsRoot: Codable {
    let message: String?
    let statusCode: Int?
    let errorDetail: String?
    let array: [DeliverySkipReasons]?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case statusCode = "StatusCode"
        case errorDetail = "ErrorDetail"
        case array = "data"
    }
    
    init(message: String?, statusCode: Int?, errorDetail: String?, data: [DeliverySkipReasons]?) {
        self.message = message
        self.statusCode = statusCode
        self.errorDetail = errorDetail
        self.array = data
    }
}

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseDatum { response in
//     if let datum = response.result.value {
//       ...
//     }
//   }

// MARK: - Datum
class DeliverySkipReasons: RealmSwift.Object, Codable {
    
    @objc dynamic var deliverySkipReasonID = 0
    @objc dynamic var deliverySkipCategoryID: Int = -1
    @objc dynamic var reasonProcessingCode: Int = -1
    @objc dynamic var reason: String?
    
    
    enum CodingKeys: String, CodingKey {
        case deliverySkipReasonID = "delivery_skip_reason_id"
        case deliverySkipCategoryID = "delivery_skip_category_id"
        case reason
        case reasonProcessingCode = "reason_processing_code"
    }
    
    
    //MARK:- realm
    override static func primaryKey() -> String? {
        return "deliverySkipReasonID"
    }
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    //MAKR:- realm
}
