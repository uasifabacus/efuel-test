import UIKit

class Cell_truckInspection: UITableViewCell {

    @IBOutlet weak var lbl_item: Label!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    func setupUI(obj: TruckInspectionReportItems) {
        
        if obj.isChecked {
            self.accessoryType = .checkmark
        }
        else {
            self.accessoryType = .none
        }
        
        self.lbl_item.text = obj.item
    }
    
    
    
    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        self.selectionStyle = .none
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
////        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        self.accessoryType = selected ? .checkmark : .none
//    }
}
